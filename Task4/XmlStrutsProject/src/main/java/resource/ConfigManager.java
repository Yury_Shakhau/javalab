package resource;

import java.util.ResourceBundle;

public class ConfigManager {

	private static final String RESOURCE_PATH = "ApplicationResources";
	
	private static final ResourceBundle BUNDLE;
	
	static {
		BUNDLE = ResourceBundle.getBundle(RESOURCE_PATH);
	}
	
	public static String key(String key) {
		return BUNDLE.getString(key);
	}
}
