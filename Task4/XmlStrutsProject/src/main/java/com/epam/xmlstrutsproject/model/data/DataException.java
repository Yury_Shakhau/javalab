package com.epam.xmlstrutsproject.model.data;

public class DataException extends Exception {
	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 432456457782421L;

	public DataException() {
		super();
	}
	
	public DataException(String message) {
		super(message);
	}
	
	public DataException(Throwable t) {
		super(t);
	}
	
	public DataException(String message, Throwable t) {
		super(message, t);
	}
}
