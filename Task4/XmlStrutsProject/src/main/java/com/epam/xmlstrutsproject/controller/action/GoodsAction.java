package com.epam.xmlstrutsproject.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.MappingDispatchAction;
import org.jdom.Document;

import com.epam.xmlstrutsproject.controller.ControllerExeption;
import com.epam.xmlstrutsproject.model.actionform.GoodsForm;
import com.epam.xmlstrutsproject.model.businesslayer.BusinesslayerException;
import com.epam.xmlstrutsproject.model.businesslayer.GoodsService;
import com.epam.xmlstrutsproject.model.data.entity.Product;

public class GoodsAction extends MappingDispatchAction {

	/** Message in success case action forward */
	private static final String SUCCESS = "success";
	
	/** Message in success case action forward */
	private static final String FAILURE = "failure";

	private ActionForward readXmlAndForward(ActionMapping mapping, ActionForm form,
			HttpServletRequest request)
			throws ControllerExeption {
		GoodsForm goodsForm = (GoodsForm) form;
		
		try {
			goodsForm.setBackUrl(RequestHeader.currentUrl(request));
			goodsForm.setDocument(GoodsService.readGoods(request));
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}
		
		return mapping.findForward(SUCCESS);
	}
	
	/**
	 * Catalog page action.
	 */
	public ActionForward categories(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		return readXmlAndForward(mapping, form, request);
	}
	
	/**
	 *Subcatalog page action.
	 */
	public ActionForward subcategories(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		return readXmlAndForward(mapping, form, request);
	}
	
	/**
	 *Products page action.
	 */
	public ActionForward products(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		return readXmlAndForward(mapping, form, request);
	}
	
	/**
	 * Save pruduct action.
	 */
	public ActionForward saveProducts(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		try {
			GoodsForm goodsForm = (GoodsForm) form;
			Document document = goodsForm.getDocument();
			
			GoodsService.saveGoods(document);
				
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}
		
		return mapping.findForward(SUCCESS);
	}
	
	/**
	 * Adding product page action.
	 */
	public ActionForward addingProduct(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		GoodsService.createProductInForm((GoodsForm) form);
		return readXmlAndForward(mapping, form, request);
	}
	
	/**
	 * Add product action.
	 */
	public ActionForward addProduct(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		try {
			GoodsForm goodsForm = (GoodsForm) form;
			if (GoodsService.addProductWithXsl(request, goodsForm)) {
				goodsForm.setProduct(new Product());
				if (goodsForm.getBackUrl() != null) {
					return new ActionRedirect(goodsForm.getBackUrl());
				} else {
					return mapping.findForward(SUCCESS);
				}
			}
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}
		return mapping.findForward(FAILURE);
	}
	
	/**
	 * Editing action.
	 */
	public ActionForward editingProducts(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		return readXmlAndForward(mapping, form, request);
	}
}
