package com.epam.xmlstrutsproject.model.data;

public final class GoodsTagName {
	
	private GoodsTagName() {}
	
	public static final String PRICE = "price";
	
	public static final String NOT_IN_STOCK = "not-in-stock";
	
	public static final String PRODUCT = "product";
}
