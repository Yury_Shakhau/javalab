package com.epam.xmlstrutsproject.model.businesslayer;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;

import org.jdom.Document;
import org.jdom.Element;

import com.epam.xmlstrutsproject.model.actionform.GoodsForm;
import com.epam.xmlstrutsproject.model.data.DataException;
import com.epam.xmlstrutsproject.model.data.GoodsTagName;
import com.epam.xmlstrutsproject.model.data.XmlRepository;
import com.epam.xmlstrutsproject.model.data.entity.Product;
import com.epam.xmlstrutsproject.util.file.FileWorker;
import com.epam.xmlstrutsproject.util.xsltemplate.TemplateUtil;

/**
 * Haves method to work with jdom document.
 * 
 * @author Yury_Shakhau
 *
 */
public final class GoodsService {

	private static final XmlRepository GOODS_REPOSITORY = new XmlRepository();
	
	private static final String CATALOG = "\\WEB-INF\\xml";

	private static final String GOODS_XML = CATALOG + "\\goods.xml";
	
	private static final String ADD_PRODUCT_XSL = CATALOG
			+ "\\add-product.xsl";
	
	private static final Lock lock = new ReentrantLock();
	
	/**
	 * Prepares transformer before transformation.
	 * 
	 * @param request
	 * @param xslPath
	 * @param parameter
	 * @param createProduct
	 * @return
	 * @throws BusinesslayerException
	 */
	private static Transformer prepareTransformer(HttpServletRequest request,
			String xslPath, GoodsForm form)
			throws BusinesslayerException {
		Transformer transformer = null;

		try {

			String xslFullPath = FileWorker.fullPath(request, xslPath);

			transformer = TemplateUtil.createTransformer(xslFullPath);
			if (form.getCategoryName() != null) {
				transformer.setParameter(GoodsForm.CATEGORY_NAME, form.getCategoryName());
			}
			if (form.getSubcategoryName() != null) {
				transformer.setParameter(GoodsForm.SUBCATEGORY_NAME, form.getSubcategoryName());
			}
			Product product = form.getProduct();
			if (product != null) {
				transformer.setParameter(Product.THIS_XSL_NAME, product);
			}

		} catch (TransformerException e) {
			throw new BusinesslayerException(e);
		}

		return transformer;
	}
	
	/**
	 * Put not-in-stock tag where price is null
	 * 
	 * @param element
	 */
	private static void putNotInStockNeeded(Element element) {
		if (GoodsTagName.PRODUCT.equals(element.getName())) {
			Element priceTag = element.getChild(GoodsTagName.PRICE);
			Element notInTag = element.getChild(GoodsTagName.NOT_IN_STOCK);
			if (priceTag.getText().length() == 0) {
				if (notInTag == null) {
					element.addContent(new Element(GoodsTagName.NOT_IN_STOCK));
				}
			} else {
				if (notInTag != null) {
					element.removeChild(GoodsTagName.NOT_IN_STOCK);
				}
			}
		} else {
			@SuppressWarnings("unchecked")
			List<Element> children = element.getChildren();
			for (int i = children.size() - 1; i >= 0; i--) {
				putNotInStockNeeded(children.get(i));
			}
		}
	}
	
	/**
	 * Creates product in form if that null.
	 * 
	 * @param form
	 */
	public static void createProductInForm(GoodsForm form) {
		if (form.getProduct() == null) {
			form.setProduct(new Product());
		}
	}
	
	/**
	 * Reads xml document from repository.
	 * 
	 * @param request
	 * @return xml document.
	 * @throws BusinesslayerException
	 */
	public static Document readGoods(HttpServletRequest request) 
			throws BusinesslayerException {
		String fullXmlPath = FileWorker.fullPath(request, GOODS_XML);
		return GOODS_REPOSITORY.loadDocument(fullXmlPath);
	}

	/**
	 * Saves xml document in repository.
	 * 
	 * @param document
	 * @throws BusinesslayerException
	 */
	public static void saveGoods(Document document) 
			throws BusinesslayerException {
		
		try {
			
			lock.lock();
				putNotInStockNeeded(document.getRootElement());
				GOODS_REPOSITORY.saveDocument(document);
				
		} catch (DataException e) {
			throw new BusinesslayerException(e);
		} finally {
			lock.unlock();
		}
	
	}
	
	/**
	 * Creates new xml node in xml file with xsl transformation.
	 * 
	 * @param request
	 * @param form
	 * @return result of adding operation. returns true if product that valid. And
	 * 		   false if not.
	 * @throws BusinesslayerException
	 */
	public static boolean addProductWithXsl(HttpServletRequest request, GoodsForm form) 
			throws BusinesslayerException {
		boolean isValid = form.getProduct().isValid();
		if (isValid) {

			try {
				Transformer transformer = prepareTransformer(request, ADD_PRODUCT_XSL, form);
				GOODS_REPOSITORY.transformDocument(transformer);
			} catch (DataException e) {
				throw new BusinesslayerException(e);
			}
			
		}
		return isValid;
	}
}
