package com.epam.xmlstrutsproject.controller;

import java.sql.SQLException;

/**
* The exception class of business layer.
* It's inherited from SQLException class
* 
* @author Yury_Shakhau
*/
public class ControllerExeption extends SQLException {

	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 4699902334668435921L;

	public ControllerExeption() {
		super();
	}
	
	public ControllerExeption(String message) {
		super(message);
	}
	
	public ControllerExeption(Throwable t) {
		super(t);
	}
	
	public ControllerExeption(String message, Throwable t) {
		super(message, t);
	}
}