package com.epam.xmlstrutsproject.model.data;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadCounter {
	
	private final AtomicInteger THREAD_COUNTER = new AtomicInteger(0);
	private final AtomicInteger THREAD_COUNTER_PARALLEL = new AtomicInteger(0);
	private final AtomicInteger THREAD_COUNTER_LAST = new AtomicInteger(0);

	private static int MAX_THREAD_COUNT;
	
	public ThreadCounter(int maxThreadCount) {
		MAX_THREAD_COUNT = maxThreadCount;
	}
	
	public void nextThread() {
		THREAD_COUNTER_PARALLEL.incrementAndGet();
		THREAD_COUNTER.incrementAndGet();
		THREAD_COUNTER_LAST.incrementAndGet();

		// if threadCounterLast overflowed.
		if (THREAD_COUNTER_LAST.get() == Integer.MAX_VALUE) {
			THREAD_COUNTER_LAST.set(THREAD_COUNTER_PARALLEL.get() + 1);
		}
	}
	
	public boolean isNotFirstOrSingleThread() {
		
		boolean notFirst = true;
		
		// If that not single thread.
		if (THREAD_COUNTER_LAST.get() > 1) {

			// If that not first thread.
			notFirst = !THREAD_COUNTER_LAST.equals(THREAD_COUNTER_PARALLEL);
		}
		return notFirst;
	}
	
	public boolean isThatLastOrCountedThread() {
		return (THREAD_COUNTER_PARALLEL.get() == 1
				|| THREAD_COUNTER.get() >= MAX_THREAD_COUNT);
	}
	
	public void resetMaxThreadCounter() {
		THREAD_COUNTER.set(0);
	}
	
	public void threadGone() {
		THREAD_COUNTER_PARALLEL.decrementAndGet();

		// If all threads gone.
		if (THREAD_COUNTER_PARALLEL.get() == 0) {
			THREAD_COUNTER_LAST.set(0);
		}
	}
}
