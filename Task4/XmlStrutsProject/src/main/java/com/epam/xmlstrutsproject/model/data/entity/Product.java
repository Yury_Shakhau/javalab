package com.epam.xmlstrutsproject.model.data.entity;

import java.io.Serializable;

import com.epam.xmlstrutsproject.model.data.entity.validation.ProductPattern;

public class Product implements Serializable {

	/**
	 * For deserialization with no exception.
	 */
	private static final long serialVersionUID = 5232744601400914269L;

	public static final String THIS_XSL_NAME = "product";
	public static final String NAME = "name";
	public static final String PROVIDER = "provider";
	public static final String MODEL = "model";
	public static final String DATE_OF_ISSUE = "dateOfIssue";
	public static final String COLOR = "color";
	public static final String NOT_IN_STOCK = "notInStock";
	public static final String PRICE = "price";
	
	private String name;
	private String provider;
	private String model;
	private String dateOfIssue;
	private String color;
	private String price;
	private Boolean notInStock = false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return !notInStock ? price : null;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public boolean isNotInStock() {
		return notInStock;
	}

	public void setNotInStock(boolean notInStock) {
		this.notInStock = notInStock;
	}

	public Boolean isValidName() {
		return (name == null || name.matches(ProductPattern.NAME_PATTERN));
	}

	public Boolean isValidProvider() {
		return (provider == null || provider
				.matches(ProductPattern.PROVIDER_PATTERN));
	}

	public boolean isValidModel() {
		return (model == null || model.matches(ProductPattern.MODEL_PATTERN));
	}

	public boolean isValidDateOfIssue() {
		return (dateOfIssue == null || dateOfIssue
				.matches(ProductPattern.DATE_OF_ISSUE_PATTERN));
	}

	public boolean isValidColor() {
		return (color == null || color.matches(ProductPattern.COLOR_PATTERN));
	}

	public boolean isValidPrice() {
		return (notInStock == null || notInStock || price == null
				|| price.length() == 0 || price
					.matches(ProductPattern.PRICE_PATTERN));
	}

	public boolean isValidLength(String field) {
		return (field == null || field.length() > 0);
	}

	public boolean isValid() {
		if (name == null) {
			return false;
		}
		if (!isValidLength(name)) {
			return false;
		}
		if (!isValidLength(provider)) {
			return false;
		}
		if (!isValidLength(model)) {
			return false;
		}
		if (!isValidLength(dateOfIssue)) {
			return false;
		}
		if (!isValidLength(color)) {
			return false;
		}
		if (!notInStock && !isValidLength(price)) {
			return false;
		}
		if (!isValidName()) {
			return false;
		}
		if (!isValidProvider()) {
			return false;
		}
		if (!isValidModel()) {
			return false;
		}
		if (!isValidDateOfIssue()) {
			return false;
		}
		if (!isValidColor()) {
			return false;
		}
		if (!isValidPrice()) {
			return false;
		}
		return true;
	}
}
