package com.epam.xmlstrutsproject.util.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

public class XmlWorker {

	private static final SAXBuilder SAX_BUILDER = new SAXBuilder();
	
	public static Document loadXml(String xmlPath) 
			throws JDOMException, IOException {
		return (Document) SAX_BUILDER.build(new File(xmlPath));
	}
	
	public static void saveXml(String xmlPath, Document document) 
			throws JDOMException, IOException {
		FileOutputStream fos = new FileOutputStream(new File(xmlPath));
		XMLOutputter out = new XMLOutputter();
		out.output(document, fos);
		fos.close();
	}
}