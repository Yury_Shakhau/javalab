package com.epam.xmlstrutsproject.controller.action;

public final class ActionMethod {

	private ActionMethod() {}
	
	public static final String ADDING_METHOD = "/addingProduct.do";
	
	public static final String ADD_METHOD = "/addProduct.do";
	
	public static final String EDIT_METHOD = "/editProduct.do";
	
	public static final String EDITING_PRODUCTS_METHOD = "/editingProducts.do";
	
	public static final String PRODUCTS_METHOD = "/products.do";
}
