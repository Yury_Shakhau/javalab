package com.epam.xmlstrutsproject.model.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.epam.xmlstrutsproject.model.businesslayer.BusinesslayerException;
import com.epam.xmlstrutsproject.util.file.XmlWorker;

public class XmlRepository {

	private String xmlPath;

	private Document docCache;
	private ByteArrayOutputStream docArrayCache;

	private final ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();
	private final Lock LOCKER = new ReentrantLock();

	private static final int MAX_THREAD_COUNT = 256;
	private static final ThreadCounter THREAD_COUNTER = new ThreadCounter(
			MAX_THREAD_COUNT);

	private Document outputStreamToDocument() throws DataException {
		try {
			return new SAXBuilder().build(new ByteArrayInputStream(
					docArrayCache.toByteArray()));
		} catch (JDOMException | IOException e) {
			throw new DataException(e);
		}
	}

	private void saveDocument() throws DataException {

		try {
			XmlWorker.saveXml(xmlPath, docCache);
		} catch (JDOMException | IOException e) {
			throw new DataException(e);
		}
	}

	private ByteArrayInputStream documentToInputStream() {
		XMLOutputter xmlOut = new XMLOutputter();
		return new ByteArrayInputStream(xmlOut.outputString(docCache)
				.getBytes());
	}
	
	public Document loadDocument() throws BusinesslayerException {

		if (xmlPath == null) {
			throw new BusinesslayerException("xmlPath must be not null.");
		}

		Lock lock = READ_WRITE_LOCK.readLock();
		lock.lock();
		try {
			if (docCache == null) {

				LOCKER.lock();
					try {
						if (docCache == null) {
							docCache = XmlWorker.loadXml(xmlPath);
						}
					} catch (JDOMException | IOException e) {
						docCache = null;
						throw new BusinesslayerException(e);
					} finally {
						LOCKER.unlock();
					}

			}
		} finally {
			lock.unlock();
		}

		return docCache;
	}

	public Document loadDocument(String xmlPath) throws BusinesslayerException {
		this.xmlPath = xmlPath;
		return loadDocument();
	}
	
	public void saveDocument(Document document) 
			throws DataException {
		
		if (document != docCache) {
			throw new DataException(document.getBaseURI()
					+ " is not such document " + docCache.getBaseURI());
		}

		docCache = document;
		saveDocument();
	}
	
	public void saveDocument(Document document, String xmlPath)
			throws DataException {

		if (xmlPath == null) {
			throw new DataException("xmlPath must be not null.");
		}

		if (!this.xmlPath.equals(xmlPath)) {
			throw new DataException(
					this.xmlPath
							+ " and "
							+ xmlPath
							+ " must be equal. You can not save an xml to not same path as you loaded.");
		}
		
		saveDocument(document);
	}

	public void transformDocument(Transformer transformer) throws DataException {

		boolean neadUnlock = false;
		Lock lock = READ_WRITE_LOCK.writeLock();

		try {
			THREAD_COUNTER.nextThread();
			lock.lock();
			neadUnlock = true;
				ByteArrayInputStream inputXmlBuf = documentToInputStream();
				StreamSource source = new StreamSource(inputXmlBuf);
				docArrayCache = new ByteArrayOutputStream();
				transformer.transform(source, new StreamResult(docArrayCache));
				docCache = outputStreamToDocument();
				if (THREAD_COUNTER.isNotFirstOrSingleThread()) {
					if (THREAD_COUNTER.isThatLastOrCountedThread()) {
						saveDocument();
					}
				}
				THREAD_COUNTER.threadGone();
			neadUnlock = false;
			lock.unlock();

		} catch (TransformerException e) {
			throw new DataException(e);
		} finally {
			if (neadUnlock) {
				THREAD_COUNTER.threadGone();
				lock.unlock();
			}
		}
	}
}
