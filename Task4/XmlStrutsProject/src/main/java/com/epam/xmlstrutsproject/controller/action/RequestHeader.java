package com.epam.xmlstrutsproject.controller.action;

import javax.servlet.http.HttpServletRequest;

public final class RequestHeader {
	
	public static final String REFERER = "referer";
	
	private RequestHeader() {}
	
	public static String currentUrl(HttpServletRequest request) {
		return request.getHeader(REFERER);
	}
}
