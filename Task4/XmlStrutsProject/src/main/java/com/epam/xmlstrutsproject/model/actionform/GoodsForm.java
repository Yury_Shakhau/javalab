package com.epam.xmlstrutsproject.model.actionform;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;
import org.jdom.Document;
import org.jdom.Element;

import com.epam.xmlstrutsproject.controller.action.ActionMethod;
import com.epam.xmlstrutsproject.model.data.entity.Product;

public class GoodsForm extends ValidatorForm {

	/**
	 * For correct deserialization.
	 */
	private static final long serialVersionUID = 1841778505717543350L;

	public static final String CATEGORY_NAME = "categoryName";
	public static final String SUBCATEGORY_NAME = "subcategoryName";

	private Document document;

	private String categoryName;
	private String subcategoryName;

	private String backUrl;

	private Product product;

	public GoodsForm() {
		product = new Product();
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubcategoryName() {
		return subcategoryName;
	}

	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String getBackUrl() {
		return backUrl;
	}

	public void setBackUrl(String backPath) {
		if (backPath != null
				&& (backPath.contains(ActionMethod.PRODUCTS_METHOD) || backPath
						.contains(ActionMethod.EDITING_PRODUCTS_METHOD))) {
			this.backUrl = backPath;
		}
	}
	
	@SuppressWarnings("unchecked")
	public int productCount(String attributeName, String categoryName, String subCategoryName) {
		int elSum = 0;
		List<Element> categories = document.getRootElement().getChildren();
		for (Element categEl : categories) {
			if (categEl.getAttribute(attributeName).getValue().equals(categoryName)) {
				List<Element> subcategories = categEl.getChildren();
				if (subCategoryName != null && subCategoryName.length() > 0) {
					for (Element subcategEl : subcategories) {
						if (subcategEl.getAttribute(attributeName).getValue().equals(subCategoryName)) {
							elSum += subcategEl.getChildren().size();
						}
					}
				} else {
					for (Element subcategEl : subcategories) {
						elSum += subcategEl.getChildren().size();
					}
				}
			}
		}
		return elSum;
	}
}