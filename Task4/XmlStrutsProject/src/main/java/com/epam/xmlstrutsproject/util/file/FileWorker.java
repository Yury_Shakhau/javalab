package com.epam.xmlstrutsproject.util.file;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class FileWorker {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(FileWorker.class);
	
	public static void writeFile(String filePath, OutputStream stream) {
		Writer writer = null;
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(filePath);
			writer = new OutputStreamWriter(os);
			writer.write(stream.toString());
		} catch (IOException e) {
			LOGGER.error("Writer can not write file " + filePath, e);
		} finally {
			if (writer != null) {
				try {
					writer.flush();
					writer.close();
					os.flush();
					os.close();
				} catch (IOException e) {
					LOGGER.error("StreamWriter can not be closed for file " + filePath, e);
				}
			}
		}
	}
	
	public static String fullPath(HttpServletRequest request, String fileSubPath) {
		String realPath = request.getServletContext().getRealPath("/");
		return realPath + fileSubPath.substring(1);
	}
}
