package com.epam.xmlstrutsproject.model.data.entity.validation;

import resource.ConfigManager;

public final class ProductPattern {

	private ProductPattern() {}
	
	public static final String NAME = "product.name.pattern";
	
	public static final String PROVIDER = "product.provider.pattern";
	
	public static final String MODEL = "product.model.pattern";
	
	public static final String DATE_OF_ISSUE = "product.dateOfIssue.pattern";
	
	public static final String COLOR = "product.color.pattern";
	
	public static final String PRICE = "product.price.pattern";
	
	public static final String NAME_PATTERN;
	
	public static final String PROVIDER_PATTERN;
	
	public static final String MODEL_PATTERN;
	
	public static final String DATE_OF_ISSUE_PATTERN;
	
	public static final String COLOR_PATTERN;
	
	public static final String PRICE_PATTERN;

	static {
		NAME_PATTERN = ConfigManager.key(NAME);
		PROVIDER_PATTERN = ConfigManager.key(PROVIDER);
		MODEL_PATTERN = ConfigManager.key(MODEL);
		DATE_OF_ISSUE_PATTERN = ConfigManager.key(DATE_OF_ISSUE);
		COLOR_PATTERN = ConfigManager.key(COLOR);
		PRICE_PATTERN = ConfigManager.key(PRICE);
	}
}
