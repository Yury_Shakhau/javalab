<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nested:define id="categoryNameSended" name="goodsForm" property="categoryName" />

<html>
	<head>
		<title>Catalog of <nested:write name="goodsForm" property="categoryName" /></title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script src="js/script.js"></script>
	</head>
	<body>
		<center>
			<div class="center-body">
				<div class="title">Catalog of <nested:write name="goodsForm" property="categoryName" /></div>
					<div class="main-link">
						<a class="inline" href="categories.do">
							Catalog
						</a>
						<a class="button-link" href="categories.do">
							<div class="button">
								Back
							</div>
						</a>
					</div>
				<div class="menu-item-title">
					<div class="menu-item-name">Name</div>
					<div class="menu-item-count">Product count</div>
				</div>
				<nested:iterate name="goodsForm" property="document.rootElement.children">
					<nested:equal property="attribute(name).value" value="${ categoryNameSended }">
						<nested:iterate property="children">
							<nested:define id="subcategoryName" property="attribute(name).value"/>
							<a class="menu-link" href="products.do?categoryName=${ categoryNameSended }&amp;subcategoryName=${ subcategoryName }">
								<div class="menu-item">
									<div class="menu-item-name">
										${subcategoryName}
									</div>
									<div class="menu-item-count">
										${goodsForm.productCount('name', categoryNameSended, subcategoryName)}
									</div>
								</div>
							</a>
						</nested:iterate>
					</nested:equal>
				</nested:iterate>
			</div>
		</center>
	</body>
</html>