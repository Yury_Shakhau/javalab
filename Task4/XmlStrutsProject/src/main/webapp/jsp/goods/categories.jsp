<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title>Catalog</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script src="js/script.js"></script>
	</head>
	<body>
		<center>
			<div class="center-body">
				<div class="title">Catalog</div>
				<div class="menu-item-title">
					<div class="menu-item-name">Name</div>
					<div class="menu-item-count">Product count</div>
				</div>
				<nested:iterate name="goodsForm" property="document.rootElement.children">
					<nested:define id="categoryName" property="attribute(name).value"/>
					<nested:link styleClass="menu-link" paramId="categoryName" paramName="categoryName" 
								 property="categoryName" action="subcategories" name="goodsForm">
						<div class="menu-item">
							<div class="menu-item-name">
								${categoryName}
							</div>
							<div class="menu-item-count">
								${goodsForm.productCount('name', categoryName, null)}
							</div>
						</div>
					</nested:link>
				</nested:iterate>
			</div>
		</center>
	</body>
</html>