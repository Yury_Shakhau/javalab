<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nested:define id="categoryNameSended" name="goodsForm" property="categoryName" />
<nested:define id="subcategoryNameSended" name="goodsForm" property="subcategoryName" />
<nested:define id="backUrl" name="goodsForm" property="backUrl" />

<nested:define id="product" name="goodsForm" property="product" />

<html>
	<head>
		<title>Adding to <nested:write name="goodsForm" property="subcategoryName" /></title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script src="js/script.js"></script>
	</head>
	<body>
		<center>
			<div class="center-body">
				<div class="title">Adding to <nested:write name="goodsForm" property="subcategoryName" /></div>
				<div class="main-link">
					<a class="inline" href="categories.do">
						Catalog
					</a>
					<div class="inline">→</div>
					<a class="inline" href="subcategories.do?categoryName=${categoryNameSended}">
						Catalog of ${categoryNameSended}
					</a>
					<div class="inline">→</div>
					<a class="inline" href="products.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
						Catalog of ${subcategoryNameSended}
					</a>
					<a class="button-link" href="${backUrl}">
						<div class="button">
							Back
						</div>
					</a>
				</div>
				<form method="post" action="addProduct.do">
					<div>
						<div class="menu-item-product">
							<div class="adding-title">Name</div>
							<div class="adding-field">
								<nested:text styleId="input-name" styleClass="adding-field-input" 
											 property="product.name" name="goodsForm" value="${product.name}" />
								<c:choose>
									<c:when test="${product.name != null && not product.isValidLength(product.name)}">
										<span class="warning-message">Name length must be greatter than 0.</span>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${not product.isValidName()}">
												<span class="warning-message">Name must contaice digits and latin letters only.</span>
											</c:when>
											<c:otherwise>
												<span id="name-warning" class="simple-text">Name must contaice digits and latin letters only.</span>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title">Provider</div>
							<div class="adding-field">
								<nested:text styleId="input-provider" styleClass="adding-field-input" 
											 property="product.provider" name="goodsForm" value="${product.provider}" />
								<c:choose>
									<c:when test="${product.provider != null && not product.isValidLength(product.provider)}">
										<span class="warning-message">Name length must be greatter than 0.</span>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${not product.isValidProvider()}">
												<span class="warning-message">Provider must contaice digits and latin letters only.</span>
											</c:when>
											<c:otherwise>
												<span id="provider-warning" class="simple-text">Provider must contaice digits and latin letters only.</span>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title">Model</div>
							<div class="adding-field">
								<nested:text styleId="input-model" styleClass="adding-field-input" 
											 property="product.model" name="goodsForm" value="${product.model}" />
								<c:choose>
									<c:when test="${not product.isValidModel()}">
										<span class="warning-message">Model must have only 2 latin letters and 3 digits after.</span>
									</c:when>
									<c:otherwise>
										<span id="model-warning" class="simple-text">Model must have only 2 latin letters and 3 digits after.</span>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title">Date of issue</div>
							<div class="adding-field">
								<nested:text styleId="input-dateOfIssue" styleClass="adding-field-input" 
											 property="product.dateOfIssue" name="goodsForm" value="${product.dateOfIssue}" />
								<c:choose>
									<c:when test="${not product.isValidDateOfIssue()}">
										<span class="warning-message">Date diapason form 01-01-1900 to 31-12-9999.</span>
									</c:when>
									<c:otherwise>
										<span id="dateOfIssue-warning" class="simple-text">Date diapason form 01-01-1900 to 31-12-9999.</span>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title">Color</div>
							<div class="adding-field">
								<nested:text styleId="input-color" styleClass="adding-field-input" 
											 property="product.color" name="goodsForm" value="${product.color}" />
								<c:choose>
									<c:when test="${product.color != null && not product.isValidLength(product.color)}">
										<span class="warning-message">Color length must be greatter than 0.</span>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${not product.isValidColor()}">
												<span class="warning-message">Color must contaice latin letters only.</span>
											</c:when>
											<c:otherwise>
												<span id="color-warning" class="simple-text">Color must contaice latin letters only.</span>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title">Not in stock</div>
							<nested:notEqual name="product" property="notInStock" value="true">
								<nested:checkbox styleId="checkbox" styleClass="check-box" property="product.notInStock" 
												 name="goodsForm" onclick="showHideElement('price-div', this)" />
							</nested:notEqual>
							<nested:equal name="product" property="notInStock" value="true">
								<nested:checkbox styleId="checkbox" styleClass="check-box" property="product.notInStock" 
												 name="goodsForm"  onclick="showHideElement('price-div', this)" />
							</nested:equal>
						</div>
						<div class="menu-item-product" id="price-div">
							<div class="adding-title">Price</div>
							<div class="adding-field">
								<nested:text styleId="input-price" styleClass="adding-field-input" 
											 property="product.price" name="goodsForm" value="${product.price}" />
								<c:choose>
									<c:when test="${product.price != null && not product.isValidLength(product.price)}">
										<span class="warning-message">Price length must be greatter than 0.</span>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${not product.isValidPrice()}">
												<span class="warning-message">Price must be is a real positive number.</span>
											</c:when>
											<c:otherwise>
												<span id="price-warning" class="simple-text">Price must be is a real positive number.</span>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="menu-item-product">
							<div class="adding-title"></div>
							<div class="adding-field">
								<input class="button adding-button-input" type="submit" value="Add product"/>
							</div>
						</div>
					</div>
				</form>
			</div>
		</center>
	</body>
	<script type="text/javascript">
		window.onload = visibleElement('price-div', 'checkbox');
		window.onload = changeColorToGreenById('input-name', 'name-warning');
		window.onload = changeColorToGreenById('input-provider', 'provider-warning');
		window.onload = changeColorToGreenById('input-model', 'model-warning');
		window.onload = changeColorToGreenById('input-dateOfIssue', 'dateOfIssue-warning');
		window.onload = changeColorToGreenById('input-color', 'color-warning');
		window.onload = changeColorToGreenById('input-price', 'price-warning');
	</script>
</html>