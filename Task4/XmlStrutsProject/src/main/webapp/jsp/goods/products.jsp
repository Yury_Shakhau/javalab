<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nested:define id="categoryNameSended" name="goodsForm" property="categoryName" />
<nested:define id="subcategoryNameSended" name="goodsForm" property="subcategoryName" />

<html>
	<head>
		<title>Catalog of ${subcategoryNameSended}</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script src="js/script.js"></script>
	</head>
	<body>
		<center>
			<div class="center-body">
				<div class="title">Catalog of ${subcategoryNameSended}</div>
					<div class="main-link">
						<a class="inline" href="categories.do">
							Catalog
						</a>
						<div class="inline">→</div>
						<a class="inline" href="subcategories.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
							Catalog of ${categoryNameSended}
						</a>
						<a class="button-link" href="subcategories.do?categoryName=${categoryNameSended}">
							<div class="button">
								Back
							</div>
						</a>
						<a class="button-link" href="editingProducts.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
							<div class="button right-margin">
								Edit products
							</div>
						</a>
						<a class="button-link" href="addingProduct.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
							<div class="button right-margin">
								Add product
							</div>
						</a>
					</div>
				<div class="menu-item-title">
					<div class="menu-item-product-name">Name</div>
					<div class="menu-item-product-provider">Provider</div>
					<div class="menu-item-product-model">Model</div>
					<div class="menu-item-product-date-of-issue">Date of issue</div>
					<div class="menu-item-product-color">Color</div>
					<div class="menu-item-product-price">Price</div>
				</div>
				<nested:iterate name="goodsForm" property="document.rootElement.children">
					<nested:equal property="attribute(name).value" value="${categoryNameSended}">
						<nested:iterate property="children">
							<nested:equal property="attribute(name).value" value="${subcategoryNameSended}">
								<nested:iterate id="product" property="children">
									<div class="menu-item-product">
										<div class="menu-value-product-name">
											<nested:write name="product" property="attribute(name).value" /></div>
										<div class="menu-value-product-provider">
											<nested:write name="product" property="child(provider).text" />
										</div>
										<div class="menu-value-product-model">
											<nested:write name="product" property="child(model).text" />
										</div>
										<div class="menu-value-product-date-of-issue">
											<nested:write name="product" property="child(date-of-issue).text" />
										</div>
										<div class="menu-value-product-color">
											<nested:write name="product" property="child(color).text" />
										</div>
										<div class="menu-value-product-price">
											<c:choose>
												<c:when test="${product.getChild('not-in-stock') == null}">
													<nested:define id="priceTag" name="product" property="child(price)"/>
													${priceTag.getText()}
												</c:when>
												<c:otherwise>
													Not in stock
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</nested:iterate>
							</nested:equal>
						</nested:iterate>
					</nested:equal>
				</nested:iterate>
			</div>
		</center>
	</body>
</html>