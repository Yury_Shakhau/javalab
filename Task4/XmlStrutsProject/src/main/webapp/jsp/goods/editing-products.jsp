<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<nested:define id="categoryNameSended" name="goodsForm" property="categoryName" />
<nested:define id="subcategoryNameSended" name="goodsForm" property="subcategoryName" />

<html>
	<head>
		<title>Editing of ${subcategoryNameSended}</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/editing-products-style.css" rel="stylesheet" type="text/css" />
		<script src="js/script.js"></script>
		<script src="js/validation.jsp"></script>
	</head>
	<body>
		<center>
			<div class="center-body">
				<div class="title">Editing of ${subcategoryNameSended}</div>
				<div class="main-link">
					<a class="inline" href="categories.do">
						Catalog
					</a>
					<div class="inline">→</div>
					<a class="inline" href="subcategories.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
						Catalog of ${subcategoryNameSended}
					</a>
					<div class="inline">→</div>
					<a class="inline" href="products.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
						Catalog of ${subcategoryNameSended}
					</a>
					<a class="button-link" href="products.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
						<div class="button">
							Back
						</div>
					</a>
					<div class="button-link">
						<button class="button right-margin input-submit" 
							    onclick="return submitProducts('form-to-send', 'menu-item-product', 'menu-value-product-not-in-stock', 'menu-value-product-price', 'error-message')">
							Save
						</button>
					</div>
					<a class="button-link" href="addingProduct.do?categoryName=${categoryNameSended}&amp;subcategoryName=${subcategoryNameSended}">
						<div class="button right-margin">
							Add product
						</div>
					</a>
				</div>
				<div class="menu-item-title">
					<div class="menu-item-product-name">Name</div>
					<div class="menu-item-product-provider">Provider</div>
					<div class="menu-item-product-model">Model</div>
					<div class="menu-item-product-date-of-issue">Date of issue</div>
					<div class="menu-item-product-color">Color</div>
					<div class="menu-item-product-not-in-stock">Not in stock</div>
					<div class="menu-item-product-price">Price</div>
				</div>
				<form id="form-to-send" method="post" action="saveProducts.do">
					<input type="hidden" name="categoryName" value="${categoryNameSended}"/>
					<input type="hidden" name="subcategoryName" value="${subcategoryNameSended}"/>
					<nested:iterate name="goodsForm" property="document.rootElement.children">
						<nested:equal property="attribute(name).value" value="${categoryNameSended}">
							<nested:iterate property="children">
								<nested:equal property="attribute(name).value" value="${subcategoryNameSended}">
									<nested:iterate id="product" property="children">
										<div class="menu-item-product">
											<div class="error-message"></div>
											<div class="menu-value-product-name">
												<nested:text property="attribute(name).value" styleClass="editing-name-input" />
											</div>
											<div class="menu-value-product-provider">
												<nested:text property="child(provider).text" styleClass="editing-provider-input" />
											</div>
											<div class="menu-value-product-model">
												<nested:text property="child(model).text" styleClass="editing-model-input" />
											</div>
											<div class="menu-value-product-date-of-issue">
												<nested:text property="child(date-of-issue).text" styleClass="editing-date-of-issue-input" />
											</div>
											<div class="menu-value-product-color">
												<nested:text property="child(color).text" styleClass="editing-color-input" />
											</div>
											<c:choose>
												<c:when test="${product.getChild('price').getText() != ''}">
													<div class="menu-value-product-not-in-stock">
														<nested:checkbox styleClass="check-box chek-box-not-in-stock" property="child(not-in-stock)" 
																		 onclick="notInStock(this, 'menu-value-product-price')" />
													</div>
												</c:when>
												<c:otherwise>
													<div class="menu-value-product-not-in-stock">
														<input type="checkbox" class="check-box chek-box-not-in-stock" checked="checked" 
															   onclick="notInStock(this, 'menu-value-product-price')" name="products.notInStock" />
													</div>
												</c:otherwise>
											</c:choose>
											<div class="menu-value-product-price">
												<nested:define id="priceName" property="child(price).text" />
												<nested:text property="child(price).text" styleClass="editing-price-input" />
												<div name="notInStockMessage" hidden="hidden">Not in stock</div>
											</div>
										</div>
									</nested:iterate>
								</nested:equal>
							</nested:iterate>
						</nested:equal>
					</nested:iterate>
				</form>
			</div>
		</center>
	</body>
	<script type="text/javascript">
		window.onload = visiblePrices('menu-item-product', 'menu-value-product-not-in-stock', 'menu-value-product-price');
	</script>
</html>