var map;
var index = 0;

function findElementByName(rootElement, attributeName, name) {
	var children = rootElement.children;
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		if (name === child.getAttribute(attributeName)) {
			return child;
		}
		var retEl = findElementByName(child, name);
		if (retEl != null) {
			return retEl;
		}
	}
	return null;
}

function findElementsForName(rootElement, attributeName, name) {
	var children = rootElement.children;
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		var elName = child.getAttribute(attributeName);
		if (name === elName) {
			map.set(index, child);
			index++;
		}
		findElementsForName(child, attributeName, name);
	}
}

function findElementsByName(rootElement, attributeName, name) {
	map = new Map();
	index = 0;
	findElementsForName(rootElement, attributeName, name);
	return map;
}

function showHideElement(id, checkBox) {
	var element = document.getElementById(id);
	var display = checkBox.checked ? 'none' : 'block';
	element.style.display = display;
}

function visibleElement(id, idCheckBox) {
	var checkBox = document.getElementById(idCheckBox);
	showHideElement(id, checkBox);
}

function visiblePrices(className, checkBoxClass, contClass) {
	var body = document.getElementsByTagName('body')[0]
	var containers = findElementsByName(body, 'class', className);
	for (var i = 0; i < index; i++) {
		var container = containers.get(i);
		var checkBox = findElementByName(container, 'class', checkBoxClass).children[0];
		var display = checkBox.checked ? 'none' : 'block';
		var notDisplay = checkBox.checked ? 'block' : 'none';
		var priceCont = findElementByName(container, 'class', contClass);
		priceCont.children[0].style.display = display;
		priceCont.children[1].style.display = notDisplay;
	}
}

function changeColorToGreen(textBox, textElement) {
	if (textElement != null) {
		if (textBox.value.length > 0) {
			textElement.style.color = '#00FF00';
		}
	}
}

function changeColorToGreenById(textBoxId, textElementId) {
	var textBox = document.getElementById(textBoxId);
	var textElement = document.getElementById(textElementId);
	changeColorToGreen(textBox, textElement);
}

function changeColorToGreenWithId(textBox, textElementId) {
	var textElement = document.getElementById(textElementId);
	changeColorToGreen(textBox, textElement);
}

function notInStock(checkBox, elName, notInName) {
	var container = checkBox.parentElement.parentElement;
	var priceContainer = findElementByName(container, 'class', elName);
	var children = priceContainer.children;
	var display = checkBox.checked ? 'none' : 'block';
	var notDisplay = checkBox.checked ? 'block' : 'none';
	children[0].style.display = display;
	children[1].style.display = notDisplay;
}