<%@ page language="java" contentType="text/javascript; charset=utf-8" pageEncoding="utf-8"%>
	
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

var productNameError = '<bean:message key="product.name.error"/>';
var productProviderError = '<bean:message key="product.provider.error"/>';
var productModelError = '<bean:message key="product.model.error"/>';
var productDateOfIssueError = '<bean:message key="product.dateOfIssue.error"/>';
var productColorError = '<bean:message key="product.color.error"/>';
var productPriceError = '<bean:message key="product.price.error"/>';

var productLengthError = '<bean:message key="product.length.error"/>';

var errorCount = 0;

function errorMessage(element, pattern, nullLength, notValid) {
	var message = '';
	if (element.value.length == 0) {
		errorCount++;
		message += nullLength;
		result = false;
	} else {
		var pat = pattern;
		if (!pat.test(element.value)) {
			errorCount++;
			message += notValid;
			result = false;
		}
	}
	if (message.length > 0) {
		message = toDiv(errorCount + '. ' +message);	
	}
	return message;
}

function toDiv(message) {
	return '<div>' + message + '</div>';
}

function validProducts(containers, checkBoxClass, contClass, errClass, checkBoxClass) {
	errorCount = 0;
	var valid = true;
	for (var i = 0; i < index; i++) {
		var message = '';
		var container = containers.get(i);
		var errBox = findElementByName(container, 'class', errClass);
		
		var nameE = findElementByName(container, 'class', 'menu-value-product-name').children[0];
		message += errorMessage(nameE, /^[a-z A-Z0-9\-]+$/, 
				'Name ' + productLengthError, productNameError);
		
		var providerE = findElementByName(container, 'class', 'menu-value-product-provider').children[0];
		message += errorMessage(providerE, /^[a-z A-Z0-9\-]+$/, 
				'Provider ' + productLengthError, productProviderError);
		
		var modelE = findElementByName(container, 'class', 'menu-value-product-model').children[0];
		message += errorMessage(modelE, /^[A-Za-z]{2}[\d]{3}$/, 
				'', productModelError);
		
		var dateOfIssueE = findElementByName(container, 'class', 'menu-value-product-date-of-issue').children[0];
		message += errorMessage(dateOfIssueE, /(0[1-9]|[12]\d|3[01])-((0[1-9])|(1[012]))-((19\d\d)|([2-9]\d\d\d))/, 
				'Date ' + productLengthError, productDateOfIssueError);
		
		var colorE = findElementByName(container, 'class', 'menu-value-product-color').children[0];
		message += errorMessage(colorE, /^[A-Za-z]+$/, 
				'Color ' + productLengthError, productColorError);
		var checkBox = findElementByName(container, 'class', checkBoxClass).children[0];
		
		if (!checkBox.checked) {
			var priceE = findElementByName(container, 'class', 'menu-value-product-price').children[0];
			message += errorMessage(priceE, /^[0-9\.{1}]+$/, 
					'Price ' + productLengthError, productPriceError);
		}
		errBox.innerHTML = message;
		if (message.length > 0) {
			valid = false;
		}
	}
	return valid;
}

function submitProducts(formName, sumContClass, checkBoxClass, contClass, errClass) {
	var form = document.getElementById(formName);
	var containers = findElementsByName(form, 'class', sumContClass);
	var valid = validProducts(containers, checkBoxClass, contClass, errClass, checkBoxClass);
	if (valid) {
		for (var i = 0; i < index; i++) {
			var container = containers.get(i);
			var checkBox = findElementByName(container, 'class', checkBoxClass).children[0];
			if (checkBox.checked) {
				var priceCont = findElementByName(container, 'class', contClass);
				priceCont.children[0].value = '';
			}
		}
		form.submit();
	}
	return valid;
}