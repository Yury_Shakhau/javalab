<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:product="xalan//:com.epam.xmlstrutsproject.model.data.entity.Product">

	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />
	
	<xsl:param name="product" />

	<xsl:template match="@*|node()">

		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>

	</xsl:template>


	<xsl:template
		match="category[@name=$categoryName]/subcategory[@name=$subcategoryName]">

		<xsl:variable name="name" select="product:getName($product)" />
		<xsl:variable name="provider" select="product:getProvider($product)" />
		<xsl:variable name="model" select="product:getModel($product)" />
		<xsl:variable name="dateOfIssue" select="product:getDateOfIssue($product)" />
		<xsl:variable name="color" select="product:getColor($product)" />
		<xsl:variable name="price" select="product:getPrice($product)" />
		<xsl:variable name="notInStock" select="product:isNotInStock($product)" />

		<xsl:copy>

			<xsl:apply-templates select="@*|node()" />

			<xsl:element name="product">
				<xsl:attribute name="name">
					<xsl:value-of select="$name" />
				</xsl:attribute>
				<xsl:element name="model">
					<xsl:value-of select="$model" />
				</xsl:element>
				<xsl:element name="provider">
					<xsl:value-of select="$provider" />
				</xsl:element>
				<xsl:element name="date-of-issue">
					<xsl:value-of select="$dateOfIssue" />
				</xsl:element>
				<xsl:element name="color">
					<xsl:value-of select="$color" />
				</xsl:element>
				<xsl:choose>
					<xsl:when test="not($notInStock)">
						<xsl:element name="price">
							<xsl:value-of select="$price" />
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="price" />
						<xsl:element name="not-in-stock" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>

		</xsl:copy>

	</xsl:template>


</xsl:stylesheet>