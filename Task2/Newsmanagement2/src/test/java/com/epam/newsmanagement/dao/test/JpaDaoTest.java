package com.epam.newsmanagement.dao.test;

import org.springframework.test.context.ContextConfiguration;

/**
 * Jpa dao test. Implements abstract dao test.
 * 
 * @author Yury_Shakhau
 *
 */
@ContextConfiguration(locations = { "classpath:testJpaContext.xml" })
public class JpaDaoTest extends DaoTest {

}
