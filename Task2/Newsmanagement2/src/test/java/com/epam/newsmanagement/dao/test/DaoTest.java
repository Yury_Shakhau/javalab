package com.epam.newsmanagement.dao.test;

import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.DatabaseUnitException;
import org.dbunit.IDatabaseTester;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.entity.News;
import com.epam.newsmanagement.util.converter.DateConverter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DaoTest extends DBTestCase {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static Logger logger = Logger.getLogger(JdbcDaoTest.class);

	private static final String DATASET_PATH = "dataset.xml";

	private static final String DATASET_LIST_PATH = "datasetList.xml";

	private static final String DATASET_ADD_PATH = "datasetAdd.xml";

	private static final String DATASET_SAVE_PATH = "datasetSave.xml";

	private static final String DATASET_DELETE_MANY_PATH = "datasetDeleteMany.xml";
	
	/**
	 * Sql for select method.
	 */
	public static final String SQL_SELECT_ALL = "SELECT " + News.TITLE + ","
			+ News.DATE_CREATE + "," + News.BRIEF + "," + News.CONTENT + ","
			+ News.ACTIVE + " FROM " + News.TABLE_NAME + " " + "WHERE "
			+ News.ACTIVE + "=" + News.ENABLE + " ORDER BY " + News.DATE_CREATE
			+ " ASC";

	private static final String DATE_PATTERN = "yyyy.MM.dd";

	@Autowired
	private INewsDao newsDao;

	@Autowired
	private IDatabaseTester tester;

	private IDataSet dataSet;

	/**
	 * Inserts data to db from xml.
	 */
	@Before
	@Override
	public void setUp() throws Exception {
		dataSet = createDataSet(DATASET_PATH);
		tester.setDataSet(dataSet);
		tester.onSetup();
	}

	/**
	 * Gets all notes from db.
	 */
	@Test
	public void testList() {
		try {
			List<News> allNews = newsDao.findAll();
			// Load expected data from an XML dataset
			IDataSet expectedDataSet = createDataSet(DATASET_LIST_PATH);
			IDataSet checkDataSet = createDatabaseDataSet();

			// Assert actual database table match expected tables
			assertEquals(allNews.size(), checkDataSet.getTable(News.TABLE_NAME)
					.getRowCount());
			Assertion.assertEquals(expectedDataSet, checkDataSet);
		} catch (DaoException | DatabaseUnitException e) {
			logger.error(e);
		}
	}

	@Test
	public void testAdd() {
		try {
			News entity = new News("title6", DateConverter.convertDate(
					"2015.01.06", DATE_PATTERN), "brief6", "content6");

			// Adding to db and getting its id.
			newsDao.add(entity).getId();

			compateDbAndXml(DATASET_ADD_PATH);
		} catch (DaoException e) {
			logger.error(e);
		}
	}

	@Test
	public void testSave() {
		try {

			// Updating in db.
			News entity = new News();
			entity.setId(3);
			entity.setTitle("title10");
			entity.setBrief("brief10");
			entity.setContent("content10");
			entity.setDateCreate(DateConverter.convertDate("2015.01.03",
					DATE_PATTERN));
			
			// Updating in db with id.
			newsDao.save(entity);

			compateDbAndXml(DATASET_SAVE_PATH);
		} catch (DaoException e) {
			logger.error(e);
		}
	}

	@Test
	public void testDeleteMany() {
		try {

			// Adding to db.
			newsDao.deleteMany(new Long[] { 1L, 3L, 5L });

			compateDbAndXml(DATASET_DELETE_MANY_PATH);
		} catch (DaoException e) {
			logger.error(e);
		}
	}
	
	private void compateDbAndXml(String dsPath) throws DaoException {
		try {
			// Load expected data from an XML dataset
			IDataSet expectedDataSet = createDataSet(dsPath);
			IDataSet checkDataSet = createDatabaseDataSet();

			// Getting tables expected and founded.
			ITable exTable = expectedDataSet.getTable(News.TABLE_NAME);
			ITable fTable = checkDataSet.getTable(News.TABLE_NAME);

			// Assert actual database table match expected tables
			assertEquals(exTable.getRowCount(), fTable.getRowCount());
			Assertion.assertEquals(expectedDataSet, checkDataSet);
		} catch (DaoException e) {
			throw e;
		} catch (DatabaseUnitException e) {
			throw new DaoException(e);
		}
	}

	@After
	@Override
	public void tearDown() throws Exception {
		DatabaseOperation.DELETE_ALL.execute(tester.getConnection(), dataSet);
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return dataSet;
	}

	private IDataSet createDataSet(String path) throws DataSetException {
		return new XmlDataSet(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(path));
	}

	private IDataSet createDatabaseDataSet() throws DaoException {
		QueryDataSet ds = null;
		try {
			ds = new QueryDataSet(tester.getConnection());
			ds.addTable(News.TABLE_NAME, SQL_SELECT_ALL);
		} catch (Exception e) {
			throw new DaoException(e);
		}
		return ds;
	}
}
