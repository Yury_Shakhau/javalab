package com.epam.newsmanagement.dao.test;

import org.springframework.test.context.ContextConfiguration;

/**
 * Jdbc dao test. Implements abstract dao test.
 * 
 * @author Yury_Shakhau
 *
 */
@ContextConfiguration(locations = { "classpath:testJdbcContext.xml" })
public class JdbcDaoTest extends DaoTest {

}
