package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.entity.News;

/**
 * Extends for delete many method implementation.
 * 
 * @author Yury Shakhau
 *
 */
public interface INewsDao extends IDao<News> {

	
}
