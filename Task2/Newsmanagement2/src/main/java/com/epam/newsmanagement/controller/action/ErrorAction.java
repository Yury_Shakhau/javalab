package com.epam.newsmanagement.controller.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.epam.newsmanagement.controller.ControllerExeption;

/**
 * This action calls when error comes.
 * 
 * @author Yury_Shakhau
 *
 */
public class ErrorAction extends Action {

	/**
	 * For calling for errors.
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
		throws ControllerExeption {
		
		return mapping.findForward(ActionConstant.SUCCESS_PATH);
	}
}
