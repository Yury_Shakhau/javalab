package com.epam.newsmanagement.controller.action;

public final class ActionConstant {

	private ActionConstant() {}
	
	/** Message in success case action forward */
	public static final String SUCCESS_PATH = "success";
	
	/** Message in success case action forward */
	public static final String FAILURE_PATH = "failure";
	
	/** Edit action string. Sub url. */
	public static final String EDIT_ACTION = "/edit.do";
}
