package com.epam.newsmanagement.util.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * 
 * 
 * @author Yury_Shkahau
 *
 */
public final class DateConverter {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger logger = Logger.getLogger(DateConverter.class);
	
	private DateConverter() {}
	
	/**
	 * Converts String to sql.Date
	 * 
	 * @param date
	 * @param datePattern datePattern pattern to convert String to date.
	 * @return
	 */
	public static java.sql.Date convertDate(String date, String datePattern) {
		java.sql.Date retDate = null;
		DateFormat format = new SimpleDateFormat(datePattern);
		try {
			java.util.Date uDate = format.parse(date);
			retDate = new java.sql.Date(uDate.getTime());
		} catch (ParseException e) {
			logger.error("Can not convert string " + date + " to date", e);
		}
		return retDate;
	}
	
	/**
	 * Converts util.Date to String by pattern datePAttern
	 * 
	 * @param date
	 * @param datePattern pattern to convert date to String.
	 * @return
	 */
	public static String convertDate(java.util.Date date, String datePattern) {
		DateFormat format = new SimpleDateFormat(datePattern);
		return  format.format(date);
	}
}
