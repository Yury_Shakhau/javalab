package com.epam.newsmanagement.controller.action;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.MappingDispatchAction;
import org.apache.struts.util.MessageResources;

import com.epam.newsmanagement.controller.ControllerExeption;
import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.entity.News;
import com.epam.newsmanagement.form.NewsForm;
import com.epam.newsmanagement.util.converter.DateConverter;

/**
 * Controller level class for executing messages from controller object.
 * 
 * @author Yury Shkhau
 *
 */
public class NewsAction extends MappingDispatchAction {
	
	private static final String REFERER = "Referer";
	
	private static final String DATE_PATTERN = "date.format";
	
	private static final String LANGUAGE = "language";

	private INewsDao newsDao;

	public INewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	private News findNewsById(NewsForm newsForm) throws ControllerExeption {
		News news = newsForm.getNews();
		long newsId = newsForm.getNews().getId();
		try {
			News fNews = newsDao.find(newsId);
			if (fNews != null) {
				news = fNews;
			}
		} catch (DaoException e) {
			throw new ControllerExeption(e);
		}
		return news;
	}
	
	private String getDatePattern(HttpServletRequest request) {
		MessageResources mrs = getResources(request);
		return mrs.getMessage(getLocale(request), DATE_PATTERN);
	}
	
	/**
	 * Redirects to list page.
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		NewsForm newsForm = (NewsForm) form;
		
		/* Getting all news and setting it to NewsForm */
		List<News> allNews = null;
		try {
			allNews = newsDao.findAll();
		} catch (DaoException e) {
			throw new ControllerExeption(e);
		}
		newsForm.setNewsList(allNews);

		return mapping.findForward(ActionConstant.SUCCESS_PATH);
	}
	
	/**
	 * Sets newsForm bean for view in page.
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;

		News news = findNewsById(newsForm);
		if (news != null) {
			newsForm.setNews(news);
			
			return mapping.findForward(ActionConstant.SUCCESS_PATH);
		}
		
		return mapping.findForward(ActionConstant.FAILURE_PATH);
	}
	
	/**
	 * Clears old newsForm bean for doing to add page.
	 */
	public ActionForward adding(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;

		newsForm.setNews(new News());
		newsForm.setDateCreate(DateConverter.convertDate(new Date(), getDatePattern(request)));
		
		return mapping.findForward(ActionConstant.SUCCESS_PATH);
	}
	
	/**
	 * Sets newsForm bean for edit in page.
	 */
	public ActionForward edit(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;
		News news = findNewsById(newsForm);
		if (news != null) {
			newsForm.setNews(news);
			String currentUrl = request.getHeader(REFERER);
			if (currentUrl != null && !currentUrl.contains(ActionConstant.EDIT_ACTION)) {
				newsForm.setCurrentUrl(currentUrl);
			}
			if (news.getId() != 0) {
				newsForm.setDateCreate(DateConverter.convertDate(news.getDateCreate(), 
						  getDatePattern(request)));
			}
			return mapping.findForward(ActionConstant.SUCCESS_PATH);
		}
		
		return mapping.findForward(ActionConstant.FAILURE_PATH);
	}
	
	/**
	 * Saves news entity in database.
	 */
	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;
		News news = newsForm.getNews();
		news.setDateCreate(DateConverter.convertDate(newsForm.getDateCreate(), 
				getDatePattern(request)));
		
		if (news.getId() == 0) {
			try {
				news = newsDao.add(news);
			} catch (DaoException e) {
				throw new ControllerExeption(e);
			}
			newsForm.setNews(news);
		} else {
			try {
				newsDao.save(news);
			} catch (DaoException e) {
				throw new ControllerExeption(e);
			}
		}
		
		return mapping.findForward(ActionConstant.SUCCESS_PATH);
	}
	
	/**
	 * Sets one or more news as delete in database.
	 */
	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;
		
		Long[] ids = newsForm.getSelectedId();
		if (ids != null) {
			try {
				newsDao.deleteMany(ids);
			} catch (DaoException e) {
				throw new ControllerExeption(e);
			}
		}
			
		return mapping.findForward(ActionConstant.SUCCESS_PATH);
	}
	
	/**
	 * For forwarding to 
	 */
	public ActionForward cancelSave(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		NewsForm newsForm = (NewsForm) form;
		
		String currentUrl = newsForm.getCurrentUrl();
		if (currentUrl != null) {
			return new ActionRedirect(currentUrl);
		}
		
		return mapping.findForward(ActionConstant.FAILURE_PATH);
	}
	
	public ActionForward changeLanguage(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ControllerExeption {
		
		DynaActionForm localeForm = (DynaActionForm) form;

		Locale locale = new Locale((String) localeForm.get(LANGUAGE));
		setLocale(request, locale);
		
		String url = request.getHeader(REFERER);
		return new ActionRedirect(url);
	}
}
