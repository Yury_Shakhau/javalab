package com.epam.newsmanagement.dao.connection;

import java.sql.Connection;
import java.sql.Statement;

public interface IConnectionPool {

	void init();
	
	void destroy();
	
	Connection takeConnection();
	
	void closeStatement(Statement st);
	
	void releaseConnection(Connection con);
}
