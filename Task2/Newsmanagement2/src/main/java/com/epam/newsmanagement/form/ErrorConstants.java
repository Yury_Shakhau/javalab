package com.epam.newsmanagement.form;

public final class ErrorConstants {
	
	private ErrorConstants() {}
	
	public static final String TITLE_REQUIRED = "errors.title.required";
	
	public static final String DATE_REQUIRED = "errors.date.required";
	
	public static final String BRIEF_REQUIRED = "errors.brief.required";
	
	public static final String CONTENT_REQUIRED = "errors.content.required";
	
	public static final String TITLE_GREATER_MAX_LENGTH = "errors.title.maxlength";
	
	public static final String BRIEF_GREATER_MAX_LENGTH = "errors.brief.maxlength";
	
	public static final String CONTENT_GREATER_MAX_LENGTH = "errors.content.maxlength";
	
	public static final String DATE_FORMAT = "errors.date.format";
	
	public static final String DATE_PATTERN = "date.format";
}
