package com.epam.newsmanagement.dao.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

/**
 * Entity for contains news information.
 * 
 * @author Yury_Shakhau
 */
@Entity
@Table(name = News.TABLE_NAME)
public class News implements Serializable {
	/**
	 * id for deserialization
	 */
	private static final long serialVersionUID = 2561208974361626253L;

	public News() { active = News.ENABLE; }

	public News(String title, Date dateCreate, String brief, String content) { 
		this.title = title;
		this.dateCreate = dateCreate;
		this.brief = brief;
		this.content = content;
		this.active = News.ENABLE; 
	}
	
	/**
	 * This table name.
	 */
	public static final String TABLE_NAME = "News";
	
	/**
	 * Identifier of this entity.
	 */
	public static final String ID = "id";
	
	/**
	 * Title of this news.
	 */
	public static final String TITLE = "title";
	
	/**
	 * Creation date of this news.
	 */
	public static final String DATE_CREATE = "dateCreate";
	
	/**
	 * Brief of this entity.
	 */
	public static final String BRIEF = "brief";
	
	/**
	 * Content of news.
	 */
	public static final String CONTENT = "content";
	
	/**
	 * If active is 0 then entity be not visible in reading.
	 */
	public static final String ACTIVE = "active";
	
	/**
	 * Enable status in db
	 */
	public static final int ENABLE = 1;
	
	/**
	 * Disable status in db
	 */
	public static final int DISABLE = 0;
	
	@Id
	@GenericGenerator(name = "auto_inc", strategy = "increment")
	@GeneratedValue(generator = "auto_inc")
	@Column(name = ID)
	private long id;

	@Column(name = TITLE)
	@NotNull
	@Size(min = 1, max = 100)
	private String title;

	@Column(name = DATE_CREATE)
	@NotNull
	private Date dateCreate;

	@Column(name = BRIEF)
	@NotNull
	@Size(min = 1, max = 500)
	private String brief;

	@Column(name = CONTENT)
	@NotNull
	@Size(min = 1, max = 2048)
	private String content;

	@Column(name = ACTIVE)
	@NotNull
	private Integer active;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer isActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brief == null) ? 0 : brief.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result
				+ ((dateCreate == null) ? 0 : dateCreate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (brief == null) {
			if (other.brief != null)
				return false;
		} else if (!brief.equals(other.brief))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (dateCreate == null) {
			if (other.dateCreate != null)
				return false;
		} else if (!dateCreate.equals(other.dateCreate))
			return false;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		int builderCapacity = (title != null ? title.length() : 0)
						    + (brief != null ? brief.length() : 0)
						    + (content != null ? content.length() : 0)
						    + 70;
		StringBuilder sb = new StringBuilder(builderCapacity);
		sb.append("News [id=")
		  .append(id)
		  .append(", title=")
		  .append(title)
		  .append(", dateCreate=")
		  .append(dateCreate)
		  .append(", brief=")
		  .append(brief)
		  .append(", content=")
		  .append(content)
		  .append("]");
		return sb.toString();
	}
}
