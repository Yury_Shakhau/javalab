package util.model.sql.query;

public class Operation extends Builder {
	
	private final StringBuilder query;
	
	public Operation(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	private static void appendVar(StringBuilder query, Object opearation, Object var) {
		query.append(opearation);
		if (var.equals(Query.UNKNOWN)) {
			query.append(var);
		} else {
			query.append('\'')
				 .append(var)
				 .append('\'');
		}
	}
	
	public Operation in(Object...var) {
		query.append(' ').append(Query.IN).append(' ');
		int lastIndex = var.length - 1;
		if (lastIndex > -1) {
			query.append('(');
			for (int i = 0; i < lastIndex; i++) {
				query.append(var[i]).append(',');
			}
			query.append(var[lastIndex]).append(')');
		}
		return this;
	}

	public Operation and(Object varName) {
		query.append(' ')
		 	 .append(Query.AND)
		 	 .append(' ')
		 	 .append(varName);
		return this;
	}
	
	public Operation or(Object varName) {
		query.append(' ')
		 	 .append(Query.OR)
		 	 .append(' ')
		 	 .append(varName);
		return this;
	}
	
	public JoinEqualQuery on(Object varName) {
		query.append(' ')
		 	 .append(Query.ON)
		 	 .append(' ')
		 	 .append(varName);
		return new JoinEqualQuery(query);
	}
	
	public Operation equal(Object var) {
		appendVar(query, Query.EQUALS, var);
		return this;
	}
	
	public Operation larger(Object var) {
		appendVar(query, Query.LARGER, var);
		return this;
	}
	
	public Operation less(Object var) {
		appendVar(query, Query.LESS, var);
		return this;
	}
	
	public Operation largerOrEqual(Object var) {
		appendVar(query, Query.LARGER_OR_EQUALS, var);
		return this;
	}
	
	public Operation lessOrEqual(Object var) {
		appendVar(query, Query.LESS_OR_EQUALS, var);
		return this;
	}

	public Operation orderBy(Object varName) {
		String sql = query.toString();
		if (!sql.contains(" " + Query.ORDER_BY + " ")) {
			query.append(' ')
				 .append(Query.ORDER_BY)
				 .append(' ');
		} else {
			query.append(',');
		}
		query.append(varName);
		return this;
	}
}
