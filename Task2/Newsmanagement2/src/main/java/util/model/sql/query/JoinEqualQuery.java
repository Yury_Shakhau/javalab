package util.model.sql.query;

public class JoinEqualQuery {

	private final StringBuilder query;
	
	public JoinEqualQuery(StringBuilder query) {
		this.query = query;
	}
	
	public Condition equal(Object var) {
		query.append(Query.EQUALS)
			 .append('\'')
			 .append(var)
			 .append('\'');
		return new Condition(query);
	}
}
