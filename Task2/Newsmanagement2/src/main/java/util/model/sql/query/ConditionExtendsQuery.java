package util.model.sql.query;

public class ConditionExtendsQuery extends Condition {

	private StringBuilder query;
	
	public ConditionExtendsQuery(StringBuilder query) {
		super(query);
		this.query = query;
	}

	public Condition as(String alias) {
		query.append(' ')
			 .append(alias);
		return this;
	}
	
	public ConditionExtendsQuery join(Object tableName) {
		query.append(' ')
			 .append(Query.JOIN)
			 .append(' ')
			 .append(tableName);
		return new ConditionExtendsQuery(query);
	}
}
