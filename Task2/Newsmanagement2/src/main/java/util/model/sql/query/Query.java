package util.model.sql.query;

public class Query {

	public static final String SELECT = "SELECT";
	
	public static final String UPDATE = "UPDATE";
	
	public static final String DELETE = "DELETE";
	
	public static final String INSERT_INTO = "INSERT INTO";
	
	public static final String ORDER_BY = "ORDER BY";
	
	public static final String VALUES = "VALUES";
	
	public static final String SET = "SET";
	
	public static final String JOIN = "JOIN";
	
	public static final String FROM = "FROM";
	
	public static final String WHERE = "WHERE";
	
	public static final String IN = "IN";
	
	public static final String EQUALS = "=";
	
	public static final String LARGER = ">";
	
	public static final String LESS = "<";
	
	public static final String LARGER_OR_EQUALS = ">" + EQUALS;
	
	public static final String LESS_OR_EQUALS = "<" + EQUALS;
	
	public static final String AND = "AND";
	
	public static final String OR = "OR";
	
	public static final String ON = "ON";
	
	public static final String UNKNOWN = "?";
	
	
	public static FromOperation select(String...vars) {
		StringBuilder query = new StringBuilder();
		query.append(SELECT).append(' ');
		int lastIndex = vars.length - 1;
		if (lastIndex > -1) {
			for (int i = 0; i < lastIndex; i++) {
				query.append(vars[i]).append(',');
			}
			query.append(vars[lastIndex]);
		}
		return new FromOperation(query);
	}
	
	public static AfterInsertQuery insertInto(String tableName) {
		StringBuilder query = new StringBuilder();
		query.append(INSERT_INTO);
		query.append(' ')
		 .append(tableName);
		return new AfterInsertQuery(query);
	}
	
	public static ConditionExtendsQuery deleteFrom(String tableName) {
		StringBuilder query = new StringBuilder();
		query.append(DELETE);
		query.append(' ')
		 .append(Query.FROM)
		 .append(' ')
		 .append(tableName);
		return new ConditionExtendsQuery(query);
	}
	
	public static VarNameQuery update(String tableName) {
		StringBuilder query = new StringBuilder();
		query.append(UPDATE)
			 .append(' ')
			 .append(tableName);
		return new VarNameQuery(query);
	}
}
