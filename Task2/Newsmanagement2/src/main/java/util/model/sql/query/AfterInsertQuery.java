package util.model.sql.query;

public class AfterInsertQuery {

	private final StringBuilder query;
	
	public AfterInsertQuery(StringBuilder query) {
		this.query = query;
	}
	
	public AfterInsertQueryExtends variebleNames(String...varNames) {
		query.append(" (");
		int lastIndex = varNames.length - 1;
		if (lastIndex > -1) {
			for (int i = 0; i < lastIndex; i++) {
				query.append(varNames[i]).append(',');
			}
			query.append(varNames[lastIndex]);
		}
		query.append(')');
		return new AfterInsertQueryExtends(query);
	}
}
