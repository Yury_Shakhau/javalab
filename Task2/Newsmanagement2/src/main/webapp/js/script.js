var map;
var index = 0;

function confirmDialog() {
	var needDelete = confirm(confirmDelete);
	if (needDelete) {
		return true;
	}
	return false;
}

function findElementByName(rootElement, name) {
	var children = rootElement.children;
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		if (name === child.getAttribute("name")) {
			return child;
		}
		var retEl = findElementByName(child, name);
		if (retEl != null) {
			return retEl;
		}
	}
	return null;
}

function findElementsForName(rootElement, name) {
	var children = rootElement.children;
	for (var i = 0; i < children.length; i++) {
		var child = children[i];
		var elName = child.getAttribute("name");
		if (name === elName) {
			map.set(index, child);
			index++;
		}
		findElementsForName(child, name);
	}
}

function findElementsByName(rootElement, name) {
	map = new Map();
	index = 0;
	findElementsForName(rootElement, name);
	return map;
}

function confirmDeleteBlock(container) {
	var confText = confirmDeleteBlockMessage;
	var newsItems = document.getElementsByTagName("tbody");
	var founded = false;
	for (var i = 0; i < newsItems.length; i++) {
		var newsItem = newsItems[i];
		var confirmElement = findElementByName(newsItem, "selectedId");
		if (confirmElement != null) {
			if (confirmElement.checked) {
				var titleElement = findElementByName(newsItem, "title");
				confText += "\r\n" + titleElement.innerHTML;
				founded = true;
			}
		}
	}
	confText += "?";
	if (!founded) {
		alert(nothingSelected);
		return false;
	}
	var needDelete = confirm(confText);
	if (needDelete) {
		return true;
	}
	return false;
}
