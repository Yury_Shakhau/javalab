<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="menu">
	<div class="menu-title">
		<bean:message key="menu.news"/>
	</div>
	<div class="menu-items">
		<table class="menu-table">
			<tr>
				<td class="menu-table-first">
					<center><html:image src="img/circle.jpg"/></center>
				</td>
				<td>
					<html:link action="/list" styleClass="menu-list-link">
						<bean:message key="menu.news.list"/>
					</html:link>
				</td>
			</tr>
			<tr>
				<td class="menu-table-first">
					<center><html:image src="img/circle.jpg"/></center>
				</td>
				<td>
					<html:link action="/adding" styleClass="menu-add-link">
						<bean:message key="menu.add.news"/>
					</html:link>
				</td>
			</tr>
		</table>
	</div>
</div>
