<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="header">
	<div class="title"><h2><bean:message key="header.news.management"/></h2></div>
	<div class="language-container">
		<span class="language">
			<html:link action="/changeLanguage.do?language=en">
				<bean:message key="header.english"/>
			</html:link>
		</span>
		<span class="language">
			<html:link action="/changeLanguage.do?language=ru">
				<bean:message key="header.russian"/>
			</html:link>
		</span>
	</div>
</div>