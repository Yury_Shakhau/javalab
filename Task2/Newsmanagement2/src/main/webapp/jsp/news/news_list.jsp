<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html:form action="delete" onsubmit="return confirmDeleteBlock(this)">
	<table class="list-news-table">
		<logic:iterate id="news" name="newsForm" property="newsList">
			<tbody class="list-news-item" name="newsItem">
				<tr class="list-row">
					<td class="list-news-title">
						<b name="title"><bean:write name="news" property="title"/></b>
					</td>
					<td class="list-news-date">
						<u><bean:write name="news" property="dateCreate" 
									   formatKey="date.format"/></u>
					</td>
				</tr>
				<tr>
					<td class="list-news-brief">
						<bean:write name="news" property="brief"/>
					</td>
				</tr>
				<tr class="list-row">
					<td class="list-news-title">
					</td>
					<td>
						<bean:define name="news" id="newsId" property="id"/>
						<div class="bottom-panel">
							<span class="list-component">
								<html:checkbox property="selectedId" value="${ news.id }"/>
							</span>
							<span class="list-component">
								<html:link paramName="newsId" paramId="news.id" action="edit">
									<bean:message key="list.link.edit"/>
								</html:link>
							</span>
							<span class="list-component">
								<html:link paramName="newsId" paramId="news.id" action="view">
									<bean:message key="list.link.view"/>
								</html:link>
							</span>
						</div>
					</td>
				</tr>
			</tbody>
		</logic:iterate>
	</table>
	<div class="delete-div">
		<html:submit styleClass="button delete-button"><bean:message key="list.delete"/></html:submit>
	</div>
</html:form>
<style>
<!--
.menu-list-link {
	color: orange;
}
-->
</style>