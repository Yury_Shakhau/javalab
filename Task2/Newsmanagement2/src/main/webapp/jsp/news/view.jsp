<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<bean:define id="news" name="newsForm" property="news"/>
<div>
	<table class="view-news-table">
		<tbody>
			<tr class="row-title">
				<td class="add-news-first"><bean:message key="list.news.title"/></td>
				<td class="news-list-title"><bean:write name="news" property="title"/></td>
			</tr>
			<tr class="row-date">
				<td class="add-news-first"><bean:message key="list.news.date"/></td>
				<td class="news-list-date">
					<bean:write name="news" property="dateCreate"
								formatKey="date.format"/>
				</td>
			</tr>
			<tr class="row-brief">
				<td class="add-news-first"><bean:message key="list.brief"/></td>
				<td class="news-list-brief"><bean:write name="news" property="brief"/></td>
			</tr>
			<tr class="row-content">
				<td class="add-news-first"><bean:message key="list.content"/></td>
				<td class="news-list-content"><bean:write name="news" property="content"/></td>
			</tr>
		</tbody>
	</table>
	<div class="edit-delete">
		<html:form styleClass="delete-button" action="delete" onsubmit="return confirmDialog()">
			<html:hidden property="selectedId" value="${ news.id }"/>
			<html:submit styleClass="button">
				<bean:message key="list.delete"/>
			</html:submit>
		</html:form>
		<html:form styleClass="edit-button" action="edit">
			<html:hidden name="news" property="id"/>
			<html:submit styleClass="button">
				<bean:message key="list.edit"/>
			</html:submit>
		</html:form>
	</div>
</div>