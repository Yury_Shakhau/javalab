package com.epam.newsmanagement.dao.test;

import org.springframework.test.context.ContextConfiguration;

/**
 * Hibernate dao test. Implements abstract dao test.
 * 
 * @author Yury_Shakhau
 *
 */
@ContextConfiguration(locations = { "classpath:testHibernateContext.xml" })
public class HibernateDaoTest extends DaoTest {

}
