<%@ page language="java" contentType="text/javascript; charset=utf-8" pageEncoding="utf-8"%>
	
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

var confirmDelete = '<bean:message key="js.confirm.delete"/>';
var confirmDeleteBlockMessage = '<bean:message key="js.confirm.delete.block"/>';
var nothingSelected = '<bean:message key="js.confirm.nothing.selected"/>';