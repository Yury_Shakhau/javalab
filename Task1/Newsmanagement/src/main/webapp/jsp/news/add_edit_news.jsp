<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link href="css/add-edit-news.css" rel="stylesheet" type="text/css" />
<center>
	<html:form action="/save.do" onsubmit="return validateNewsForm(this)">
		<table class="add-news-table">
			<tr>
				<td></td>
				<td>
					<font color="red"><html:errors prefix="prefix"/></font>
				</td>
			</tr>
			<tr class="row-title">
				<td class="add-news-first">
					<bean:message key="list.news.title"/>
				</td>
				<td>
					<html:text styleClass="news-title" name="newsForm" property="news.title"/>
				</td>
			</tr>
			<tr class="row-date">
				<td class="add-news-first">
					<bean:message key="list.news.date"/>
				</td>
				<td>
					<html:text styleClass="news-date" name="newsForm" property="dateCreate" />
				</td>
			</tr>
			<tr class="row-brief">
				<td class="add-news-first">
					<bean:message key="list.brief"/>
				</td>
				<td>
					<html:textarea styleClass="news-brief"  name="newsForm" property="news.brief"/>
				</td>
			</tr>
			<tr class="row-content">
				<td class="add-news-first">
					<bean:message key="list.content"/>
				</td>
				<td>
					<html:textarea styleClass="news-content"  name="newsForm" property="news.content"/>
				</td>
			</tr>
		</table>
		<html:submit styleClass="button"><bean:message key="list.save"/></html:submit>
	</html:form>
	<html:form action="/cancelSave">
		<html:submit styleClass="button"><bean:message key="list.cancel"/></html:submit>
	</html:form>
</center>