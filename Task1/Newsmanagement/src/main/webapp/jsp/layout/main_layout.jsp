<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><bean:message key="header.news.management"/></title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/jsMessages.jsp"></script>
		<html:javascript formName="newsForm" method="validateNewsForm" dynamicJavascript="true" staticJavascript="false"/>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/staticJavascript.jsp"></script>
	</head>
	<body>
		<div class="body"></div>
		<div class="news-block">
			<div class="list-news-header"><tiles:insert attribute="body_header"/></div>
			<tiles:insert attribute="body"/>
			<div class="under-footer"></div>
		</div>
		<tiles:insert attribute="menu"/>
		<tiles:insert attribute="footer"/>
		<tiles:insert attribute="header"/>
	</body>
</html>