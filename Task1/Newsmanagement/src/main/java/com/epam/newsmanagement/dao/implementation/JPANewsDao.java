package com.epam.newsmanagement.dao.implementation;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.entity.News;
import com.epam.newsmanagement.util.factory.JPAUtil;

public class JPANewsDao implements INewsDao {

	private JPAUtil jpaUtil;

	public void setJpaUtil(JPAUtil jpaUtil) {
		this.jpaUtil = jpaUtil;
	}

	private void closeManager(EntityManager em) {
		if (em != null) {
			em.close();
		}
	}
	
	@Override
	public News find(long id) {
		News news = null;
		EntityManager em = null;
		try {
			em = jpaUtil.createManagerFactory();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			
			CriteriaQuery<News> cq = cb.createQuery(News.class);
			Root<News> r = cq.from(News.class);
			news = em.createQuery(
						cq.select(
							r
						)
						.where(
							cb.and(
								cb.equal(
									r.get(News.ID), id
								),
								cb.equal(
									r.get(News.ACTIVE), News.ENABLE
								)
							)
						)
				   )
				   .getSingleResult();
			
		} finally {
			closeManager(em);
		}
		return news;
	}

	@Override
	public News add(News entity) {
		EntityManager em = null;
		try {
			em = jpaUtil.createManagerFactory();
			EntityTransaction t = em.getTransaction();
			
			t.begin();
				em.persist(entity);
			t.commit();
			
		} finally {
			closeManager(em);
		}
		return entity;
	}

	@Override
	public void save(News entity) {
		EntityManager em = null;
		try {
			em = jpaUtil.createManagerFactory();
			EntityTransaction t = em.getTransaction();
			
			t.begin();
				em.merge(entity);
			t.commit();
		
		} finally {
			closeManager(em);
		}
	}

	@Override
	public List<News> findAll() {
		List<News> news = null;
		EntityManager em = null;
		try {
			em = jpaUtil.createManagerFactory();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<News> cq = cb.createQuery(News.class);
			
			Root<News> r = cq.from(News.class);
			news = em.createQuery(
				  	   cq.select(
				  	       r
				  	   )
					   .where(
					       cb.equal(
				    		   r.get(News.ACTIVE), News.ENABLE
			    		   )
				       )
				       .orderBy(
			    		   cb.desc(
		    				   r.get(News.DATE_CREATE)
			    		   )
				       )
				  )
				  .getResultList();
		
		} finally {
			closeManager(em);
		}
		return news;
	}

	@Override
	public void deleteMany(Long[] ids) {
		EntityManager em = null;
		try {
			em = jpaUtil.createManagerFactory();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			EntityTransaction t = em.getTransaction();
			
			t.begin();
				CriteriaUpdate<News> cu = cb.createCriteriaUpdate(News.class);
				Root<News> r = cu.from(News.class);
				
				em.createQuery(
					cu.set(
						News.ACTIVE, News.DISABLE
					)
					.where(
						r.get(
							News.ID
						).in(
							Arrays.asList(ids)
						)
					)
				)
				.executeUpdate();
			t.commit();
		
		} finally {
			closeManager(em);
		}
	}
}
