package com.epam.newsmanagement.util.factory;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * 
 * 
 * @author Yury_Shkahau
 *
 */
public class FactoryConfig {

	private Map<String, String> config;

	public void setConfig(Map<String, String> config) {
		this.config = config;
	}
	
	public Properties getProperties() {
		Properties props = new Properties();
		for (Entry<String, String> c : config.entrySet()) {
			props.setProperty(c.getKey(), c.getValue());
		}
		return props;
	}
}
