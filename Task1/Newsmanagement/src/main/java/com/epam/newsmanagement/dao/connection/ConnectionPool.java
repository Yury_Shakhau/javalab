package com.epam.newsmanagement.dao.connection;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.DaoException;

public final class ConnectionPool implements IConnectionPool {
	
	private BlockingQueue<Connection> connections;
	private BlockingQueue<Connection> givenAwayConnections;
	
	private String driverName;
	private String url;
	private String user;
	private String password;
	private int poolSize;
	
	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}
	
	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger logger = Logger.getLogger(ConnectionPool.class);
	
	public void init() {
		Connection conn = null;
		try {
			
			Class.forName(driverName);
			givenAwayConnections = new ArrayBlockingQueue<Connection>(poolSize);
			connections = new ArrayBlockingQueue<Connection>(poolSize);
			for (int i = 0; i < poolSize; i++) {
				conn = DriverManager.getConnection(url, user, password);
				Connection pc = new PooledConnection(conn);
				connections.add(pc);
			}
		} catch (SQLException e) {
			throw new Error("Sql exception in db connection creating", e);
		} catch (ClassNotFoundException e) {
			throw new Error("Can not found sql driver", e);
		}
	}
	
	public void destroy() {
		closeConnectionsQueue(givenAwayConnections);
		closeConnectionsQueue(connections);
	}

	public Connection takeConnection() {
		Connection connection = null;
		try {
			connection = connections.take();
			givenAwayConnections.offer(connection);
		} catch (InterruptedException e) {
			logger.error("Statement can not close", e);
		}
		return connection;
	}
	
	public void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			logger.error("Statement can not close", e);
		}
	}
	
	public void releaseConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			logger.error("Connection can not close", e);
		}
	}
	
	private void closeConnectionsQueue(BlockingQueue<Connection> queue) {
		Connection connection = null;
		try {
			while ((connection = queue.poll()) != null) {
				if (!connection.getAutoCommit()) {
					connection.commit();
				}
				((PooledConnection) connection).release();
			}
		} catch (SQLException e) {
			logger.error("Connection can not close", e);
		}
	}
	
	private class PooledConnection implements Connection {

		private Connection connection;
		
		private PooledConnection(Connection connection) {
			this.connection = connection;
		}
		
		private void release() throws DaoException {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new DaoException("Connection can not close", e);
			}
		}
		
		@Override
		public void close() throws SQLException {
			if (connection.isClosed()) {
				throw new DaoException("Attempting to close closed connection");
			}
			if (connection.isReadOnly()) {
				connection.setReadOnly(false);
			}
			if (!givenAwayConnections.remove(this)) {
				throw new DaoException("Error deleting form the given way connection "
						+ "pool");
			}
			if (!connections.offer(this)) {
				throw new DaoException("Error allocating connection in the pool");
			}
		}
		
		@Override
		public void commit() throws SQLException {
			try {
				connection.commit();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}
		
		@Override
		public <T> T unwrap(Class<T> iface) throws SQLException {
			try {
				return connection.unwrap(iface);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public boolean isWrapperFor(Class<?> iface) throws SQLException {
			try {
				return connection.isWrapperFor(iface);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Statement createStatement() throws SQLException {
			try {
				return connection.createStatement();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			try {
				return connection.prepareStatement(sql);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public CallableStatement prepareCall(String sql) throws SQLException {
			try {
				return connection.prepareCall(sql);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public String nativeSQL(String sql) throws SQLException {
			try {
				return connection.nativeSQL(sql);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setAutoCommit(boolean autoCommit) throws SQLException {
			try {
				connection.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public boolean getAutoCommit() throws SQLException {
			try {
				return connection.getAutoCommit();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void rollback() throws SQLException {
			try {
				connection.rollback();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}
		
		@Override
		public boolean isClosed() throws SQLException {
			try {
				return connection.isClosed();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public DatabaseMetaData getMetaData() throws SQLException {
			try {
				return connection.getMetaData();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setReadOnly(boolean readOnly) throws SQLException {
			try {
				connection.setReadOnly(readOnly);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public boolean isReadOnly() throws SQLException {
			try {
				return connection.isReadOnly();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setCatalog(String catalog) throws SQLException {
			try {
				connection.setCatalog(catalog);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public String getCatalog() throws SQLException {
			try {
				return connection.getCatalog();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setTransactionIsolation(int level) throws SQLException {
			try {
				connection.setTransactionIsolation(level);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public int getTransactionIsolation() throws SQLException {
			try {
				return connection.getTransactionIsolation();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public SQLWarning getWarnings() throws SQLException {
			try {
				return connection.getWarnings();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void clearWarnings() throws SQLException {
			try {
				connection.clearWarnings();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Statement createStatement(int resultSetType, int resultSetConcurrency)
				throws SQLException {
			try {
				return connection.createStatement(resultSetType, resultSetConcurrency);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType,
				int resultSetConcurrency) throws SQLException {
			try {
				return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType,
				int resultSetConcurrency) throws SQLException {
			try {
				return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Map<String, Class<?>> getTypeMap() throws SQLException {
			try {
				return connection.getTypeMap();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
			try {
				setTypeMap(map);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setHoldability(int holdability) throws SQLException {
			try {
				connection.setHoldability(holdability);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public int getHoldability() throws SQLException {
			try {
				return connection.getHoldability();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Savepoint setSavepoint() throws SQLException {
			try {
				return setSavepoint();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Savepoint setSavepoint(String name) throws SQLException {
			try {
				return connection.setSavepoint(name);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void rollback(Savepoint savepoint) throws SQLException {
			try {
				connection.rollback(savepoint);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void releaseSavepoint(Savepoint savepoint) throws SQLException {
			try {
				connection.releaseSavepoint(savepoint);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Statement createStatement(int resultSetType,
				int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			try {
				return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int resultSetType,
				int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			try {
				return connection.prepareStatement(sql, resultSetType, 
										resultSetConcurrency, resultSetHoldability);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public CallableStatement prepareCall(String sql, int resultSetType,
				int resultSetConcurrency, int resultSetHoldability)
				throws SQLException {
			try {
				return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
				throws SQLException {
			try {
				return connection.prepareStatement(sql, autoGeneratedKeys);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql, int[] columnIndexes)
				throws SQLException {
			try {
				return connection.prepareStatement(sql, columnIndexes);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public PreparedStatement prepareStatement(String sql, String[] columnNames)
				throws SQLException {
			try {
				return connection.prepareStatement(sql, columnNames);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Clob createClob() throws SQLException {
			try {
				return connection.createClob();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Blob createBlob() throws SQLException {
			try {
				return connection.createBlob();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public NClob createNClob() throws SQLException {
			try {
				return connection.createNClob();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public SQLXML createSQLXML() throws SQLException {
			try {
				return connection.createSQLXML();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public boolean isValid(int timeout) throws SQLException {
			try {
				return connection.isValid(timeout);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setClientInfo(String name, String value)
				throws SQLClientInfoException {
			try {
				connection.setClientInfo(name, value);
			} catch (SQLClientInfoException e) {
				logger.error(e);
			}
		}

		@Override
		public void setClientInfo(Properties properties)
				throws SQLClientInfoException {
			try {
				connection.setClientInfo(properties);
			} catch (SQLClientInfoException e) {
				logger.error(e);
			}
		}

		@Override
		public String getClientInfo(String name) throws SQLException {
			try {
				return connection.getClientInfo(name);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Properties getClientInfo() throws SQLException {
			try {
				return connection.getClientInfo();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Array createArrayOf(String typeName, Object[] elements)
				throws SQLException {
			try {
				return connection.createArrayOf(typeName, elements);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public Struct createStruct(String typeName, Object[] attributes)
				throws SQLException {
			try {
				return connection.createStruct(typeName, attributes);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setSchema(String schema) throws SQLException {
			try {
				connection.setSchema(schema);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public String getSchema() throws SQLException {
			try {
				return connection.getSchema();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void abort(Executor executor) throws SQLException {
			try {
				connection.abort(executor);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public void setNetworkTimeout(Executor executor, int milliseconds)
				throws SQLException {
			try {
				setNetworkTimeout(executor, milliseconds);
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}

		@Override
		public int getNetworkTimeout() throws SQLException {
			try {
				return connection.getNetworkTimeout();
			} catch (SQLException e) {
				throw new DaoException(e);
			}
		}
	}
}
