package com.epam.newsmanagement.dao;

import java.sql.SQLException;

/**
* The exception class of dao layer.
* It's inherited from SQLException class
* 
* @author Yury_Shakhau
*/
public class DaoException extends SQLException {

	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 4699909534668435921L;

	public DaoException() {
		super();
	}
	
	public DaoException(String message) {
		super(message);
	}
	
	public DaoException(Throwable t) {
		super(t);
	}
	
	public DaoException(String message, Throwable t) {
		super(message, t);
	}
}
