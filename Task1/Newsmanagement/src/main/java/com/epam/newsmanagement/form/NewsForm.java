package com.epam.newsmanagement.form;

import java.util.List;

import org.apache.struts.validator.ValidatorForm;

import com.epam.newsmanagement.dao.entity.News;

/**
 * Bean for getting data from page and setting it there.
 * 
 * @author Yury Shakhau
 *
 */
public class NewsForm extends ValidatorForm {

	/**
	 * Id for deseriaization.
	 */
	private static final long serialVersionUID = -5508514675700454938L;
	
	/* Entity for for getting data about news from page and setting it there. */
	private News news = new News();

	private List<News> newsList;

	private Long[] selectedId;
	
	private String dateCreate;
	
	private String currentUrl;
	
	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	public Long[] getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(Long[] selectedId) {
		this.selectedId = selectedId;
	}
	
	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}
	
	public String getDateCreate() {
		return dateCreate;
	}

	public String getCurrentUrl() {
		return currentUrl;
	}

	public void setCurrentUrl(String currentUrl) {
		this.currentUrl = currentUrl;
	}
}
