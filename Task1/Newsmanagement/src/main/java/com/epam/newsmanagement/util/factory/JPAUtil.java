package com.epam.newsmanagement.util.factory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;

import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.epam.newsmanagement.dao.DaoException;

/**
 * 
 * 
 * @author Yury_Shkahau
 *
 */
public final class JPAUtil {
	
	private static EntityManagerFactory ENTITY_MANAGER_FACTORY;
	
	private static final String PERSISTANCE_UNIT_NAME = "NewsJPAUnit";
	
	private static String provider;
	
	private static String packgesToScan;
	
	private FactoryConfig config;

	public void setConfig(FactoryConfig config) {
		this.config = config;
	}

	public void setProvider(String provider) {
		JPAUtil.provider = provider;
	}

	public void setPackgesToScan(String packgesToScan) {
		JPAUtil.packgesToScan = packgesToScan;
	}

	@SuppressWarnings("unchecked")
	public void init() throws DaoException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setJpaVendorAdapter( new HibernateJpaVendorAdapter() );
		em.setPackagesToScan(packgesToScan);
		em.setPersistenceUnitName(PERSISTANCE_UNIT_NAME);
		em.setJpaProperties(config.getProperties());
		try {
			em.setPersistenceProviderClass((Class<? extends PersistenceProvider>) Class.forName(provider));
		} catch (ClassNotFoundException e) {
			throw new DaoException(e);
		}
		em.afterPropertiesSet();
		ENTITY_MANAGER_FACTORY = em.getObject();
	}

	public JPAUtil() {
		
	}

	
	public EntityManager createManagerFactory() {
		return ENTITY_MANAGER_FACTORY.createEntityManager();
	}
	
	public void closeFactory() {
		ENTITY_MANAGER_FACTORY.close();
	}
}
