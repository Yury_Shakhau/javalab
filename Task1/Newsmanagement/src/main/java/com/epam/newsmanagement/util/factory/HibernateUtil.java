package com.epam.newsmanagement.util.factory;

import java.util.List;

import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.epam.newsmanagement.dao.DaoException;

/**
 * 
 * 
 * @author Yury_Shkahau
 *
 */
public final class HibernateUtil {

	private static SessionFactory SESSION_FACTORY;
	
	private FactoryConfig config;

	public void setConfig(FactoryConfig config) {
		this.config = config;
	}

	private static List<String> mappingClass;

	private static List<String> mappingAnotatedClass;

	private static List<String> mappingXml;

	public void setMappingClass(List<String> mappingClass) {
		HibernateUtil.mappingClass = mappingClass;
	}
	
	public void setMappingXml(List<String> mappingXml) {
		HibernateUtil.mappingXml = mappingXml;
	}
	
	public void setAnotatedClass(List<String> mappingAnotatedClass) {
		HibernateUtil.mappingAnotatedClass = mappingAnotatedClass;
	}
	
	public HibernateUtil() {
		
	}
	
	public void init() throws DaoException {
		Configuration conf = new Configuration();
		conf.setProperties(config.getProperties());
		if (mappingAnotatedClass != null) {
			for (String cn : mappingAnotatedClass) {
				
				try {
					conf.addAnnotatedClass(Class.forName(cn));
				} catch (MappingException | ClassNotFoundException e) {
					throw new DaoException(e);
				}
				
			}
		}
		if (mappingClass != null) {
			for (String cn : mappingClass) {
				
				try {
					conf.addClass(Class.forName(cn));
				} catch (MappingException | ClassNotFoundException e) {
					throw new DaoException(e);
				}
				
			}
		}
		if (mappingXml != null) {
			for (String mc : mappingXml) {
				conf.addResource(mc);
			}
		}
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
												.applySettings(conf.getProperties())
												.build();
		SESSION_FACTORY = conf.buildSessionFactory(serviceRegistry);
	}
	
	public Session getCurrentSession() {
		return SESSION_FACTORY.getCurrentSession();
	}
	
	public Session geOpenSession() {
		return SESSION_FACTORY.openSession();
	}
	
	public void closeFactory() {
		SESSION_FACTORY.close();
	}
}
