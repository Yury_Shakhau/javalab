package com.epam.newsmanagement.dao.implementation;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.entity.News;
import com.epam.newsmanagement.util.factory.HibernateUtil;

public class HibernateNewsDao implements INewsDao {
	
	private HibernateUtil hibernateUtil;

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	
	/**
	 * Hql for delete method.
	 */
	public static final String HQL_DELETE = "UPDATE " + News.TABLE_NAME + " "
			 							   + "SET " + News.ACTIVE + "=" + News.DISABLE + " WHERE " + News.ID + " = ?";
	
	private void closeSession(Session session) throws DaoException {
		try {
			if (session != null) {
				session.flush();
				session.close();
			}
		} catch (HibernateException e) {
			throw new DaoException(e);
		}
	}
	
	private Criteria createCriteria(Session session) throws DaoException {
		try {
			return session.createCriteria(
			   	       News.class
				   )
				   .add(
				       Restrictions.eq(
				    		News.ACTIVE, News.ENABLE
					   )
				   );
		} catch (HibernateException e) {
			throw new DaoException(e);
		}
	}
	
	@Override
	public News find(long id) throws DaoException {
		Session s = null;
		News news = null;
		try {
			s = hibernateUtil.getCurrentSession();
			news = (News) createCriteria(s)
						  .add(
							   Restrictions.eq(
								   News.ID, id
							   )
						  )
						 .uniqueResult();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			closeSession(s);
		}
		return news;
	}

	@Override
	public News add(News entity) throws DaoException {
		Session s = null;
		try {
			s = hibernateUtil.getCurrentSession();
			Transaction t = s.beginTransaction();
			s.save(entity);
			t.commit();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			closeSession(s);
		}
		return entity;
	}

	@Override
	public void save(News entity) throws DaoException {
		Session s = null;
		try {
			s = hibernateUtil.getCurrentSession();
			Transaction t = s.beginTransaction();
			s.update(entity);
			t.commit();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			closeSession(s);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<News> findAll() throws DaoException {
		Session s = null;
		List<News> news = null;
		try {
			s = hibernateUtil.getCurrentSession();
			news = (List<News>) createCriteria(s)
						  	    .addOrder(
					  				 Order.desc(
				  						 News.DATE_CREATE
				  					 )
			  				    )
						  	   .list();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			closeSession(s);
		}
		return news;
	}

	private void delete(Session s, long id) throws DaoException {
		try {
			s.createQuery(
				HQL_DELETE
			  )
			 .setLong(
				 0, id
			  )
			 .executeUpdate();
		} catch (HibernateException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void deleteMany(Long[] ids) throws DaoException {
		Session s = null;
		try {
			s = hibernateUtil.getCurrentSession();
			Transaction t = s.beginTransaction();
			for (Long id : ids) {
				delete(s, id);
			}
			t.commit();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			closeSession(s);
		}
	}
}
