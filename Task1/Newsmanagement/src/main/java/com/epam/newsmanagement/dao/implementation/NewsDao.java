package com.epam.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import util.model.sql.query.Query;

import com.epam.newsmanagement.dao.DaoException;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.dao.connection.IConnectionPool;
import com.epam.newsmanagement.dao.entity.News;

/**
 * INewsDao implementation. Implement its methods.
 * 
 * @author Yury Shakhau
 *
 */
public final class NewsDao implements INewsDao {

	/**
	 * Sql for add method.
	 */
	public static final String SQL_ADD = Query.insertInto(News.TABLE_NAME)
													.variebleNames(News.ID, News.TITLE, News.DATE_CREATE, News.BRIEF, News.CONTENT, News.ACTIVE)
													.values(Query.UNKNOWN, Query.UNKNOWN, Query.UNKNOWN, Query.UNKNOWN, Query.UNKNOWN, News.ENABLE)
										.build();

	/**
	 * Sql for save method.
	 */
	public static final String SQL_SAVE = Query.update(News.TABLE_NAME)
														.set(News.TITLE, Query.UNKNOWN)
														.set(News.DATE_CREATE , Query.UNKNOWN)
														.set(News.BRIEF , Query.UNKNOWN)
														.set(News.CONTENT , Query.UNKNOWN)
														.set(News.ACTIVE , News.ENABLE)
												.where(News.ID).equal(Query.UNKNOWN)
										   .build();
	
	/**
	 * Sql for find(long id) method.
	 */
	public static final String SQL_FIND_BY_ID = Query.select(News.ID, News.TITLE, News.DATE_CREATE, News.BRIEF, News.CONTENT, News.ACTIVE)
													 .from(News.TABLE_NAME)
													 .where(News.ACTIVE).equal(News.ENABLE)
													 .and(News.ID).equal(Query.UNKNOWN)
												.build();
	
	/**
	 * Sql for addAll method.
	 */
	public static final String SQL_SELECT_ALL = Query.select(News.ID, News.TITLE, News.DATE_CREATE, News.BRIEF, News.CONTENT, News.ACTIVE)
													.from(News.TABLE_NAME)
													.where(News.ACTIVE).equal(News.ENABLE)
													.orderBy(News.DATE_CREATE)
											   .build();

	/**
	 * Sql for delete method.
	 */
	public static final String SQL_DELETE = Query.update(News.TABLE_NAME)
												  .set(News.ACTIVE, News.DISABLE)
												  .where(News.ID).equal(Query.UNKNOWN)
										    .build();

	/**
	 * Sql for deleteMany method.
	 */
	public static final String SQL_DELETE_MANY = Query.update(News.TABLE_NAME)
													   .set(News.ACTIVE, News.DISABLE)
													   .where(News.ID)
													   .in()
											      .build() + "(";
	
	/**
	 * For getting connection to db.
	 */
	private IConnectionPool connectionPool;

	public IConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public void setConnectionPool(IConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	@Override
	public News find(long id) throws DaoException {
		Connection con = connectionPool.takeConnection();
		PreparedStatement st = null;
		News news = null;
		try {
			st = con.prepareStatement(SQL_FIND_BY_ID);
			st.setLong(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				news = new News();
				news.setId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setDateCreate(rs.getDate(3));
				news.setBrief(rs.getString(4));
				news.setContent(rs.getString(5));
				news.setActive(rs.getInt(6));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeStatement(st);
			connectionPool.releaseConnection(con);
		}
		return news;
	}

	@Override
	public News add(News entity) throws DaoException {
		Connection con = connectionPool.takeConnection();
		PreparedStatement st = null;
		try {
			System.out.println(SQL_ADD);
			st = con.prepareStatement(SQL_ADD, new String[] { "id" });
			st.setLong(1, entity.getId());
			st.setString(2, entity.getTitle());
			st.setDate(3, entity.getDateCreate());
			st.setString(4, entity.getBrief());
			st.setString(5, entity.getContent());
			st.executeUpdate();
			ResultSet genKey = st.getGeneratedKeys();
			if (genKey.next()) {
				long insertedId = genKey.getLong(1);
				entity.setId(insertedId);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeStatement(st);
			connectionPool.releaseConnection(con);
		}
		return entity;
	}

	@Override
	public void save(News entity) throws DaoException {
		Connection con = connectionPool.takeConnection();
		PreparedStatement st = null;
		try {
			st = con.prepareStatement(SQL_SAVE);
			st.setString(1, entity.getTitle());
			st.setDate(2, entity.getDateCreate());
			st.setString(3, entity.getBrief());
			st.setString(4, entity.getContent());
			st.setLong(5, entity.getId());
			st.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeStatement(st);
			connectionPool.releaseConnection(con);
		}
	}

	@Override
	public List<News> findAll() throws DaoException {
		Connection con = connectionPool.takeConnection();
		Statement st = null;
		List<News> newsList = new ArrayList<>();
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_ALL);
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setDateCreate(rs.getDate(3));
				news.setBrief(rs.getString(4));
				news.setContent(rs.getString(5));
				news.setActive(rs.getInt(6));
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeStatement(st);
			connectionPool.releaseConnection(con);
		}
		return newsList;
	}

	@Override
	public void deleteMany(Long[] ids) throws DaoException {
		StringBuffer sql = new StringBuffer(SQL_DELETE_MANY);
		Connection con = connectionPool.takeConnection();
		Statement st = null;
		try {
			st = con.createStatement();
			int lastIndex = ids.length - 1;
			for (int i = 0; i < ids.length; i++) {
				Long id = ids[i];
				sql.append(id);
				if (i != lastIndex) {
					sql.append(',');
				} else {
					sql.append(')');
				}
			}
			st.execute(sql.toString());
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeStatement(st);
			connectionPool.releaseConnection(con);
		}
	}
}
