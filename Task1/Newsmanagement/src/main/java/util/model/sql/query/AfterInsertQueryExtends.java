package util.model.sql.query;

public class AfterInsertQueryExtends {

	private final StringBuilder query;
	
	public AfterInsertQueryExtends(StringBuilder query) {
		this.query = query;
	}
	
	public Builder values(Object...value) {
		query.append(' ').append(Query.VALUES).append(" (");
		int lastIndex = value.length - 1;
		if (lastIndex > -1) {
			for (int i = 0; i < lastIndex; i++) {
				query.append(value[i]).append(',');
			}
			query.append(value[lastIndex]);
		}
		query.append(')');
		return new Builder(query);
	}
}
