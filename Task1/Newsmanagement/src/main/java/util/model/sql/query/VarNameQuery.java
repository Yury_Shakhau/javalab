package util.model.sql.query;

public class VarNameQuery extends Condition {

	private final StringBuilder query;
	
	public VarNameQuery(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	public VarNameQuery set(String varName, Object value) {
		String sql = query.toString();
		if (!sql.contains(" " + Query.SET + " ")) {
			query.append(' ')
				 .append(Query.SET)
				 .append(' ');
		} else {
			query.append(',');
		}
		query.append(varName)
		 	 .append(Query.EQUALS)
		 	 .append('\'')
		 	 .append(value)
		 	 .append('\'');
		return this;
	}
}
