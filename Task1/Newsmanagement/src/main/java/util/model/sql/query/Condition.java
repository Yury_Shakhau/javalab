package util.model.sql.query;

public class Condition extends Builder {

	private StringBuilder query;
	
	public Condition(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	public Operation where(String varName) {
		query.append(' ')
			 .append(Query.WHERE)
			 .append(' ')
			 .append(varName);
		return new Operation(query);
	}
}
