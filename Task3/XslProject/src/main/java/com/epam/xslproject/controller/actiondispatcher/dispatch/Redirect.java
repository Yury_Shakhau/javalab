package com.epam.xslproject.controller.actiondispatcher.dispatch;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.businesslayer.BusinesslayerException;
import com.epam.xslproject.businesslayer.UrlHelper;
import com.epam.xslproject.controller.ControllerExeption;

public class Redirect implements IForwarOrRedirect {

	private String uri;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public static void redirect(HttpServletRequest request,
			HttpServletResponse response, Object bean, String methodName)
			throws BusinesslayerException {
		try {

			String destinationUrl = UrlHelper.buildUrl(request, bean, methodName);
			response.sendRedirect(destinationUrl);

		} catch (IOException e) {
			throw new BusinesslayerException(e);
		}
	}
	
	@Override
	public void forwardOrRedirect(HttpServletRequest request,
			HttpServletResponse response) throws ControllerExeption {
		try {
			response.sendRedirect(uri);
		} catch (IOException e) {
			throw new ControllerExeption(e);
		}
	}
}
