package com.epam.xslproject.util.xsltemplate;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

public final class TemplateUtil {

	private static final TransformerFactory FACTORY = TransformerFactory
			.newInstance();

	private static ConcurrentMap<String, Templates> 
						TEMPLATE_CONTAINER = new ConcurrentHashMap<>();

	public static final Transformer createTransformer(String xslPath)
			throws TransformerConfigurationException {
		Templates templates = TEMPLATE_CONTAINER.get(xslPath);
		if (templates == null) {
			Source xslSource = new StreamSource(new File(xslPath));
			templates = FACTORY.newTemplates(xslSource);
			TEMPLATE_CONTAINER.putIfAbsent(xslPath, templates);
		}
		return templates.newTransformer();
	}
}
