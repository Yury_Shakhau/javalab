package com.epam.xslproject.model.bean;

public final class Parameter {

	public static final String CATEGORY_NAME = "categoryName";
	public static final String SUB_CATEGORY_NAME = "subcategoryName";
	
	private String categoryName;

	private String subcategoryName;
	
	private Product product;
	
	public String getCategoryName() {
		return categoryName;
	}
	
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	public String getSubcategoryName() {
		return subcategoryName;
	}
	
	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
