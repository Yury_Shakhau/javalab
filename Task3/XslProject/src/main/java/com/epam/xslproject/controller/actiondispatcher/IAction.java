package com.epam.xslproject.controller.actiondispatcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.controller.ControllerExeption;

/**
 * 
 * @author Yury_Shakhau
 *
 */
public interface IAction {

	String getScope();

	void init();

	void execute(HttpServletRequest request, HttpServletResponse response,
			Object form) throws ControllerExeption;
}
