package com.epam.xslproject.controller.actiondispatcher;

import java.util.HashMap;
import java.util.Map;

public class ActionContainer {

	private Map<String, IAction> actions;

	public ActionContainer() {
		actions = new HashMap<>();
	}

	public void setActions(Map<String, IAction> actions) {
		this.actions = actions;
	}
	
	public IAction getAction(String className) {
		return actions.get(className);
	}
	
	public void addAction(String className, IAction action) {
		if (actions.get(className) == null) {
			actions.put(className, action);
		}
	}
}
