package com.epam.xslproject.controller;

/**
* The exception class of business layer.
* It's inherited from SQLException class
* 
* @author Yury_Shakhau
*/
public class ControllerExeption extends Exception {

	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 4699902334668435921L;

	public ControllerExeption() {
		super();
	}
	
	public ControllerExeption(String message) {
		super(message);
	}
	
	public ControllerExeption(Throwable t) {
		super(t);
	}
	
	public ControllerExeption(String message, Throwable t) {
		super(message, t);
	}
}