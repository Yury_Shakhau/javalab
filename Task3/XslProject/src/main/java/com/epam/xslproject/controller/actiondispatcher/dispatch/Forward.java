package com.epam.xslproject.controller.actiondispatcher.dispatch;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.controller.ControllerExeption;

public class Forward implements IForwarOrRedirect {

	private String uri;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public void forwardOrRedirect(HttpServletRequest request,
			HttpServletResponse response) throws ControllerExeption {
		try {
			request.getRequestDispatcher(uri).forward(request, response);
		} catch (ServletException | IOException e) {
			throw new ControllerExeption(e);
		}
	}
}
