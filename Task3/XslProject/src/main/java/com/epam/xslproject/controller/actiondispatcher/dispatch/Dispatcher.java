package com.epam.xslproject.controller.actiondispatcher.dispatch;

import java.util.Map;

import com.epam.xslproject.controller.actiondispatcher.ActionContainer;

public class Dispatcher {

	private static ActionContainer actionContainer;
	
	private static Map<String, Dispatch> forwards;

	public Dispatcher() {
		actionContainer = new ActionContainer();
	}
	
	public static ActionContainer getActionContainer() {
		return actionContainer;
	}

	public Map<String, Dispatch> getForwards() {
		return forwards;
	}

	public static Map<String, Dispatch> forwards() {
		return forwards;
	}
	
	public void setForwards(Map<String, Dispatch> forwards) {
		Dispatcher.forwards = forwards;
	}

	public static void destroy() {
		forwards = null;
		actionContainer = null;
	}
}
