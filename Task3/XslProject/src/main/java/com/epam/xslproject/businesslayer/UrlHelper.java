package com.epam.xslproject.businesslayer;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.servlet.http.HttpServletRequest;

import com.epam.xslproject.action.HeaderName;

public final class UrlHelper {

	public static final String QUESTION = "?";
	public static final String AMP = "&";
	public static final String EQUAL = "=";
	public static final String METHOD = "method";

	/**
	 * It is builds url by bean parameters.
	 * It is appends bean not static and not null fields to url.
	 * 
	 * @param request
	 * @param parameter
	 * @return
	 * @throws BusinesslayerException
	 */
	public static final String buildUrl(HttpServletRequest request, Object bean, String methodName)
			throws BusinesslayerException {
		StringBuilder fullUrl = new StringBuilder();
		String uri = request.getHeader(HeaderName.REFERER);
		uri = uri.split("\\" + QUESTION)[0];
		fullUrl.append(uri).append(QUESTION).append(METHOD).append(EQUAL)
				.append(methodName);
		Class<?> beanClass = bean.getClass();
		Field[] fields = beanClass.getDeclaredFields();
		
		// appends bean not static and not null fields to url.
		try {
			for (Field field : fields) {
				if (!Modifier.isStatic(field.getModifiers())) {
					field.setAccessible(true);
					Object value = field.get(bean);
					if (value != null) {
						fullUrl.append(AMP)
								.append(field.getName())
								.append(EQUAL)
								.append(value);
					}
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new BusinesslayerException(e);
		}
		
		return fullUrl.toString();
	}
}
