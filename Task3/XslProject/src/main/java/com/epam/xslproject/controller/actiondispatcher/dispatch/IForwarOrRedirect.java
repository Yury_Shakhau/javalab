package com.epam.xslproject.controller.actiondispatcher.dispatch;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.controller.ControllerExeption;

public interface IForwarOrRedirect {

	String getUri();
	
	void setUri(String uri);
	
	void forwardOrRedirect(HttpServletRequest request, HttpServletResponse response)
				throws ControllerExeption;
}
