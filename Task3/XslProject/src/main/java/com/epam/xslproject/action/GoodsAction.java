package com.epam.xslproject.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.businesslayer.BusinesslayerException;
import com.epam.xslproject.businesslayer.XslProductTransformer;
import com.epam.xslproject.controller.ControllerExeption;
import com.epam.xslproject.controller.actiondispatcher.DispatchAction;
import com.epam.xslproject.model.bean.Parameter;

public class GoodsAction extends DispatchAction {

	// private static final String SUCCESS = "success";

	@Override
	public void init() {
		super.init();
	}

	public String categories(HttpServletRequest request,
			HttpServletResponse response, Object form)
			throws ControllerExeption {

		try {
			XslProductTransformer.transformRead(request, response,
					XslProductTransformer.GOODS_XML,
					XslProductTransformer.CATEGORIES_XSL, (Parameter) form,
					false);
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}

		return null;
	}

	public String subcategories(HttpServletRequest request,
			HttpServletResponse response, Object form)
			throws ControllerExeption {

		try {
			XslProductTransformer.transformRead(request, response,
					XslProductTransformer.GOODS_XML,
					XslProductTransformer.SUBCATEGORIES_XSL, (Parameter) form,
					false);
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}

		return null;
	}

	public String products(HttpServletRequest request,
			HttpServletResponse response, Object form)
			throws ControllerExeption {

		try {
			XslProductTransformer
					.transformRead(request, response,
							XslProductTransformer.GOODS_XML,
							XslProductTransformer.PRODUCTS_XSL,
							(Parameter) form, false);
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}

		return null;
	}

	public String addingProduct(HttpServletRequest request,
			HttpServletResponse response, Object form)
			throws ControllerExeption {

		try {
			XslProductTransformer.transformRead(request, response,
					XslProductTransformer.GOODS_XML,
					XslProductTransformer.ADDING_PRODUCT_XSL, (Parameter) form,
					true);
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}

		return null;
	}

	public String addProduct(HttpServletRequest request,
			HttpServletResponse response, Object form)
			throws ControllerExeption {

		Parameter parameter = (Parameter) form;
		try {
			XslProductTransformer.transformSaveOnValid(request, response,
					XslProductTransformer.GOODS_XML,
					XslProductTransformer.ADDING_PRODUCT_XSL, parameter, false);
		} catch (BusinesslayerException e) {
			throw new ControllerExeption(e);
		}

		return null;
	}
}
