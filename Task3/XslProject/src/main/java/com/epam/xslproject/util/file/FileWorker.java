package com.epam.xslproject.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class FileWorker {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(FileWorker.class);
	
	public static void writeFile(String filePath, OutputStream stream) {
		Writer writer = null;
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(filePath);
			writer = new OutputStreamWriter(os);
			writer.write(stream.toString());
		} catch (IOException e) {
			LOGGER.error("Writer can not write file " + filePath, e);
		} finally {
			if (writer != null) {
				try {
					writer.flush();
					writer.close();
					os.flush();
					os.close();
				} catch (IOException e) {
					LOGGER.error("StreamWriter can not be closed for file " + filePath, e);
				}
			}
		}
	}
	
	public static String readTextFile(String filePath) {
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = null;
		InputStreamReader isr = null;
		FileInputStream is = null;
		try {
			is = new FileInputStream(filePath);
			isr = new InputStreamReader(is);
			reader = new BufferedReader(isr);
			String line = null;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
		} catch (IOException e) {
			LOGGER.error("Reader can not write file " + filePath, e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
					is.close();
				} catch (IOException e) {
					LOGGER.error("StreamWriter can not be closed for file " + filePath, e);
				}
			}
		}
		return sb.toString();
	}
	
	public static ByteArrayOutputStream readFile(String path) throws IOException {
	    ByteArrayOutputStream out = null;
	    InputStream input = null;
	    try{
	        out = new ByteArrayOutputStream();
	        input = new BufferedInputStream(new FileInputStream(path));
	        int data = 0;
	        while ((data = input.read()) != -1){
	          out.write(data);
	        }
	    }
	    finally{
	       if (null != input){
	           input.close();
	       }
	       if (null != out){
	           out.close();
	       }
	    }
	    return out;
	}
	
	public static String fullPath(HttpServletRequest request, String fileSubPath) {
		String realPath = request.getServletContext().getRealPath("/");
		return realPath + fileSubPath.substring(1);
	}
}
