package com.epam.xslproject.controller.actiondispatcher.dispatch;

import java.util.Map;

public class Dispatch {

	private String actionCalss;
	private String actionMethod;
	private Map<String, IForwarOrRedirect> forwards;
	private String actionFromName;
	private String actionFrom;
	private String scope;

	public void setActionFrom(String actionFrom) {
		this.actionFrom = actionFrom;
	}

	public String getActionFrom() {
		return actionFrom;
	}

	public String getActionCalss() {
		return actionCalss;
	}
	
	public void setActionCalss(String actionCalss) {
		this.actionCalss = actionCalss;
	}
	
	public String getActionMethod() {
		return actionMethod;
	}

	public void setActionMethod(String actionMethod) {
		this.actionMethod = actionMethod;
	}

	public Map<String, IForwarOrRedirect> getForwards() {
		return forwards;
	}
	
	public void setForwards(Map<String, IForwarOrRedirect> forwards) {
		this.forwards = forwards;
	}
	
	public String getActionFromName() {
		return actionFromName;
	}

	public void setActionFromName(String actionFromName) {
		this.actionFromName = actionFromName;
	}
	
	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
