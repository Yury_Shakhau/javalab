package com.epam.xslproject.controller.beancreation;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.epam.xslproject.businesslayer.UrlHelper;
import com.epam.xslproject.controller.ControllerExeption;

public class ActionFormFactory {

	private static final String DOT_PATTERN = "\\.";

	private static final String CHECKBOX_TRUE_VALUE = "on";

	/**
	 * It is converts String to real bean value type.
	 * 
	 * @author Yury_Shakhau
	 *
	 */
	private static class Converter {

		private static interface IConverter {
			Object convert(String value);
		}
		/**
		 * returns String value.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class StringConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return value;
			}
		}
		
		/**
		 * Converts String to Boolean.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class BooleanConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return value.equals(CHECKBOX_TRUE_VALUE) ? true : new Boolean(value);
			}
		}
		
		/**
		 * Converts String to Integer.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class IntegerConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return Integer.parseInt(value);
			}
		}
		
		/**
		 * Converts String to Long.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class LongConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return Long.parseLong(value);
			}
		}

		/**
		 * Converts String to Double.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class DoubleConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return Double.parseDouble(value);
			}
		}

		/**
		 * Converts String to Float.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class FloatConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return Double.parseDouble(value);
			}
		}

		/**
		 * Converts String to Float.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static class BigIntegerConverter implements IConverter {
			
			@Override
			public Object convert(String value) {
				return new BigInteger(value);
			}
		}
		
		/**
		 * Converter container to getting converter as we nead.
		 */
		private static Map<Class<?>, IConverter> converters = new HashMap<>();
		
		/**
		 * Converter container initialization.
		 */
		static {
			converters.put(String.class, new StringConverter());
			converters.put(Boolean.class, new BooleanConverter());
			converters.put(Integer.class, new IntegerConverter());
			converters.put(Long.class, new LongConverter());
			converters.put(Double.class, new DoubleConverter());
			converters.put(Float.class, new FloatConverter());
			converters.put(BigInteger.class, new BigIntegerConverter());
		}

		/**
		 * It is converts String to real bean value type.
		 * 
		 * @param fieldType ban value type.
		 * @param value bean value in a String view/.
		 * @return value converted value to real bean value type.
		 */
		public static Object convert(Class<?> fieldType, String value) {
			IConverter converter = converters.get(fieldType);
			return converter != null ? converter.convert(value) : null;
		}

	}
	
	/**
	 * Builds bean object by fieldNames and value parameters/
	 * 
	 * @param obj bean to build.
	 * @param fieldNames sentence fields in a bean.
	 * @param value bean value.
	 * @throws ControllerExeption throws on reflections exception.
	 */
	public static void buildObject(Object obj, String[] fieldNames, Object value)
			throws ControllerExeption {
		Class<?> objClass = obj.getClass();
		int lastIndex = fieldNames.length - 1;
		try {
			if (lastIndex > -1) {
				for (int i = 0; i < lastIndex; i++) {
					String fieldName = fieldNames[i];
					Field field = objClass.getDeclaredField(fieldName);
					field.setAccessible(true);
					Object fieldObj = field.get(obj);
					if (fieldObj == null) {
						Class<?> fieldType = field.getType();
						fieldObj = fieldType.newInstance();
						field.set(obj, fieldObj);
					}
					objClass = fieldObj.getClass();
					obj = fieldObj;
				}
				String fieldName = fieldNames[lastIndex];
				Field field = objClass.getDeclaredField(fieldName);
				field.setAccessible(true);
				
				// Converts String value to real variable type.
				value = Converter.convert(field.getType(), (String) value);
				field.set(obj, value);
			}
		} catch (NoSuchFieldException | SecurityException
				| InstantiationException | IllegalAccessException
				| IllegalArgumentException e) {
			throw new ControllerExeption(e);
		}
	}

	public static Object createAndBuildActionForm(HttpServletRequest request,
			String actionFromClassName, Object form) throws ControllerExeption {

		// If bean exists.
		if (actionFromClassName != null) {

			// request parameters.
			Map<String, String[]> parameterMap = request.getParameterMap();
			try {
				if (form == null) {
					Class<?> actionFormClass = Class.forName(actionFromClassName);
					form = actionFormClass.newInstance();
				}
				Set<Entry<String, String[]>> params = parameterMap.entrySet();
				for (Entry<String, String[]> param : params) {
					String fieldName = param.getKey();
					if (!UrlHelper.METHOD.equals(fieldName)) {
						String[] fieldNames = fieldName.split(DOT_PATTERN);
						buildObject(form, fieldNames,
								param.getValue()[0]);
					}
				}
			} catch (ClassNotFoundException | SecurityException
					| InstantiationException | IllegalAccessException
					| IllegalArgumentException e) {
				e.printStackTrace();
				throw new ControllerExeption(e);
			}
		}

		return form;
	}
}
