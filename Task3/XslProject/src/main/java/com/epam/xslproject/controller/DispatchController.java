package com.epam.xslproject.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.epam.xslproject.businesslayer.UrlHelper;
import com.epam.xslproject.controller.actiondispatcher.ActionContainer;
import com.epam.xslproject.controller.actiondispatcher.DispatchAction;
import com.epam.xslproject.controller.actiondispatcher.IAction;
import com.epam.xslproject.controller.actiondispatcher.dispatch.Dispatch;
import com.epam.xslproject.controller.actiondispatcher.dispatch.Dispatcher;
import com.epam.xslproject.controller.actiondispatcher.dispatch.IForwarOrRedirect;
import com.epam.xslproject.controller.beancreation.ActionFormFactory;

/**
 * MVC controller.
 * Creates beans by url, calls action methods 
 * and forwards or redirects to references by action replies.
 * Also can saves beans in session or request.
 * All of that must be mapped in in applicationCotext.xml
 * with spring injection.
 * 
 * @author Yury_Shakhau
 *
 */
@WebServlet(urlPatterns = DispatchController.URL_PATTERN)
public class DispatchController extends HttpServlet {

	public static final String URL_PATTERN = "/ActionController";

	private static final String ERROR_PAGE_404 = "jsp/errors/error404.jsp";
	
	/**
	 * Types for action method to invoke.
	 */
	private static final Class<?>[] methodClasses = new Class<?>[] {
			HttpServletRequest.class, HttpServletResponse.class,
			Object.class };

	/**
	 * Indentifer for deserialization.
	 */
	private static final long serialVersionUID = 175628374264523452L;
	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(DispatchController.class);

	/**
	 * Starts spring injection for load action config.
	 * Creates actions and adds its to action container.
	 */
	@Override
	public void init() throws ServletException {
		super.init();

		// Initialization with spring injection.
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(context);
		ctx.getBean(Dispatcher.class);

		ActionContainer container = Dispatcher.getActionContainer();

		// Creating actions by mapping.
		Map<String, Dispatch> forwards = Dispatcher.forwards();
		Set<Entry<String, Dispatch>> actionNameClass = forwards.entrySet();
		for (Entry<String, Dispatch> entry : actionNameClass) {
			try {
				Class<?> actionClass = Class.forName(entry.getValue()
						.getActionCalss());
				IAction action = (IAction) actionClass.newInstance();
				container.addAction(entry.getKey(), action);
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException e) {
				LOGGER.error(e);
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			doAction(request, response);
		} catch (ControllerExeption e) {
			LOGGER.error(e);
			e.printStackTrace();
			try {
				request.getRequestDispatcher(ERROR_PAGE_404).forward(request, response);
			} catch (IOException e1) {
				LOGGER.error(e);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			doAction(request, response);
		} catch (ControllerExeption e) {
			e.printStackTrace();
			LOGGER.error(e);
			try {
				request.getRequestDispatcher(ERROR_PAGE_404).forward(request, response);
			} catch (IOException e1) {
				LOGGER.error(e);
			}
		}
	}

	/**
	 * Runs when doGet or doPost was called.
	 * 
	 * @param request
	 * @param response
	 */
	private void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ControllerExeption {
		
		ActionContainer container = Dispatcher.getActionContainer();
		String dispatchName = request.getParameter(UrlHelper.METHOD);
		Map<String, Dispatch> forwards = Dispatcher.forwards();
		Dispatch dispatch = forwards.get(dispatchName);
		
		String uriName = null;
			if (dispatch != null) {
				String methodName = dispatch.getActionMethod();

				try {

					// Bean creation by url request parameters.
					Object form = null;
					String scope = dispatch.getScope();

					if (scope != null) {
						if (scope.equals(DispatchAction.SESSION)) {
							String actionFormName = dispatch.getActionFromName();
							form = request.getSession().getAttribute(actionFormName);
							form = ActionFormFactory.createAndBuildActionForm(
									request, dispatch.getActionFrom(), form);
							request.getSession().setAttribute(actionFormName, form);
						} else {
							form = ActionFormFactory.createAndBuildActionForm(
									request, dispatch.getActionFrom(), form);
							if (scope.equals(DispatchAction.REQUEST)) {
								String actionFormName = dispatch.getActionFromName();
								request.setAttribute(actionFormName, form);
							}
						}
					}
					
					// Parameters to sent to method to invoke by reflection.
					Object[] ojectsMethod = new Object[] { request, response,
							form };

					IAction action = container.getAction(dispatchName);
					Class<?> actionClass = action.getClass();
					action.init();
					
					// If method exists in this action mapping.
					if (methodName != null) {
						Method actionMethod = actionClass.getMethod(methodName,
								methodClasses);
						uriName = (String) actionMethod
								.invoke(action, ojectsMethod);
					} else {
						
						// If method not exists in this action mapping.
						// And this method must be overload in this way.
						// Other way it will be throwing Controller exception.
						action.execute(request, response, form);
					}
				} catch (NoSuchMethodException | SecurityException
						| IllegalAccessException | InvocationTargetException e) {
					throw new ControllerExeption(e);
				}
			}

			// If action returned uriName to forward or redirect.
			if (uriName != null && uriName.length() > 0) {
				Map<String, IForwarOrRedirect> forwardOrRedirects = dispatch
						.getForwards();
				if (forwardOrRedirects != null) {

					// Searches forward or redirect by its name.
					IForwarOrRedirect forwarOrRedirect = forwardOrRedirects
							.get(uriName);
					if (forwarOrRedirect != null) {
						forwarOrRedirect.forwardOrRedirect(request, response);
					}
				}
			}
	}

	/**
	 * Sets null to static references to destroy them objects with GC.
	 */
	@Override
	public void destroy() {
		super.destroy();
		ActionContainer container = Dispatcher.getActionContainer();
		container.setActions(null);
		container = null;
		Dispatcher.destroy();
	}
}
