package com.epam.xslproject.businesslayer;

public class BusinesslayerException extends Exception {

	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 4699324546782421L;

	public BusinesslayerException() {
		super();
	}
	
	public BusinesslayerException(String message) {
		super(message);
	}
	
	public BusinesslayerException(Throwable t) {
		super(t);
	}
	
	public BusinesslayerException(String message, Throwable t) {
		super(message, t);
	}
}
