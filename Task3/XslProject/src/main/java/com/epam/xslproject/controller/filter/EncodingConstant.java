package com.epam.xslproject.controller.filter;

public final class EncodingConstant {

	private EncodingConstant() {}
	
	public static final String ENCODING_WORD = "encoding";
	
	public static final String DESCRIPTION = "Encoding Param";
	
	public static final String UTF8 = "UTF-8";
}
