package com.epam.xslproject.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

import org.apache.log4j.Logger;

/**
* Intercept user request for changing request encoding.
* The follow annotation does work from web 3.0 version.
*/
@WebFilter(urlPatterns = { "/*" },
		  initParams = {
		  @WebInitParam(name = EncodingConstant.ENCODING_WORD, value = EncodingConstant.UTF8, 
				  description = EncodingConstant.DESCRIPTION)})
public class EncodingFilter implements Filter {

	/**
	 * Page encoding.
	 */
	private String encoding;

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);
	
	/**
	 * Tunes fConfig object.
	 * @param fConfig pass to this methos for tune.
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		encoding = fConfig.getInitParameter(EncodingConstant.ENCODING_WORD);
	}
	
	/**
	 * Sets request encoding as UTF-8.
	 * @param chain filter list for processing of requests.
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) {
		try {
			String codeReq = req.getCharacterEncoding();
			if (encoding != null && !encoding.equalsIgnoreCase(codeReq)) {
				req.setCharacterEncoding(encoding);
				resp.setCharacterEncoding(encoding);
			}
			chain.doFilter(req, resp);
		} catch (IOException | ServletException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}

	@Override
	public void destroy() {
		encoding = null;
	}
}
