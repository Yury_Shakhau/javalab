package com.epam.xslproject.businesslayer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.epam.xslproject.controller.actiondispatcher.dispatch.Redirect;
import com.epam.xslproject.model.bean.Parameter;
import com.epam.xslproject.model.bean.Product;
import com.epam.xslproject.util.file.FileWorker;
import com.epam.xslproject.util.xsltemplate.TemplateUtil;

public final class XslProductTransformer {

	private static final String XML_EXTENTION = ".xml";
	private static final String XSL_EXTENTION = ".xsl";

	public static final String CATALOG = "\\WEB-INF\\xml";

	public static final String CATEGORIES_XSL = CATALOG + "\\categories.xsl";

	public static final String SUBCATEGORIES_XSL = CATALOG
			+ "\\subcategories.xsl";

	public static final String PRODUCTS_XSL = CATALOG + "\\products.xsl";

	public static final String ADDING_PRODUCT_XSL = CATALOG
			+ "\\adding-product.xsl";

	public static final String GOODS_XML = CATALOG + "\\goods.xml";

	public static final String PRODUCTS_METHOD_NAME = "products";

	private static final ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();
	private static final Lock LOCKER = new ReentrantLock();

	private static final int MAX_THREAD_COUNT = 256;
	private static final ThreadCounter THREAD_COUNTER = new ThreadCounter(MAX_THREAD_COUNT);
	
	private static ByteArrayOutputStream xmlCache;

	private XslProductTransformer() {
	}

	/**
	 * Prepares transformer before transformation.
	 * 
	 * @param request
	 * @param xslPath
	 * @param parameter
	 * @param createProduct
	 * @return
	 * @throws BusinesslayerException
	 */
	private static Transformer prepareTransformer(HttpServletRequest request,
			String xslPath, Parameter parameter, boolean createProduct)
			throws BusinesslayerException {
		Transformer transformer = null;

		// If it is not xsl, will thrown exception there.
		if (!xslPath.toLowerCase().endsWith(XSL_EXTENTION)) {
			throw new BusinesslayerException(
					"Xsl extention is .xsl in the end of file path");
		}

		try {

			String xslFullPath = FileWorker.fullPath(request, xslPath);

			transformer = TemplateUtil.createTransformer(xslFullPath);
			if (parameter != null) {
				if (parameter.getCategoryName() != null) {
					transformer.setParameter(Parameter.CATEGORY_NAME,
							parameter.getCategoryName());
				}
				if (parameter.getSubcategoryName() != null) {
					transformer.setParameter(Parameter.SUB_CATEGORY_NAME,
							parameter.getSubcategoryName());
				}
				Product product = parameter.getProduct();
				if (createProduct && product == null) {
					product = new Product();
					parameter.setProduct(product);
				}
				if (product != null) {
					if (product.getName() != null) {
						transformer.setParameter(Product.NAME,
								product.getName());
					}
					if (product.getProvider() != null) {
						transformer.setParameter(Product.PROVIDER,
								product.getProvider());
					}
					if (product.getModel() != null) {
						transformer.setParameter(Product.MODEL,
								product.getModel());
					}
					if (product.getDateOfIssue() != null) {
						transformer.setParameter(Product.DATE_OF_ISSUE,
								product.getDateOfIssue());
					}
					if (product.getColor() != null) {
						transformer.setParameter(Product.COLOR,
								product.getColor());
					}
					transformer.setParameter(Product.NOT_IN_STOCK,
							product.isNotInStock());
					if (product.getPrice() != null) {
						transformer.setParameter(Product.PRICE,
								product.getPrice());
					}
					transformer.setParameter(Product.THIS_XSL_NAME, product);
				}
			}

		} catch (TransformerException e) {
			throw new BusinesslayerException(e);
		}

		return transformer;
	}

	// Synchronized read to cache method
	private static ByteArrayOutputStream tryReadFile(String filePath,
			ByteArrayOutputStream cache) throws BusinesslayerException {
		boolean neadUnlock = false;
		try {
			if (cache == null) {
				LOCKER.lock();
				neadUnlock = true;
				if (cache == null) {
					cache = FileWorker.readFile(filePath);
				}
			}
		} catch (IOException e) {
			throw new BusinesslayerException(e);
		} finally {
			if (neadUnlock) {
				LOCKER.unlock();
			}
		}
		return cache;
	}

	// Write to cache method. Must needed in write locked parts of code only.
	private static void tryWriteFile(String filePath,
			ByteArrayOutputStream cache) {
		if (THREAD_COUNTER.isThatLastOrCountedThread()) {
			FileWorker.writeFile(filePath, cache);
			THREAD_COUNTER.resetMaxThreadCounter();
		}

			
		THREAD_COUNTER.threadGone();
	}

	/**
	 * Transform document with writing to response stream.
	 * 
	 * @param request
	 * @param response
	 * @param xmlPath path to xml to read.
	 * @param xslPath path to xsl to read.
	 * @param parameter bean with data.
	 * @param createProduct is need create product if that did not create.
	 * @throws BusinesslayerException
	 */
	public static void transformRead(HttpServletRequest request,
			HttpServletResponse response, String xmlPath, String xslPath,
			Parameter parameter, boolean createProduct)
			throws BusinesslayerException {

		// If it is not xml, will thrown exception there.
		if (!xmlPath.toLowerCase().endsWith(XML_EXTENTION)) {
			throw new BusinesslayerException(
					"Xml extention is .xml in the end of file path");
		}

		Lock lock = READ_WRITE_LOCK.readLock();
		boolean neadUnlock = false;

		try {
			Transformer transformer = prepareTransformer(request, xslPath,
					parameter, createProduct);

			if (xmlCache == null) {
				String xmlFullPath = FileWorker.fullPath(request, xmlPath);
				xmlCache = tryReadFile(xmlFullPath, xmlCache);
			}
			Writer writer = response.getWriter();
			Result result = new StreamResult(writer);

			lock.lock();
			neadUnlock = true;
			ByteArrayInputStream inputXmlBuf = new ByteArrayInputStream(
					xmlCache.toByteArray());
			StreamSource xmlSource = new StreamSource(inputXmlBuf);

			transformer.transform(xmlSource, result);
			neadUnlock = false;
			lock.unlock();
		} catch (TransformerException | IOException e) {
			throw new BusinesslayerException(e);
		} finally {
			if (neadUnlock) {
				xmlCache = null;
				lock.unlock();
			}
		}
	}

	/**
	 * Makes transformation to ByteArrayOutputStream and return that.
	 * 
	 * @param transformer transform object.
	 * @param xmlBuff cache for transformation.
	 * @return ByteArrayOutputStream, the result of transformatio.
	 * @throws BusinesslayerException
	 */
	public static ByteArrayOutputStream transform(Transformer transformer,
			ByteArrayOutputStream xmlBuff) throws BusinesslayerException {
		ByteArrayInputStream buffInputStream = new ByteArrayInputStream(
				xmlBuff.toByteArray());
		Source xmlSource = new StreamSource(buffInputStream);
		ByteArrayOutputStream buffOutputStream = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(buffOutputStream);
		Result result = new StreamResult(writer);

		try {
			transformer.transform(xmlSource, result);
		} catch (TransformerException e) {
			throw new BusinesslayerException(e);
		}

		return buffOutputStream;
	}

	/**
	 * Transform document with saving it is to xml file if it is valid. Else
	 * it'll be staying in current page.
	 * 
	 * @param request
	 * @param response
	 * @param xmlPath
	 * @param xslPath
	 * @param parameter
	 * @param createProduct
	 *            createProduct if it is true, will be creating new bean if that
	 *            null then.
	 * @throws BusinesslayerException
	 * @throws
	 */
	public static void transformSaveOnValid(HttpServletRequest request,
			HttpServletResponse response, String xmlPath, String xslPath,
			Parameter parameter, boolean createProduct)
			throws BusinesslayerException {

		// If it is not xml, will thrown exception there.
		if (!xmlPath.toLowerCase().endsWith(XML_EXTENTION)) {
			throw new BusinesslayerException(
					"Xml extention is .xml in the end of file path");
		}

		Lock lock = null;
		boolean neadUnlock = false;
		String xmlFullPath = FileWorker.fullPath(request, xmlPath);

		try {
			Transformer transformer = prepareTransformer(request, xslPath,
					parameter, createProduct);

			lock = READ_WRITE_LOCK.readLock();

			lock.lock();
			neadUnlock = true;
				if (xmlCache == null) {
					xmlCache = tryReadFile(xmlFullPath, xmlCache);
				}

				THREAD_COUNTER.nextThread();
				ByteArrayOutputStream buffOutputStream = transform(transformer,
						xmlCache);
			neadUnlock = false;
			lock.unlock();

			if (parameter.getProduct().isValid()) {
				lock = READ_WRITE_LOCK.writeLock();

				lock.lock();
				neadUnlock = true;

					if (THREAD_COUNTER.isNotFirstOrSingleThread()) {
						buffOutputStream = transform(transformer, xmlCache);
					}
					xmlCache = buffOutputStream;
					tryWriteFile(xmlFullPath, xmlCache);
				neadUnlock = false;
				lock.unlock();

				parameter.setProduct(null);
				Redirect.redirect(request, response, parameter,
						PRODUCTS_METHOD_NAME);
			} else {
				Writer responseWriter = response.getWriter();
				
				lock = READ_WRITE_LOCK.readLock();
				lock.lock();
				neadUnlock = true;
					responseWriter.write(buffOutputStream.toString());
				neadUnlock = false;
				lock.unlock();

			}

		} catch (IOException e) {
			throw new BusinesslayerException(e);
		} finally {
			if (neadUnlock) {
				tryWriteFile(xmlFullPath, xmlCache);
				lock.unlock();
			}
		}
	}
}
