package com.epam.xslproject.controller.actiondispatcher;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xslproject.controller.ControllerExeption;
import com.epam.xslproject.controller.actiondispatcher.dispatch.IForwarOrRedirect;

public class DispatchAction implements IAction {

	public static final String SESSION = "session";

	public static final String REQUEST = "request";

	private String actionMethod;
	private Map<String, IForwarOrRedirect> forwards;
	private Map<String, String> actionFroms;

	private String scope;

	public void setActionFroms(Map<String, String> actionFroms) {
		this.actionFroms = actionFroms;
	}

	public Map<String, String> getActionFroms() {
		return actionFroms;
	}

	public String getActionMethod() {
		return actionMethod;
	}

	public void setActionMethod(String actionMethod) {
		this.actionMethod = actionMethod;
	}

	public Map<String, IForwarOrRedirect> getForwards() {
		return forwards;
	}

	public void setForwards(Map<String, IForwarOrRedirect> forwards) {
		this.forwards = forwards;
	}

	@Override
	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public void init() {
		if (scope == null) {
			scope = SESSION;
		}
	}

	@Override
	public void execute(HttpServletRequest request,
			HttpServletResponse response, Object form)
				throws ControllerExeption {
		throw new ControllerExeption("Method execute of DispatchAction is unsupported.");
	}
}
