<%@ page isErrorPage="true" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<title>Error</title>
	</head>
	<body>
		<h1>Error was found.</h1>
		<a class="back-button-link" href="${pageContext.request.contextPath}/index.jsp">
			<div class="button">
				To main page
			</div>
		</a>
	</body>
</html>