function showHideElement(id, checkBox) {
	var element = document.getElementById(id);
	var display = checkBox.checked ? 'none' : 'block';
	element.style.display = display;
}

function visibleElement(id, idCheckBox) {
	var checkBox = document.getElementById(idCheckBox);
	showHideElement(id, checkBox);
}

function changeColorToGreen(textBox, textElement) {
	if (textElement != null) {
		if (textBox.value.length > 0) {
			textElement.style.color = '#00FF00';
		}
	}
}

function changeColorToGreenById(textBoxId, textElementId) {
	var textBox = document.getElementById(textBoxId);
	var textElement = document.getElementById(textElementId);
	changeColorToGreen(textBox, textElement);
}

function changeColorToGreenWithId(textBox, textElementId) {
	var textElement = document.getElementById(textElementId);
	changeColorToGreen(textBox, textElement);
}
