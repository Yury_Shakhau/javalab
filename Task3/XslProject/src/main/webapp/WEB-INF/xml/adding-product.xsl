<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:product="xalan//:com.epam.xslproject.model.bean.Product">

	<xsl:import href="add-product.xsl"/>


	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />
	
	<xsl:param name="product" />

	<xsl:variable name="name" select="product:getName($product)" />
	<xsl:variable name="provider" select="product:getProvider($product)" />
	<xsl:variable name="model" select="product:getModel($product)" />
	<xsl:variable name="dateOfIssue" select="product:getDateOfIssue($product)" />
	<xsl:variable name="price" select="product:getPrice($product)" />
	<xsl:variable name="color" select="product:getColor($product)" />
	<xsl:variable name="notInStock" select="product:isNotInStock($product)" />

	<xsl:template match="/">
	
		<xsl:choose>
			<xsl:when test="product:isValid($product)">
				<xsl:apply-imports/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="adding-product" />
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	
	<xsl:output method="html" />
	
	<xsl:template name="adding-product">
		
		<html>
			<head>
				<title>Adding to <xsl:value-of select="$subcategoryName" /></title>
				<link href="css/style.css" rel="stylesheet" type="text/css" />
				<script src="js/script.js"></script>
			</head>
			<body>
				<center>
					<div class="center-body">
						<div class="title">Adding to <xsl:value-of select="$subcategoryName" /></div>
						<div class="main-link">
							<a class="inline" href="ActionController?method=categories">
								Catalog
							</a>
							<div class="inline">→</div>
							<a class="inline" href="ActionController?method=subcategories&amp;categoryName={$categoryName}">
								Catalog of <xsl:value-of select="$categoryName" />
							</a>
							<div class="inline">→</div>
							<a class="inline" href="ActionController?method=products&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
								Catalog of <xsl:value-of select="$subcategoryName" />
							</a>
							<a class="button-link" href="ActionController?method=products&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
								<div class="button">
									Back
								</div>
							</a>
						</div>
						<form method="post" action="ActionController">
							<input name="method" type="hidden" value="addProduct" />
							<input name="categoryName" type="hidden" value="{$categoryName}" />
							<input name="subcategoryName" type="hidden" value="{$subcategoryName}" />
							<div>
								<div class="menu-item-product">
									<div class="adding-title">Name</div>
									<div class="adding-field">
										<input id="input-name" class="adding-field-input" name="product.name" type="input" value="{ $name }" placeholder="Name" />
										<xsl:choose>
											<xsl:when test="not(product:isValidLength($product, $name))">
												<span class="warning-message">Name length must be greatter than 0.</span>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="not(product:isValidName($product))">
														<span class="warning-message">Name must contaice digits and latin letters only.</span>
													</xsl:when>
													<xsl:otherwise>
														<span id="name-warning" class="simple-text">Name must contaice digits and latin letters only.</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title">Provider</div>
									<div class="adding-field">
										<input id="input-provider" class="adding-field-input" name="product.provider" type="input" value="{ $provider }" placeholder="Provider" />
										<xsl:choose>
											<xsl:when test="not(product:isValidLength($product, $provider))">
												<span class="warning-message">Name length must be greatter than 0.</span>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="not(product:isValidProvider($product))">
														<span class="warning-message">Provider must contaice digits and latin letters only.</span>
													</xsl:when>
													<xsl:otherwise>
														<span id="provider-warning" class="simple-text">Provider must contaice digits and latin letters only.</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title">Model</div>
									<div class="adding-field">
										<input id="input-model" class="adding-field-input" name="product.model" type="input" value="{ $model }" placeholder="Model" />
										<xsl:choose>
											<xsl:when test="not(product:isValidModel($product))">
												<span class="warning-message">Model must have only 2 latin letters and 3 digits after.</span>
											</xsl:when>
											<xsl:otherwise>
												<span id="model-warning" class="simple-text">Model must have only 2 latin letters and 3 digits after.</span>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title">Date of issue</div>
									<div class="adding-field">
										<input id="input-dateOfIssue" class="adding-field-input" name="product.dateOfIssue" type="input" value="{ $dateOfIssue }" placeholder="dd-MM-yyyy" />
										<xsl:choose>
											<xsl:when test="not(product:isValidDateOfIssue($product))">
												<span class="warning-message">Date diapason form 01-01-1900 to 31-12-9999.</span>
											</xsl:when>
											<xsl:otherwise>
												<span id="dateOfIssue-warning" class="simple-text">Date diapason form 01-01-1900 to 31-12-9999.</span>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title">Color</div>
									<div class="adding-field">
										<input id="input-color" class="adding-field-input" name="product.color" type="input" value="{ $color }" placeholder="Color" />
										<xsl:choose>
											<xsl:when test="not(product:isValidLength($product, $color))">
												<span class="warning-message">Color length must be greatter than 0.</span>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="not(product:isValidColor($product))">
														<span class="warning-message">Color must contaice latin letters only.</span>
													</xsl:when>
													<xsl:otherwise>
														<span id="color-warning" class="simple-text">Color must contaice latin letters only.</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title">Not in stock</div>
									<xsl:choose>
										<xsl:when test="not(product:isNotInStock($product))">
											<input class="check-box" type="checkbox" id="checkbox" name="product.notInStock" onclick="showHideElement('price-div', this)" />
										</xsl:when>
										<xsl:otherwise>
											<input class="check-box" type="checkbox" id="checkbox" name="product.notInStock" checked="checked" onclick="showHideElement('price-div', this)" />
										</xsl:otherwise>
									</xsl:choose>
								</div>
								<div class="menu-item-product" id="price-div">
									<div class="adding-title">Price</div>
									<div class="adding-field">
										<input id="input-price" class="adding-field-input" name="product.price" type="input" value="{ $price }" placeholder="Price" />
										<xsl:choose>
											<xsl:when test="not(product:isValidLength($product, $price))">
												<span class="warning-message">Price length must be greatter than 0.</span>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="not(product:isValidPrice($product))">
														<span class="warning-message">Price must be is a real positive number.</span>
													</xsl:when>
													<xsl:otherwise>
														<span id="price-warning" class="simple-text">Price must be is a real positive number.</span>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
								<div class="menu-item-product">
									<div class="adding-title"></div>
									<div class="adding-field">
										<input class="button adding-button-input" type="submit" value="Add product"/>
									</div>
								</div>
							</div>
						</form>
					</div>
				</center>
			</body>
			<script type="text/javascript">
				window.onload = visibleElement('price-div', 'checkbox');
				window.onload = changeColorToGreenById('input-name', 'name-warning');
				window.onload = changeColorToGreenById('input-provider', 'provider-warning');
				window.onload = changeColorToGreenById('input-model', 'model-warning');
				window.onload = changeColorToGreenById('input-dateOfIssue', 'dateOfIssue-warning');
				window.onload = changeColorToGreenById('input-color', 'color-warning');
				window.onload = changeColorToGreenById('input-price', 'price-warning');
			</script>
		</html>
		
	</xsl:template>
	
</xsl:stylesheet>