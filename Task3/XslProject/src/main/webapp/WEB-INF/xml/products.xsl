<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:param name="categoryName" />
	<xsl:param name="subcategoryName" />

	<xsl:template match="/">
		
		<html>
			<head>
				<title>Catalog of <xsl:value-of select="$subcategoryName" /></title>
				<link href="css/style.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
				<center>
					<div class="center-body">
						<div class="title">Catalog of <xsl:value-of select="$subcategoryName" /></div>
						<div class="main-link">
							<div class="inline">
								<a class="inline" href="ActionController?method=categories">
									Catalog
								</a>
								<div class="inline">→</div>
								<a class="inline" href="ActionController?method=subcategories&amp;categoryName={$categoryName}">
									Catalog of <xsl:value-of select="$categoryName" />
								</a>
							</div>
							<a class="button-link" href="ActionController?method=subcategories&amp;categoryName={$categoryName}">
								<div class="button">
									Back
								</div>
							</a>
							<a class="button-link" href="ActionController?method=addingProduct&amp;categoryName={$categoryName}&amp;subcategoryName={$subcategoryName}">
								<div class="button right-margin">
									Add product
								</div>
							</a>
						</div>
						<div class="menu-item-title">
							<div class="menu-item-product-name">Name</div>
							<div class="menu-item-product-provider">Provider</div>
							<div class="menu-item-product-model">Model</div>
							<div class="menu-item-product-date-of-issue">Date of issue</div>
							<div class="menu-item-product-color">Color</div>
							<div class="menu-item-product-price">Price</div>
						</div>
						<xsl:for-each select="goods/category[@name=$categoryName]/subcategory[@name=$subcategoryName]/product">
							<div class="menu-item-product">
								<div class="menu-value-product-name"><xsl:value-of select="@name" /></div>
								<div class="menu-value-product-provider"><xsl:value-of select="provider" /></div>
								<div class="menu-value-product-model"><xsl:value-of select="model" /></div>
								<div class="menu-value-product-date-of-issue"><xsl:value-of select="date-of-issue" /></div>
								<div class="menu-value-product-color"><xsl:value-of select="color" /></div>
								<div class="menu-value-product-price">
									<xsl:variable name="price" select="price"/>
									<xsl:choose>
										<xsl:when test="price">
											<xsl:value-of select="price" />
										</xsl:when>
										<xsl:otherwise>
											Not in stock
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</center>
			</body>
		</html>
	</xsl:template>
	
</xsl:stylesheet>