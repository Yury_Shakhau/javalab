<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output method="html" />
	
	<xsl:template match="/">
		
		<html>
			
			<head>
				<title>Catalog</title>
				<link href="css/style.css" rel="stylesheet" type="text/css" />
			</head>
			
			<body>
				<center>
					<div class="center-body">
						<div class="title">Catalog</div>
						<div class="menu-item-title">
							<div class="menu-item-name">Name</div>
							<div class="menu-item-count">Product count</div>
						</div>
						<xsl:for-each select="goods/category">
							<xsl:variable name="categoryName" select="@name"/>
							<a class="menu-link" href="ActionController?method=subcategories&amp;categoryName={$categoryName}">
								<div class="menu-item">
									<div class="menu-item-name">
										<xsl:value-of select="@name" />
									</div>
									<div class="menu-item-count">
										<xsl:value-of select="count(subcategory/product)" />
									</div>
								</div>
							</a>
						</xsl:for-each>
					</div>
				</center>
			</body>
		
		</html>
	
	</xsl:template>
	
</xsl:stylesheet>