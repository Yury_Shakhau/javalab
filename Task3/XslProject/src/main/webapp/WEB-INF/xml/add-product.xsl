<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" />

	<xsl:variable name="name" />
	<xsl:variable name="provider" />
	<xsl:variable name="model" />
	<xsl:variable name="dateOfIssue" />
	<xsl:variable name="price" />
	<xsl:variable name="color" />
	<xsl:variable name="notInStock" />

	<xsl:template match="@*|node()">

		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>

	</xsl:template>


	<xsl:template
		match="category[@name=$categoryName]/subcategory[@name=$subcategoryName]">

		<xsl:copy>

			<xsl:apply-templates select="@*|node()" />

			<xsl:element name="product">
				<xsl:attribute name="name">
					<xsl:value-of select="$name" />
				</xsl:attribute>
				<xsl:element name="model">
					<xsl:value-of select="$model" />
				</xsl:element>
				<xsl:element name="provider">
					<xsl:value-of select="$provider" />
				</xsl:element>
				<xsl:element name="date-of-issue">
					<xsl:value-of select="$dateOfIssue" />
				</xsl:element>
				<xsl:element name="color">
					<xsl:value-of select="$color" />
				</xsl:element>
				<xsl:choose>
					<xsl:when test="not($notInStock)">
						<xsl:element name="price">
							<xsl:value-of select="$price" />
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="notInStock" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>

		</xsl:copy>

	</xsl:template>


</xsl:stylesheet>