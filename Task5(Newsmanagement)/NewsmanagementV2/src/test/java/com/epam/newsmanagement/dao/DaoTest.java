package com.epam.newsmanagement.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.DatabaseUnitException;
import org.dbunit.IDatabaseTester;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.XmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.model.data.connection.IConnectionPool;
import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.IDaoFactory;
import com.epam.newsmanagement.model.data.entity.Entity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context-test.xml" })
public abstract class DaoTest extends DBTestCase {

	private final String DATA_SET_FOLDER;
	
	protected final String DATASET_PATH;
	protected final String DATASET_PLUS_ONE_PATH;
	protected final String DATASET_PLUS_TWO_PATH;
	protected final String DATASET_MINUS_ONE_PATH;
	protected final String DATASET_MINUS_TWO_PATH;
	protected final String DATASET_MODIFIED_PATH;
	
	@Autowired
	private IConnectionPool connectionPool;
	
	@Autowired
	private IDaoFactory daoFactory;
	
	@Autowired
	private IDatabaseTester tester;
	
	private IDataSet dataSet; 
	
	private final String SQL_SELECT_ALL;
	
	public DaoTest(String dataSetFolder) {
		DATA_SET_FOLDER = dataSetFolder;
		SQL_SELECT_ALL = "SELECT * FROM " + DATA_SET_FOLDER + " ORDER BY ID";
		this.DATASET_PATH = dataSetFolder + "/" + "dataset.xml";
		this.DATASET_PLUS_ONE_PATH = dataSetFolder + "/" + "datasetPlusOne.xml";
		this.DATASET_PLUS_TWO_PATH = dataSetFolder + "/" + "datasetPlusTwo.xml";
		this.DATASET_MINUS_ONE_PATH = dataSetFolder + "/" + "datasetMinusOne.xml";
		this.DATASET_MINUS_TWO_PATH = dataSetFolder + "/" + "datasetMinusTwo.xml";
		this.DATASET_MODIFIED_PATH = dataSetFolder + "/" + "datasetModified.xml";
	}

	public IDaoFactory getDaoFactory() {
		return daoFactory;
	}
	
	/**
	 * Inserts data to db from xml.
	 */
	@Before
	@Override
	public void setUp() {
		try {
			dataSet = createDataSet(DATASET_PATH);
			tester.setDataSet(dataSet);
			tester.onSetup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	@Override
	public void tearDown() throws Exception {
		DatabaseOperation.DELETE_ALL.execute(tester.getConnection(), dataSet);
	}

	/**
	 * Converts String to sql.Date
	 * 
	 * @param date
	 * @param datePattern datePattern pattern to convert String to date.
	 * @return
	 */
	public static java.sql.Date convertDate(String date, String datePattern)
			throws ParseException {
		java.sql.Date retDate = null;
		DateFormat format = new SimpleDateFormat(datePattern);
		java.util.Date uDate = format.parse(date);
		retDate = new java.sql.Date(uDate.getTime());
		return retDate;
	}
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return dataSet;
	}
	
	protected void compateDbAndXml(String dsPath) throws DaoException {
		try {
			// Load expected data from an XML dataset
			IDataSet expectedDataSet = createDataSet(dsPath);
			IDataSet checkDataSet = createDatabaseDataSet();

			// Getting tables expected and founded.
			ITable exTable = expectedDataSet.getTable(DATA_SET_FOLDER);
			ITable fTable = checkDataSet.getTable(DATA_SET_FOLDER);

			// Assert actual database table match expected tables
			assertEquals(exTable.getRowCount(), fTable.getRowCount());
			Assertion.assertEquals(expectedDataSet, checkDataSet);
		} catch (DaoException e) {
			throw e;
		} catch (DatabaseUnitException e) {
			throw new DaoException(e);
		}
	}
	
	protected IDataSet createDataSet(String path) throws DataSetException {
		return new XmlDataSet(Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(path));
	}
	
	protected IDataSet createDatabaseDataSet() throws DaoException {
		QueryDataSet ds = null;
		try {
			ds = new QueryDataSet(tester.getConnection());
			ds.addTable(DATA_SET_FOLDER, SQL_SELECT_ALL);
		} catch (Exception e) {
			throw new DaoException(e);
		}
		return ds;
	}
	
	protected List<List<Object>> dataSetToList(IDataSet dataSet) 
			throws DataSetException {
		List<List<Object>> retList = new ArrayList<>();
		ITable table = dataSet.getTable(DATA_SET_FOLDER);
		Column[] cs = table.getTableMetaData().getColumns();
		int rowCount = table.getRowCount();
		for (int i = 0; i < rowCount; i++) {
			List<Object> r = new ArrayList<>();
			String cn = cs[0].getColumnName();
			r.add(table.getValue(i, cn));
			cn = cs[1].getColumnName();
			r.add(table.getValue(i, cn));
			cn = cs[2].getColumnName();
			r.add(table.getValue(i, cn));
			cn = cs[3].getColumnName();
			r.add(table.getValue(i, cn));
			cn = cs[4].getColumnName();
			r.add(table.getValue(i, cn));
			cn = cs[5].getColumnName();
			r.add(table.getValue(i, cn));
			retList.add(r);
		}
		return retList;
	}
	
	/**
	 * Execute comparation between xml and db data.
	 * 
	 * @param tableName
	 * @throws DatabaseUnitException
	 * @throws DaoException
	 */
	protected void doComparation(String tableName, String xmlForCmpPath)
			throws DatabaseUnitException, DaoException {
		// Load expected data from an XML dataset
		IDataSet expectedDataSet = createDataSet(xmlForCmpPath);
		IDataSet checkDataSet = createDatabaseDataSet();

		// Assert actual database table match expected tables
		assertEquals(expectedDataSet.getTable(tableName).getRowCount(),
					 checkDataSet.getTable(tableName).getRowCount());
		String[] ignoringCols = { Entity.ID };
		Assertion.assertEqualsIgnoreCols(expectedDataSet.getTable(tableName),
				checkDataSet.getTable(tableName), ignoringCols);
	}
}
