package com.epam.newsmanagement.dao;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.ITagDao;
import com.epam.newsmanagement.model.data.entity.Tag;

public class TagDaoTest extends DaoTest {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(NewsDaoTest.class);
	
	private static final String TABLE_NAME = "tag";
	
	public TagDaoTest() {
		super(TABLE_NAME);
	}

	/**
	 * Add one method test for this dao.
	 */
	@Test
	public void add() {
		
		try {			
			Tag tag = new Tag();
			
			tag.setTagName("tag4");
			
			ITagDao tagDao = getDaoFactory().getTagDao();
			tagDao.add(tag);
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Add many method test for this dao.
	 */
	@Test
	public void addMany() {
		
		try {
			Tag tag1 = new Tag();
			Tag tag2 = new Tag();
			
			tag1.setTagName("tag4");
			tag1.setTagName("tag5");

			ITagDao tagDao = getDaoFactory().getTagDao();
			tagDao.addMany(Arrays.asList(tag1, tag2));
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Dalete by id method test for this dao.
	 */
	@Test
	public void deleteById() {
		
		try {
			
			ITagDao tagDao = getDaoFactory().getTagDao();
			List<Tag> tags = tagDao.findAll();
			/**
			 * Last added id.
			 */
			long id = tags.get(tags.size() - 1).getId();
			tagDao.delete(id);

			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Delete by many id method test for this dao.
	 */
	@Test
	public void deleteManyById() {
		
		try {
			
			ITagDao tagDao = getDaoFactory().getTagDao();
			List<Tag> tags = tagDao.findAll();
			/**
			 * Last added ids.
			 */
			long id1 = tags.get(tags.size() - 1).getId();
			long id2 = tags.get(tags.size() - 2).getId();
			tagDao.deleteMany(new Long[] { id1, id2 });
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find by id method test for this dao.
	 */
	@Test
	public void findById() {
		
		try {
			
			ITagDao tagDao = getDaoFactory().getTagDao();
			List<Tag> tags = tagDao.findAll();
			/**
			 * Last added ids.
			 */
			long id = tags.get(tags.size() - 1).getId();
			Tag tag = tagDao.find(id);

			/**
			 * Executing comparation for news.
			 */
			assertNotNull(tag);
			assertNotSame(tag.getId(), 0);
			assertNotNull(tag.getTagName());
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find all method test for this dao.
	 */
	@Test
	public void finAll() {
		
		try {
			
			ITagDao tagDao = getDaoFactory().getTagDao();
			List<Tag> tags = tagDao.findAll();
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PATH);
			// Load expected data from an XML dataset
			IDataSet checkDataSet = createDatabaseDataSet();
			// Assert actual database table match expected tables
			assertEquals(tags.size(),
						 checkDataSet.getTable(TABLE_NAME).getRowCount());
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Save method test for this dao.
	 */
	@Test
	public void save() {
		
		try {
			ITagDao tagDao = getDaoFactory().getTagDao();
			List<Tag> tags = tagDao.findAll();

			Tag tag = tags.get(tags.size() - 1);
			tag.setTagName("tag5");
			tagDao.save(tag);
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MODIFIED_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
}
