package com.epam.newsmanagement.dao;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.ICommentsDao;
import com.epam.newsmanagement.model.data.entity.Comments;

public class CommentsDaoTest extends DaoTest {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(NewsDaoTest.class);
	
	private static final String TABLE_NAME = "comments";
	
	public CommentsDaoTest() {
		super(TABLE_NAME);
	}

	/**
	 * Add one method test for this dao.
	 * @throws  
	 */
	@Test
	public void add() {
		
		try {			
			Comments comment = new Comments();
			
			comment.setCommentText("commentText1");
			comment.setCreationDate(DaoTest.convertDate("2015-01-01", "yyyy-MM-dd"));
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			commentDao.add(comment);
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException | ParseException e) {
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Add many method test for this dao.
	 */
	@Test
	public void addMany() {
		
		try {
			Comments comment1 = new Comments();
			Comments comment2 = new Comments();
			
			comment1.setCommentText("commentText1");
			comment1.setCreationDate(DaoTest.convertDate("2015-01-01", "yyyy-MM-dd"));

			comment2.setCommentText("commentText2");
			comment2.setCreationDate(DaoTest.convertDate("2015-02-02", "yyyy-MM-dd"));
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			commentDao.addMany(Arrays.asList(comment1, comment2));
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException | ParseException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Dalete by id method test for this dao.
	 */
	@Test
	public void deleteById() {
		
		try {
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			List<Comments> comments = commentDao.findAll();
			/**
			 * Last added id.
			 */
			long id = comments.get(comments.size() - 1).getId();
			commentDao.delete(id);

			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Delete by many id method test for this dao.
	 */
	@Test
	public void deleteManyById() {
		
		try {
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			List<Comments> comments = commentDao.findAll();
			/**
			 * Last added ids.
			 */
			long id1 = comments.get(comments.size() - 1).getId();
			long id2 = comments.get(comments.size() - 2).getId();
			commentDao.deleteMany(new Long[] { id1, id2 });
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find by id method test for this dao.
	 */
	@Test
	public void findById() {
		
		try {
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			List<Comments> comments = commentDao.findAll();
			/**
			 * Last added ids.
			 */
			long id = comments.get(comments.size() - 1).getId();
			Comments comment = commentDao.find(id);

			/**
			 * Executing comparation for news.
			 */
			assertNotNull(comment);
			assertNotSame(comment.getId(), 0);
			assertNotNull(comment.getCommentText());
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find all method test for this dao.
	 */
	@Test
	public void finAll() {
		
		try {
			
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			List<Comments> comments = commentDao.findAll();
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PATH);
			// Load expected data from an XML dataset
			IDataSet checkDataSet = createDatabaseDataSet();
			// Assert actual database table match expected tables
			assertEquals(comments.size(),
						 checkDataSet.getTable(TABLE_NAME).getRowCount());
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Save method test for this dao.
	 */
	@Test
	public void save() {
		
		try {
			ICommentsDao commentDao = getDaoFactory().getCommentDao();
			List<Comments> comments = commentDao.findAll();

			Comments comment = comments.get(comments.size() - 1);
			comment.setCommentText("commentText5");
			commentDao.save(comment);
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MODIFIED_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
}
