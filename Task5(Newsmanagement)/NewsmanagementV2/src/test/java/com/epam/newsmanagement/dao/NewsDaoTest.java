package com.epam.newsmanagement.dao;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.entity.News;

public class NewsDaoTest extends DaoTest {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(NewsDaoTest.class);
	
	private static final String TABLE_NAME = "news";
	
	public NewsDaoTest() {
		super(TABLE_NAME);
	}

	/**
	 * Add one method test for this dao.
	 */
	@Test
	public void add() {
		
		try {			
			News news = new News();
			
			news.setTitle("title4");
			news.setShortText("shortText4");
			news.setFullText("fullText4");
			news.setId(4);
			news.setModificationDate(DaoTest.convertDate("2015-04-04", "yyyy-MM-dd"));
			news.setCreationDate(DaoTest.convertDate("2015-04-04", "yyyy-MM-dd"));
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			newsDao.add(news);
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_ONE_PATH);
		} catch (ParseException | DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Add many method test for this dao.
	 */
	@Test
	public void addMany() {
		
		try {			
			News news1 = new News();
			news1.setId(4);
			news1.setTitle("title4");
			news1.setShortText("shortText4");
			news1.setFullText("fullText4");
			news1.setModificationDate(DaoTest.convertDate("2015-04-04", "yyyy-MM-dd"));
			news1.setCreationDate(DaoTest.convertDate("2015-04-04", "yyyy-MM-dd"));
			
			News news2 = new News();
			news2.setId(5);
			news2.setTitle("title5");
			news2.setShortText("shortText5");
			news2.setFullText("fullText5");
			news2.setModificationDate(DaoTest.convertDate("2015-05-05", "yyyy-MM-dd"));
			news2.setCreationDate(DaoTest.convertDate("2015-05-05", "yyyy-MM-dd"));
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			newsDao.add(news1);
			newsDao.add(news2);
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_TWO_PATH);
		} catch (ParseException | DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Dalete by id method test for this dao.
	 */
	@Test
	public void deleteById() {
		
		try {
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			List<News> newsList = newsDao.findAll();
			/**
			 * Last added id.
			 */
			long id = newsList.get(newsList.size() - 1).getId();
			newsDao.delete(id);

			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Delete by many id method test for this dao.
	 */
	@Test
	public void deleteManyById() {
		
		try {
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			List<News> newsList = newsDao.findAll();
			/**
			 * Last added ids.
			 */
			long id1 = newsList.get(newsList.size() - 1).getId();
			long id2 = newsList.get(newsList.size() - 2).getId();
			newsDao.deleteMany(new Long[] { id1, id2 });
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find by id method test for this dao.
	 */
	@Test
	public void findById() {
		
		try {
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			List<News> newsList = newsDao.findAll();
			/**
			 * Last added ids.
			 */
			long id = newsList.get(newsList.size() - 1).getId();
			News news = newsDao.find(id);

			/**
			 * Executing comparation for news.
			 */
			assertNotNull(news);
			assertNotSame(news.getId(), 0);
			assertNotNull(news.getFullText());
			assertNotNull(news.getShortText());
			assertNotNull(news.getTitle());
			assertNotNull(news.getCreationDate());
			assertNotNull(news.getId());
			assertNotNull(news.getModificationDate());
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find all method test for this dao.
	 */
	@Test
	public void finAll() {
		
		try {
			
			INewsDao newsDao = getDaoFactory().getNewsDao();
			List<News> newsList = newsDao.findAll();
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PATH);
			// Load expected data from an XML dataset
			IDataSet checkDataSet = createDatabaseDataSet();
			// Assert actual database table match expected tables
			assertEquals(newsList.size(),
						 checkDataSet.getTable(TABLE_NAME).getRowCount());
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Save method test for this dao.
	 */
	@Test
	public void save() {
		
		try {
			INewsDao newsDao = getDaoFactory().getNewsDao();
			List<News> newsList = newsDao.findAll();

			News news = newsList.get(newsList.size() - 1);
			news.setTitle("title5");
			news.setShortText("shortText5");
			news.setFullText("fullText5");
			news.setModificationDate(DaoTest.convertDate("2015-05-05", "yyyy-MM-dd"));
			news.setCreationDate(DaoTest.convertDate("2015-05-05", "yyyy-MM-dd"));
			newsDao.save(news);
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MODIFIED_PATH);
		} catch (DaoException | DatabaseUnitException | ParseException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
}
