package com.epam.newsmanagement.dao;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.IAuthorDao;
import com.epam.newsmanagement.model.data.entity.Author;

public class AuthorDaoTest extends DaoTest {

	/**
	 * Logger for logging any exceptions to log file.
	 */
	private static final Logger LOGGER = Logger.getLogger(NewsDaoTest.class);
	
	private static final String TABLE_NAME = "author";
	
	public AuthorDaoTest() {
		super(TABLE_NAME);
	}

	/**
	 * Add one method test for this dao.
	 */
	@Test
	public void add() {
		
		try {			
			Author author = new Author();
			
			author.setName("Author4");
			
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			authorDao.add(author);
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Add many method test for this dao.
	 */
	@Test
	public void addMany() {
		
		try {
			Author author1 = new Author();
			Author author2 = new Author();
			
			author1.setName("Author4");
			author1.setName("Author5");

			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			authorDao.addMany(Arrays.asList(author1, author2));
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PLUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Dalete by id method test for this dao.
	 */
	@Test
	public void deleteById() {
		
		try {
			
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			List<Author> authors = authorDao.findAll();
			/**
			 * Last added id.
			 */
			long id = authors.get(authors.size() - 1).getId();
			authorDao.delete(id);

			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_ONE_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Delete by many id method test for this dao.
	 */
	@Test
	public void deleteManyById() {
		
		try {
			
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			List<Author> authors = authorDao.findAll();
			/**
			 * Last added ids.
			 */
			long id1 = authors.get(authors.size() - 1).getId();
			long id2 = authors.get(authors.size() - 2).getId();
			authorDao.deleteMany(new Long[] { id1, id2 });
			
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MINUS_TWO_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find by id method test for this dao.
	 */
	@Test
	public void findById() {
		
		try {
			
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			List<Author> authors = authorDao.findAll();
			/**
			 * Last added ids.
			 */
			long id = authors.get(authors.size() - 1).getId();
			Author author = authorDao.find(id);

			/**
			 * Executing comparation for news.
			 */
			assertNotNull(author);
			assertNotSame(author.getId(), 0);
			assertNotNull(author.getName());
		} catch (DaoException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
	}
	
	/**
	 * Find all method test for this dao.
	 */
	@Test
	public void finAll() {
		
		try {
			
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			List<Author> authors = authorDao.findAll();
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_PATH);
			// Load expected data from an XML dataset
			IDataSet checkDataSet = createDatabaseDataSet();
			// Assert actual database table match expected tables
			assertEquals(authors.size(),
						 checkDataSet.getTable(TABLE_NAME).getRowCount());
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
	
	/**
	 * Save method test for this dao.
	 */
	@Test
	public void save() {
		
		try {
			IAuthorDao authorDao = getDaoFactory().getAuthorDao();
			List<Author> authors = authorDao.findAll();

			Author author = authors.get(authors.size() - 1);
			author.setName("Author5");
			authorDao.save(author);
			/**
			 * Executing comparation between xml and db data.
			 */
			doComparation(TABLE_NAME, DATASET_MODIFIED_PATH);
		} catch (DaoException | DatabaseUnitException e) {
			e.printStackTrace();
			LOGGER.error(e);
		}
		
	}
}
