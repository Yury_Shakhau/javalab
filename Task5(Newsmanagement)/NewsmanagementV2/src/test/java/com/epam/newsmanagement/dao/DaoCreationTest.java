package com.epam.newsmanagement.dao;

import java.util.Random;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.model.data.connection.IConnectionPool;
import com.epam.newsmanagement.model.data.dao.IAuthorDao;
import com.epam.newsmanagement.model.data.dao.ICommentsDao;
import com.epam.newsmanagement.model.data.dao.IDaoFactory;
import com.epam.newsmanagement.model.data.dao.INewsAuthorDao;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.dao.INewsTagDao;
import com.epam.newsmanagement.model.data.dao.ITagDao;
import com.epam.newsmanagement.model.data.entity.Author;
import com.epam.newsmanagement.model.data.entity.Comments;
import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.NewsAuthor;
import com.epam.newsmanagement.model.data.entity.NewsTag;
import com.epam.newsmanagement.model.data.entity.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application-context.xml" })
public class DaoCreationTest extends DBTestCase {


	private static final int ENTIY_COUNT = 20;
	
	@Autowired
	private IConnectionPool connectionPool;
	
	@Autowired
	private IDaoFactory daoFactory;
	
	@Autowired
	private IDatabaseTester tester;
	
	private IDataSet dataSet;
	
	private static String generateString() {
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < 15; i++) {
			sb.append(r.nextInt(10));
		}
		return sb.toString();
	}
	
	private static long generateId() {
		Random r = new Random();
		return 1 + r.nextInt(ENTIY_COUNT - 1);
	}
	
	/**
	 * Inserts data to db from xml.
	 */
	@Before
	@Override
	public void setUp() throws Exception {
		INewsDao newsDao = daoFactory.getNewsDao();
		IAuthorDao authorDao = daoFactory.getAuthorDao();
		ITagDao tagDao = daoFactory.getTagDao();
		ICommentsDao commentDao = daoFactory.getCommentDao();
		INewsAuthorDao newsAuthorDao = daoFactory.getNewsAuthorDao();
		INewsTagDao newsTagDao = daoFactory.getNewsTagDao();
		java.sql.Date dateNow = new java.sql.Date(new java.util.Date().getTime());
		for (int i = 0; i < ENTIY_COUNT; i++) {
			News news = new News();
			news.setCreationDate(dateNow);
			news.setFullText(generateString());
			news.setModificationDate(dateNow);
			news.setShortText(generateString());
			news.setTitle(generateString());
			newsDao.add(news);
		}
		for (int i = 0; i < ENTIY_COUNT; i++) {
			Author author = new Author();
			author.setName(generateString());
			authorDao.add(author);
		}
		for (int i = 0; i < ENTIY_COUNT; i++) {
			Comments comment = new Comments();
			comment.setCommentText(generateString());
			comment.setCreationDate(dateNow);
			comment.setCreationDate(dateNow);
			comment.setNewsId(generateId());
			commentDao.add(comment);
		}
		for (int i = 0; i < ENTIY_COUNT; i++) {
			Tag tag = new Tag();
			tag.setTagName(generateString());
			tagDao.add(tag);
		}
		for (int i = 0; i < ENTIY_COUNT; i++) {
			NewsAuthor newsAuthor = new NewsAuthor();
			newsAuthor.setAuthorId(generateId());
			newsAuthor.setNewsId(generateId());
			newsAuthorDao.add(newsAuthor);
		}
		for (int i = 0; i < ENTIY_COUNT; i++) {
			NewsTag newsTag = new NewsTag();
			newsTag.setTagId(generateId());
			newsTag.setNewsId(generateId());
			newsTagDao.add(newsTag);
		}
	}

	@Test
	public void addNews() {
		assertEquals(true, true);
	}

	
	@Override
	protected IDataSet getDataSet() throws Exception {
		return dataSet;
	}
}
