package com.epam.newsmanagement.model.data.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import util.model.sql.query.Executor;
import util.model.sql.query.Query;

import com.epam.newsmanagement.model.data.DataModelException;
import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.DaoFactory;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.dao.INewsTagDao;
import com.epam.newsmanagement.model.data.dao.ITagDao;
import com.epam.newsmanagement.model.data.entity.NewsTag;
import com.epam.newsmanagement.model.data.entity.Tag;
import com.epam.newsmanagement.model.data.entity.building.EntityWorker;

/**
 * Extends for for this dao.
 * Implements incident dao mathods.
 * 
 * @author Yury_Shakhau
 *
 * @param <Tag> entity type.
 */
@Repository
public class TagDao extends Dao<Tag> implements ITagDao {

	/**
	 * Search by name sql.
	 */
	public final String FIND_BY_NAME = Query
			.select(Tag.FIELDS)
			.from(Tag.class)
			.where(Tag.TAG_NAME)
			.equal(Query.UNKNOWN)
			.build();
	
	public TagDao() {
		super(Tag.class);
	}

	/**
	 * Searches tag by its name.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return this entity with set id.
	 * @throws DaoException
	 */
	@Override
	public Tag findByName(String name) throws DaoException {
		Tag tag = null;
		Connection c = getConnectionPool().takeConnection();
		ResultSet rs = null;
		try {
			PreparedStatement st = c.prepareStatement(SQL_FIND);
			rs = Executor.executeQuery(st, name);
			if (rs.next()) {
				tag = (Tag) EntityWorker.createEntity(Tag.class, getFields(), rs);
			}
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(rs, c);
		}
		return tag;
	}
	
	/**
	 * Adds tag for news.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return true if that added, false if not.
	 * @throws DaoException
	 */
	@Override
	public Tag addTag(long newsId, Tag tag) throws DaoException {
		INewsTagDao newsTagDao = DaoFactory.getInstance().getNewsTagDao();
		INewsDao newsDao = DaoFactory.getInstance().getNewsDao();
		if (newsDao.find(newsId) != null) {
			Tag tagFromDB = findByName(tag.getTagName());
			if (tagFromDB == null) {
				tag = add(tag);
			} else {
				tag = tagFromDB;
			}
			if (tag.getId() > 0) {
				NewsTag newsTag = new NewsTag();
				newsTag.setNewsId(newsId);
				newsTag.setTagId(tag.getId());
				newsTagDao.add(newsTag);
			} else {
				throw new DaoException("Tag did not added.");
			}
		} else {
			throw new DaoException("News with id " + newsId +" does not exists.");
		}
		return tag;
	}

	/**
	 * Adds tags for news.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return true if that added, false if not.
	 * @throws DaoException
	 * @throws  
	 */
	@Override
	public Collection<Tag> addTag(long newsId, Collection<Tag> tags) throws DaoException  {
		for (Tag tag : tags) {
			tag = addTag(newsId, tag);
		}
		return tags;
	}
}
