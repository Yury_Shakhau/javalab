package com.epam.newsmanagement.model.data.dao.implementation;

import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.model.data.dao.ICommentsDao;
import com.epam.newsmanagement.model.data.entity.Comments;

/**
 * Extends for for this dao.
 * Implements incident dao mathods.
 * 
 * @author Yury_Shakhau
 *
 * @param <Comments> entity type.
 */
@Repository
public class CommentsDao extends Dao<Comments> implements ICommentsDao {

	public CommentsDao() {
		super(Comments.class);
	}
}
