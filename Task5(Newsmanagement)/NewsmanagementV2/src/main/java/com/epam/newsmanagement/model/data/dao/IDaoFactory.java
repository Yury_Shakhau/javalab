package com.epam.newsmanagement.model.data.dao;

public interface IDaoFactory {

	IAuthorDao getAuthorDao();
	ICommentsDao getCommentDao();
	INewsAuthorDao getNewsAuthorDao();
	INewsDao getNewsDao();
	INewsTagDao getNewsTagDao();
	ITagDao getTagDao();
}
