package com.epam.newsmanagement.model.data.dao.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import util.model.sql.query.Query;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.IDaoFactory;
import com.epam.newsmanagement.model.data.dao.INewsAuthorDao;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.entity.Author;
import com.epam.newsmanagement.model.data.entity.Comments;
import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.NewsAuthor;
import com.epam.newsmanagement.model.data.entity.NewsTag;
import com.epam.newsmanagement.model.data.entity.Tag;

/**
 * Extends for for this dao.
 * Implements incident dao mathods.
 * 
 * @author Yury_Shakhau
 *
 * @param <News> entity type.
 */
@Repository
public class NewsDao extends Dao<News> implements INewsDao {

	/**
	 * Selects all news for author id.
	 */
	private static final String SQL_FIND_BY_AUTHOR_ID = 
		Query.select(News.FIELDS).from(News.TABLE_NAME)
		     .join(NewsAuthor.TABLE_NAME).on(News.ID).equal(NewsAuthor.NEWS_ID)
		     .join(Author.TABLE_NAME).on(Author.ID).equal(NewsAuthor.AUTHOR_ID)
		     .where(Author.ID).equal(Query.UNKNOWN)
		.build();
	
	/**
	 * Selects all news for tag name.
	 */
	private static final String SQL_FIND_BY_TAG_NAME = 
			Query.select(News.FIELDS).from(News.TABLE_NAME)
			     .join(NewsTag.TABLE_NAME).on(News.ID).equal(NewsTag.NEWS_ID)
			     .join(Tag.TABLE_NAME).on(NewsTag.TAG_ID).equal(Tag.ID)
			     .where(Tag.TAG_NAME).equal(Query.UNKNOWN)
			.build();
	
	/**
	 * Selects all news sorted by most commented.
	 */
	private static final String SQL_FIND_SORTED_BY_COMMENTS = 
			Query.select(News.FIELDS).from(News.TABLE_NAME)
			     .join(Comments.TABLE_NAME).on(News.ID).equal(Comments.NEWS_ID)
			     .groupBy(Comments.NEWS_ID)
			     .orderBy(Query.count(Comments.ID)).desc()
			.build();
	
	@Autowired
	private IDaoFactory daoFactory;
	
	public NewsDao() {
		super(News.class);
	}

	@Override
	public void deleteNewsFull(long id) throws DaoException {
		INewsAuthorDao newsAuthorDao = daoFactory.getNewsAuthorDao();
	//	INewsTagDao newsTagDao = daoFactory.getNewsTagDao();
	//	ICommentsDao commentDao = daoFactory.getCommentDao();
		newsAuthorDao.deleteByNewsId(id);
	}

	/**
	 * Searches news by author id.
	 */
	@Override
	public List<News> findNewsByAuthorId(long authorId) throws DaoException {
		return joinLeftGet(getConnectionPool(), getFields(), SQL_FIND_BY_AUTHOR_ID, authorId);
	}

	/**
	 * Searches new sby tag name.
	 */
	@Override
	public List<News> findNewsByTagName(Tag tag) throws DaoException {
		return joinLeftGet(getConnectionPool(), getFields(), SQL_FIND_BY_TAG_NAME, tag.getTagName());
	}

	@Override
	public List<News> findSortedByMostCommented() throws DaoException {
		return find(getConnectionPool(), News.class, getFields(), SQL_FIND_SORTED_BY_COMMENTS);
	}
}
