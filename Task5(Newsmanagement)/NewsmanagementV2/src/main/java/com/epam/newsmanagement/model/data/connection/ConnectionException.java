package com.epam.newsmanagement.model.data.connection;

import java.sql.SQLException;

public class ConnectionException extends SQLException {
	
	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 46929093735921L;

	public ConnectionException() {
		super();
	}
	
	public ConnectionException(String message) {
		super(message);
	}
	
	public ConnectionException(Throwable t) {
		super(t);
	}
	
	public ConnectionException(String message, Throwable t) {
		super(message, t);
	}
}
