package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Tag entity.
 * 
 * @author Yury_Shakhau
 *
 */
public class Tag implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -2289984413802173L;
	
	/**
	 * This table name in db.
	 */
	public static final String TABLE_NAME = News.class.getSimpleName();
	
	/**
	 * This primary key.
	 */
	public static final String ID = TABLE_NAME + ".id";
	
	/**
	 * This name column name.
	 */
	public static final String TAG_NAME = TABLE_NAME + ".tagName";
	
	public static final List<String> FIELDS = Arrays.asList(
			ID,
			TAG_NAME);
	/**
	 * Primary key/
	 */
	private long id;
	
	/**
	 * Name of this tag.
	 */
	private String tagName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
}
