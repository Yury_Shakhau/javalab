package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;

/**
 * Cross table between news and tag.
 * 
 * @author Yury_Shakhau
 *
 */
public class NewsTag implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -2743802173L;
	
	/** 
	 * This table name. 
	 */
	public static final String TABLE_NAME = News.class.getSimpleName();
	
	/**
	 * newsId name for using in jdbc.
	 */
	public static final String NEWS_ID = TABLE_NAME + ".newsId";
	
	/**
	 * tagId for using in jdbc.
	 */
	public static final String TAG_ID = TABLE_NAME + ".tagId";
	
	/**
	 * Primary key/
	 */
	private long id;
	
	/**
	 * Foreign key to news.
	 */
	private long newsId;
	
	/**
	 * Foreign key to tag.
	 */
	private long tagId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
}
