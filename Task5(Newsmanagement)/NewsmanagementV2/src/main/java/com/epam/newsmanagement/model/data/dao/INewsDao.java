package com.epam.newsmanagement.model.data.dao;

import java.util.List;

import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.Tag;

/**
 * 
 * 
 * @author user
 *
 */
public interface INewsDao extends IDao<News> {

	/**
	 * Deletes news with its comments and cross records in
	 * newsTag and newsAuthor tables.
	 * 
	 * @param id
	 * @throws DaoException
	 */
	void deleteNewsFull(long id) throws DaoException;
	
	/**
	 * Searches news by authorId.
	 * 
	 * @param authorId
	 * @return
	 * @throws DaoException
	 */
	List<News> findNewsByAuthorId(long authorId) throws DaoException;
	
	/**
	 * Searches news by authorId.
	 * 
	 * @param authorId
	 * @return
	 * @throws DaoException
	 */
	List<News> findNewsByTagName(Tag tag) throws DaoException;
	
	/**
	 * Method for selecting all entities sorted .
	 * 
	 * @return list of entities form db.
	 */
	List<News> findSortedByMostCommented() throws DaoException;
}
