package com.epam.newsmanagement.model.data.entity;

/**
 * For creating java syntax exception for not type such this.
 * All entities must implements this interface.
 * Common dao has this type as a parameter.
 * 
 * @author Yury_Shakhau
 *
 */
public interface IEntity {

}
