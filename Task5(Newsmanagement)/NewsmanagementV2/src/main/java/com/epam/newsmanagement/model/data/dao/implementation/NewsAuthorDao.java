package com.epam.newsmanagement.model.data.dao.implementation;

import org.springframework.stereotype.Repository;

import util.model.sql.query.Query;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.INewsAuthorDao;
import com.epam.newsmanagement.model.data.entity.NewsAuthor;

/**
 * Extends for for this dao.
 * Implements incident dao mathods.
 * 
 * @author Yury_Shakhau
 *
 * @param <NewsAuthor> entity type.
 */
@Repository
public class NewsAuthorDao extends Dao<NewsAuthor> implements INewsAuthorDao {

	public final String SQL_DELETE_BY_NEWS_ID = 
			Query.deleteFrom(getEntityName())
				.where(NewsAuthor.NEWS_ID).equal(Query.UNKNOWN)
		   .build();
	
	public final String SQL_DELETE_BY_AUTHOR_ID = 
		Query.deleteFrom(getEntityName())
			.where(NewsAuthor.AUTHOR_ID).equal(Query.UNKNOWN)
	   .build();
	
	public NewsAuthorDao() {
		super(NewsAuthor.class);
	}

	/**
	 * Deletes entity form db by newsId.
	 * 
	 * @param newsId id of this foreign key to news entity.
	 * @throws DaoException
	 */
	@Override
	public void deleteByNewsId(long newsId) throws DaoException {
		Dao.delete(getConnectionPool(), SQL_DELETE_BY_NEWS_ID, newsId);
	}

	/**
	 * Deletes entity form db by authorId.
	 * 
	 * @param authorId id of this foreign key to author entity.
	 * @throws DaoException
	 */
	@Override
	public void deleteByAuthorId(long authorId) throws DaoException {
		Dao.delete(getConnectionPool(), SQL_DELETE_BY_AUTHOR_ID, authorId);
	}
}
