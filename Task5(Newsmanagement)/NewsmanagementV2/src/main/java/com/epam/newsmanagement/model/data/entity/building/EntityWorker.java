package com.epam.newsmanagement.model.data.entity.building;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.model.sql.query.Query;

import com.epam.newsmanagement.model.data.DataModelException;
import com.epam.newsmanagement.model.data.entity.Entity;

/**
 * Class for creating end building entities by db data
 * automaticaly.
 * 
 * @author Yury_Shakhau
 */
public final class EntityWorker {
	/**
	 * Type invoker container.
	 */
	private static final TypedResultGeter RESULT_GETTER = new TypedResultGeter();
	
	private EntityWorker() {
	}
	
	/**
	 * Class for type invoker container.
	 * Execute resultSet method for fit type in entity.
	 * 
	 * @author Yury_Shakhau
	 *
	 */
	private static final class TypedResultGeter {
		
		/** Simple types container */
		private static final Map<Object, IInvoker> CONTAINER = new HashMap<>();
		
		/**
		 * Simple types mapping.
		 */
		static {
			IInvoker shortInvoker = new ShortInvoker();
			IInvoker intInvoker = new IntInvoker();
			IInvoker stringInvoker = new StringInvoker();
			IInvoker longInvoker = new LongInvoker();
			IInvoker dateInvoker = new DateInvoker();
			IInvoker booleanInvoker = new BooleanInvoker();
			IInvoker doubleInvoker = new DoubleInvoker();
			IInvoker floatInvoker = new FloatInvoker();
			IInvoker byteInvoker = new ByteInvoker();
			CONTAINER.put(Short.class, shortInvoker);
			CONTAINER.put(Integer.class, intInvoker);
			CONTAINER.put(String.class, stringInvoker);
			CONTAINER.put(Long.class, longInvoker);
			CONTAINER.put(Date.class, dateInvoker);
			CONTAINER.put(Boolean.class, booleanInvoker);
			CONTAINER.put(Double.class, doubleInvoker);
			CONTAINER.put(Float.class, floatInvoker);
			CONTAINER.put(Byte.class, byteInvoker);
			CONTAINER.put(short.class, shortInvoker);
			CONTAINER.put(int.class, intInvoker);
			CONTAINER.put(long.class, longInvoker);
			CONTAINER.put(boolean.class, booleanInvoker);
			CONTAINER.put(double.class, doubleInvoker);
			CONTAINER.put(float.class, floatInvoker);
			CONTAINER.put(byte.class, byteInvoker);
		}
		
		/**
		 * Invoker interface.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static interface IInvoker {
			Object invoke(ResultSet set, int index) throws SQLException;
		}
		
		/**
		 * Invoker for short from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class ShortInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getShort(index);
			}

		}
		
		/**
		 * Invoker for int from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class IntInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getInt(index);
			}

		}
		
		/**
		 * Invoker for String from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class StringInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getString(index);
			}

		}
		
		/**
		 * Invoker for Long from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class LongInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getLong(index);
			}

		}
		
		/**
		 * Invoker for Date from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class DateInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getDate(index);
			}

		}
		
		/**
		 * Invoker for Boolean from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class BooleanInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getBoolean(index);
			}

		}
		
		/**
		 * Invoker for Double from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class DoubleInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getDouble(index);
			}

		}
		
		/**
		 * Invoker for Float from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class FloatInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getFloat(index);
			}

		}
		
		/**
		 * Invoker for Byte from resultSet.
		 * 
		 * @author Yury_Shakhau
		 *
		 */
		private static final class ByteInvoker implements IInvoker {

			@Override
			public Object invoke(ResultSet set, int index) throws SQLException {
				return set.getByte(index);
			}

		}
		
		public Object takeFromResultSet(ResultSet set, int index, Class<?> type) 
				throws SQLException, DataModelException {
			Object retObj = null;
			IInvoker invoker = CONTAINER.get(type);
			if (invoker != null) {
				try {
					retObj = invoker.invoke(set, index);
				} catch (SQLException e) {
					throw new DataModelException(e);
				}
			} else {
				throw new DataModelException("Type " + type + " does not supported");
			}
			return retObj;
		}
	}
	
	/**
	 * Creates entity from class.
	 * 
	 * @param entityClass
	 * @return
	 * @throws DataModelException
	 */
	public static Object createEntity(Class<?> entityClass)
			throws DataModelException {
		Object entity = null;
		try {
			entity = entityClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new DataModelException("Object can not be careted for class "
					+ entityClass, e);
		}
		return entity;
	}

	/**
	 * Builds entity by resultSet parameters form db.
	 * 
	 * @param entity
	 * @param entityClass
	 * @param result
	 * @return
	 * @throws DataModelException
	 */
	public static Object buildEntity(Object entity, Field[] fields,
			ResultSet result) throws DataModelException {
		try {
			int i = 1;
			for (Field f : fields) {
				Class<?> cl = f.getType();
				Object fieldValue = RESULT_GETTER.takeFromResultSet(result, i, cl);
				f.set(entity, fieldValue);
				i++;
			}

		} catch (IllegalArgumentException | IllegalAccessException
				| SQLException e) {
			throw new DataModelException(e);
		}
		return entity;
	}

	public static Object createEntity(Class<?> entityClass, Field[] fields, ResultSet result)
			throws DataModelException {
		Object entity = createEntity(entityClass);
		return buildEntity(entity, fields, result);
	}
	
	/**
	 * Sets entity id.
	 * 
	 * @param entity
	 * @param id
	 * @throws DataModelException
	 */
	public static void putEntityId(Object entity, long id)
			throws DataModelException {
		Class<?> entityClass = entity.getClass();
		try {
			Field f = entityClass.getDeclaredField(Entity.ID);
			f.setAccessible(true);
			f.set(entity, id);
		} catch (NoSuchFieldException | SecurityException
				| IllegalArgumentException | IllegalAccessException e) {
			throw new DataModelException(e);
		}

	}
	
	public static String[] fieldNames(Field[] fields) {
		String[] fNames = new String[fields.length];
		for (int i = 0; i < fNames.length; i++) {
			fNames[i] = fields[i].getName();
		}
		return fNames;
	}
	
	public static String[] fieldNames(Class<?> entityClass) {
		return fieldNames(makeFieldsAccessable(entityClass));
	}
	
	public static String[] fieldNamesWithoutId(String[] fieldNames) {
		List<String> buff = new ArrayList<>(fieldNames.length);
		for (String fn : fieldNames) {
			if (!fn.equals(Entity.ID)) {
				buff.add(fn);
			}
		}
		String[] fieldNamesRet = new String[buff.size()];
		for (int i = 0; i < buff.size(); i++) {
			fieldNamesRet[i] = buff.get(i);
		}
		return fieldNamesRet;
	}
	
	/**
	 * Makes entity fields accessible from reflection.
	 * And returns them.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Field[] makeFieldsAccessable(Class<?> entityClass) {
		Field[] fields = entityClass.getDeclaredFields();
		List<Field> fList = new ArrayList<>();
		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			if (!Modifier.isStatic(f.getModifiers())) {
				f.setAccessible(true);
				fList.add(f);
			}
		}
		Field[] retArray = new Field[fList.size()];
		int i = 0;
		for (Field f : fList) {
			retArray[i] = f;
			i++;
		}
		return retArray;
	}
	
	/**
	 * And returns entity fields without id field.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Field[] fieldsWithoutId(Field[] fieldNames) {
		List<Field> buff = new ArrayList<>(fieldNames.length);
		for (Field fn : fieldNames) {
			if (!fn.getName().equals(Entity.ID)) {
				buff.add(fn);
			}
		}
		Field[] fieldsRet = new Field[buff.size()];
		for (int i = 0; i < buff.size(); i++) {
			fieldsRet[i] = buff.get(i);
		}
		return fieldsRet;
	}
	
	/**
	 * And returns entity fields with id field in the end.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Field[] fieldsIdInTheEnd(Field[] fieldNames) {
		Field[] fieldsRet = new Field[fieldNames.length];
		Field idField = null;
		int i = 0;
		for (Field fn : fieldNames) {
			if (!fn.getName().equals(Entity.ID)) {
				fieldsRet[i] = fn;
				i++;
			} else {
				idField = fn;
			}
		}
		if (idField != null) {
			fieldsRet[i] = idField;
		}
		return fieldsRet;
	}
	
	/**
	 * Gets entity values wiith reflection.
	 * 
	 * @param entity
	 * @param fields
	 * @return
	 * @throws DataModelException
	 */
	public static Object[] entityValues(Object entity, Field[] fields) 
			throws DataModelException {
		Object[] retObjs = new Object[fields.length];
		try {
			for (int i = 0; i < fields.length; i++) {
				retObjs[i] = fields[i].get(entity);
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new DataModelException(e);
		}
		return retObjs;
	}
	
	/**
	 * Creates question array for jdbc.
	 * 
	 * @param objects
	 * @return
	 */
	public static String[] createUnknownsArray(Object[] objects) {
		String[] sb = new String[objects.length];
		for (int i = 0; i < objects.length; i++) {
			sb[i] = "?";
		}
		return sb;
	}
	
	/**
	 * Creates questions string for jdbc.
	 * 
	 * @param objects
	 * @return
	 */
	public static String createUnknowns(Object[] objects) {
		StringBuilder sb = new StringBuilder();
		if (objects.length > 0) {
			sb.append(Query.UNKNOWN);
			for (int i = 1; i < objects.length; i++) {
				sb.append(',').append(Query.UNKNOWN);
			}
		}
		return sb.toString();
	}
}
