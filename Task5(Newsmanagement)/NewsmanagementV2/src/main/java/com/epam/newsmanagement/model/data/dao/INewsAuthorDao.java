package com.epam.newsmanagement.model.data.dao;

import com.epam.newsmanagement.model.data.entity.NewsAuthor;

/**
 * News dao extension.
 * 
 * @author Yury_Shakhau
 *
 */
public interface INewsAuthorDao extends IDao<NewsAuthor> {

	/**
	 * Deletes entity form db by newsId.
	 * 
	 * @param newsId id of this foreign key to news entity.
	 * @throws DaoException
	 */
	void deleteByNewsId(long newsId) throws DaoException;
	
	/**
	 * Deletes entity form db by authorId.
	 * 
	 * @param authorId id of this foreign key to author entity.
	 * @throws DaoException
	 */
	void deleteByAuthorId(long authorId) throws DaoException;
}
