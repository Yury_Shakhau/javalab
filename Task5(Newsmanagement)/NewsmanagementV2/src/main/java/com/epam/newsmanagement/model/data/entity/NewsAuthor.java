package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;

/**
 * Cross table between news and author.
 * 
 * @author Yury_Shakhau
 *
 */
public class NewsAuthor implements Serializable, IEntity {
	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -2759289984413802173L;
	
	/** 
	 * This table name. 
	 */
	public static final String TABLE_NAME = NewsAuthor.class.getSimpleName();
	
	/**
	 * newsId name for using in jdbc.
	 */
	public static final String NEWS_ID = TABLE_NAME + ".newsId";
	
	/**
	 * authorId name for using in jdbc.
	 */
	public static final String AUTHOR_ID = TABLE_NAME + ".authorId";
	
	/**
	 * Primary key.
	 */
	private long id;
	
	/**
	 * Foreign key to news.
	 */
	private long newsId;
	
	/**
	 * Foreign to author.
	 */
	private long authorId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getNewsId() {
		return newsId;
	}
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
}
