package com.epam.newsmanagement.model.data.dao;

import com.epam.newsmanagement.model.data.entity.Comments;

/**
 * Comments dao extention.
 * 
 * @author Yury_Shakhau
 *
 */
public interface ICommentsDao extends IDao<Comments> {

	
}
