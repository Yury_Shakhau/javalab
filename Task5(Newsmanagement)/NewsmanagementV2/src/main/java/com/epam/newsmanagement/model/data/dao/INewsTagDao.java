package com.epam.newsmanagement.model.data.dao;

import com.epam.newsmanagement.model.data.entity.NewsTag;

public interface INewsTagDao extends IDao<NewsTag> {

}
