package com.epam.newsmanagement.model.data.dao;

import com.epam.newsmanagement.model.data.entity.Author;

/**
 * Extends for this dao.
 * Implements incident dao methods.
 * 
 * @author Yury_Shakhau
 *
 * @param <Author> entity type.
 */
public interface IAuthorDao extends IDao<Author> {

	/**
	 * Adds author for news.
	 * 
	 * @param newsId news id.
	 * @param author author entity.
	 * @return author entity with added id from db.
	 * @throws DaoException
	 */
	Author addAuthor(long newsId, Author author) throws DaoException;
}
