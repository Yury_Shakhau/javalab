package com.epam.newsmanagement.model.data.dao;

import java.util.List;

/**
* Abstraction of all entities dao.
* 
* @author Yury Shakhau
*/
public interface IDao<T> {
	
	/**
	 * Method for selects entity by its id.
	 * 
	 * @param id user id.
	 */
	T find(long id) throws DaoException;

	/**
	 * Adds entity to the database.
	 * 
	 * @param entity entity for adding to a database.
	 */
	T add(T entity) throws DaoException;

	/**
	 * Adds entity to the database.
	 * 
	 * @param entity entity for adding to a database.
	 */
	void addMany(List<T> entities) throws DaoException;
	/**
	 * Saves entity.
	 * 
	 * @param entity entity for saving in a database.
	 */
	void save(T entity) throws DaoException;
	
	/**
	 * Method for selecting all entities.
	 */
	List<T> findAll() throws DaoException;
	
	/**
	 * Deletes news rows in database by id.
	 * 
	 * @param id for delete rows.
	 * 
	 * @throws DaoException throws on sql exeption.
	 */
	public void delete(Long id) throws DaoException;
	
	/**
	 * Deletes news rows in database by ids array.
	 * 
	 * @param ids id array for delete rows.
	 * 
	 * @throws DaoException throws on sql exeption.
	 */
	void deleteMany(Long[] ids) throws DaoException;
}
