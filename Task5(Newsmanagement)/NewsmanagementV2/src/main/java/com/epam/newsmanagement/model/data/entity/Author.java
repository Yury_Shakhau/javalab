package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;

/**
 * Query builder methods for creating sql dynamically.
 * 
 * @author Yury_Shakhau
 *
 */
public class Author implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -27592343802173L;
	
	/**
	 * This table name in db.
	 */
	public static final String TABLE_NAME = Author.class.getSimpleName();
	
	/**
	 * This primary key.
	 */
	public static final String ID = TABLE_NAME + ".id";
	
	/**
	 * This name column name.
	 */
	public static final String NAME = TABLE_NAME + ".name";
	
	/**
	 * Primary key.
	 */
	private long id;
	
	/**
	 * Author name.
	 */
	private String name;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
