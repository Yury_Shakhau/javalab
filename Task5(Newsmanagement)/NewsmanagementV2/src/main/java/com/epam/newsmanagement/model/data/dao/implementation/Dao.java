package com.epam.newsmanagement.model.data.dao.implementation;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import util.model.sql.query.Executor;
import util.model.sql.query.Query;

import com.epam.newsmanagement.model.data.DataModelException;
import com.epam.newsmanagement.model.data.connection.IConnectionPool;
import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.IDao;
import com.epam.newsmanagement.model.data.entity.Entity;
import com.epam.newsmanagement.model.data.entity.IEntity;
import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.building.EntityWorker;

/**
 * Universal Dao class.
 * Implements crud for any tables.
 * Table names and entity names must be equals and
 * column names and entity fields must equals also.
 * 
 * @author Yury_Shakhau
 *
 * @param <T> entity type.
 */
public abstract class Dao<T extends IEntity> implements IDao<T> {

	private static final Logger LOGGER = Logger.getLogger(Dao.class);

	private static final String[] PRIMARY_KEY_OUT = new String[] { Entity.ID };
	
	/**
	 * Entity class for getting all information about this entity.
	 */
	private final Class<?> entityClass;

	private final String entityName;
	
	/**
	 * Entity info about all fields.
	 */
	private final Field[] fields;
	private final Field[] fieldsIdInTheEnd;
	private final Field[] fieldsWithoutId;
	private final String[] fieldNames;
	private final String[] fieldNamesWithoutId;
	private final String unknownsWithoutId;
//	private final String[] unknownsArray;
	private final String[] unknownsWithoutIdArray;
	
	/**
	 * Query for search any entity in db.
	 */
	public final String SQL_FIND;
	
	/**
	 * Query for add any entity to db.
	 */
	public final String SQL_ADD;
	
	/**
	 * Query for save any entity in db.
	 */
	public final String SQL_SAVE;
	
	/**
	 * Query for search any entities in db.
	 */
	public final String SQL_FIND_ALL;
	
	/**
	 * Query for search delete entity from db.
	 */
	public final String SQL_DELETE;

	/**
	 * For data access to db.
	 */
	@Autowired
	private IConnectionPool connectionPool;
	
	public Class<?> getEntityClass() {
		return entityClass;
	}

	public Field[] getFields() {
		return fields;
	}
	
	public String getEntityName() {
		return entityName;
	}
	
	public IConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public void setConnectionPool(IConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}
	
	public Dao(Class<?> entityClass) {
		this.entityClass = entityClass;
		this.entityName = entityClass.getSimpleName();
		this.fields = EntityWorker.makeFieldsAccessable(this.entityClass);
		this.fieldsIdInTheEnd = EntityWorker.fieldsIdInTheEnd(fields);
		this.fieldsWithoutId = EntityWorker.fieldsWithoutId(fields);
		this.fieldNames = EntityWorker.fieldNames(this.fields);
		this.fieldNamesWithoutId = EntityWorker.fieldNamesWithoutId(fieldNames);
		this.unknownsWithoutId = EntityWorker.createUnknowns(fieldsWithoutId);
//		this.unknownsArray = EntityWorker.createUnknownsArray(fields);
		this.unknownsWithoutIdArray = EntityWorker.createUnknownsArray(fieldsWithoutId);
		SQL_FIND = Query.select(fieldNames)
						.from(entityName)
						.where(Entity.ID).equal(Query.UNKNOWN)
				  .build().toString();
		SQL_ADD = Query.insertInto(entityName)
							.variebleNames(fieldNamesWithoutId)
							.values(unknownsWithoutId)
						.build();
		SQL_SAVE = Query.update(entityName)
							 .setArrayPrpared(fieldNamesWithoutId, unknownsWithoutIdArray)
							 .where(Entity.ID).equal(Query.UNKNOWN)
					    .build();
		SQL_FIND_ALL = Query.select(fieldNames)
								 .from(entityName)
								 .orderBy(Entity.ID)
							.build();
		SQL_DELETE = Query.deleteFrom(entityName)
							   .where(Entity.ID).equal(Query.UNKNOWN)
						  .build();
	}

	/**
	 * For returning connection to connection pool.
	 * 
	 * @param rs
	 * @param connection connection to returning.
	 */
	protected static void closeConnection(ResultSet rs, Connection connection) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				LOGGER.error("ResultSet " + rs + " can not be closed.", e);
			}
		}
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			LOGGER.error("Connection " + connection + " can not be closed.", e);
		}
	}

	/**
	 * Universal delete method for every situation) it is deletes any row from 
	 * table of this entity from db.
	 * 
	 * @param cp ConnectionPool.
	 * @param sql query to db.
	 * @param id delete criteria value.
	 * @throws DaoException
	 */
	public static void delete(IConnectionPool cp, String sql, Long id) 
			throws DaoException {
		Connection c = cp.takeConnection();
		try {
			PreparedStatement st = c.prepareStatement(sql);
			Executor.executeQuery(st, id);
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
	}
	
	/**
	 * Join and first select.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws DaoException 
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> joinLeftGet(IConnectionPool cp, Field[] fields, 
			String sql, Object value) throws DaoException {
		List<T> newsList = new ArrayList<>();
		try {
			Connection c = cp.takeConnection();
			PreparedStatement st = c.prepareStatement(sql);
			ResultSet rs = Executor.execute(st, value);
			while (rs.next()) {
				T t = (T) EntityWorker.createEntity(News.class, fields, rs);
				newsList.add(t);
			}
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		}
		return newsList;
	}
	
	/**
	 * Universal delete method for every situation) it is deletes any row from 
	 * table of this entity from db.
	 * 
	 * @param cp ConnectionPool.
	 * @param sql query to db.
	 * @param id delete criteria value.
	 * @throws DaoException
	 */
	public static void execute(IConnectionPool cp, String sql) 
			throws DaoException {
		Connection c = cp.takeConnection();
		try {
			Statement st = c.createStatement();
			Executor.executeUpdate(st, sql);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
	}
	
	/**
	 * Method for selecting all entities.
	 * 
	 * @return list of entities form db.
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> find(IConnectionPool cp, Class<?> entityClass,
			Field[] fields, String sql) throws DaoException {
		List<T> entities = new ArrayList<>();
		Connection c = cp.takeConnection();
		ResultSet rs = null;
		try {
			PreparedStatement st = c.prepareStatement(sql);
			rs = st.executeQuery();
			while (rs.next()) {
				T entity = (T) EntityWorker.createEntity(entityClass, fields, rs);
				entities.add(entity);
			}
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(rs, c);
		}
		return entities;
	}
	
	/**
	 * Method for selects entity by its id.
	 * 
	 * @param id user id.
	 * @return founded entity.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T find(long id) throws DaoException {
		T entity = null;
		Connection c = connectionPool.takeConnection();
		ResultSet rs = null;
		try {
			PreparedStatement st = c.prepareStatement(SQL_FIND);
			rs = Executor.executeQuery(st, id);
			if (rs.next()) {
				entity = (T) EntityWorker.createEntity(entityClass, fields, rs);
			}
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(rs, c);
		}
		return entity;
	}

	/**
	 * Adds entity to the database.
	 * 
	 * @param entity entity for adding to a database.
	 */
	@Override
	public T add(T entity) throws DaoException {
		Connection c = connectionPool.takeConnection();
		try {
			Object[] values = EntityWorker.entityValues(entity, fieldsWithoutId);
			PreparedStatement st = c.prepareStatement(SQL_ADD, PRIMARY_KEY_OUT);
			/* Sets values and execute statement. */
			Executor.executeUpdate(st, fieldsWithoutId, values);
			ResultSet genKey = st.getGeneratedKeys();
			if (genKey.next()) {
				long entityId = genKey.getLong(1);
				
				/* Seting entity id. */
				EntityWorker.putEntityId(entity, entityId);
			}
		} catch (SQLException | DataModelException e) {
			e.printStackTrace();
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
		return entity;
	}

	/**
	 * Adds entities to the database.
	 * 
	 * @param entities entities for adding to a database.
	 */
	@Override
	public void addMany(List<T> entities) throws DaoException {
		Connection c = connectionPool.takeConnection();
		try {
			PreparedStatement st = c.prepareStatement(SQL_ADD);
			for (T entity : entities) {
				Object[] values = EntityWorker.entityValues(entity, fieldsWithoutId);
				
				
				Executor.addBatch(st, values);
			}
			Executor.executeBatch(st);
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
	}
	
	/**
	 * Saves entity.
	 * 
	 * @param entity entity for saving in a database.
	 */
	@Override
	public void save(T entity) throws DaoException {
		Connection c = connectionPool.takeConnection();
		try {
			Object[] values = EntityWorker.entityValues(entity, fieldsIdInTheEnd);
			PreparedStatement st = c.prepareStatement(SQL_SAVE);
			Executor.executeUpdate(st, fieldsIdInTheEnd, values);
		} catch (SQLException | DataModelException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
	}

	/**
	 * Method for selecting all entities.
	 * 
	 * @return list of entities form db.
	 */
	@Override
	public List<T> findAll() throws DaoException {
		return find(connectionPool, entityClass, fields, SQL_FIND_ALL);
	}

	/**
	 * Deletes news rows in database by id.
	 * 
	 * @param id for delete rows.
	 * 
	 * @throws DaoException throws on sql exeption.
	 */
	@Override
	public void delete(Long id) throws DaoException {
		delete(connectionPool, SQL_DELETE, id);
	}

	/**
	 * Deletes news rows in database by ids array.
	 * 
	 * @param ids id array for delete rows.
	 * 
	 * @throws DaoException throws on sql exeption.
	 */
	@Override
	public void deleteMany(Long[] ids) throws DaoException {
		Connection c = connectionPool.takeConnection();
		try {
			PreparedStatement st = c.prepareStatement(SQL_DELETE);
			for (Long id : ids) {
				Executor.addBatch(st, id);
			}
			Executor.executeBatch(st);
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeConnection(null, c);
		}
	}
}
