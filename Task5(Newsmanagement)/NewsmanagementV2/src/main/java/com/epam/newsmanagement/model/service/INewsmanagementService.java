package com.epam.newsmanagement.model.service;

import java.util.Collection;
import java.util.List;


import com.epam.newsmanagement.model.data.DataModelException;
import com.epam.newsmanagement.model.data.entity.Author;
import com.epam.newsmanagement.model.data.entity.Comments;
import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.Tag;

/**
 * It consists required methods of business logic of this application.
 * 
 * @author Yury_Shkahau
 *
 */
public interface INewsmanagementService {

	/**
	 * Adds news to db.
	 * 
	 * @param news
	 * @throws DataModelException
	 */
	News addNews(News news) throws DataModelException;
	
	/**
	 * Edits news from db by its id inside.
	 */
	void editNews(News news) throws DataModelException;
	
	/**
	 * Delete news from db by its input id.
	 */
	void deleteNews(long id) throws DataModelException;
	
	/**
	 * Delete news from db by its input id.
	 */
	void deleteNews(News news) throws DataModelException;
	
	/**
	 * Searches all news.
	 * 
	 * @return all news sorted by its comment count.
	 * @throws DataModelException
	 */
	List<News> listOfNews() throws DataModelException;
	
	/**
	 * Searches new by its id.
	 * 
	 * @param id news id.
	 * @return news founded.
	 * @throws DataModelException
	 */
	News singleNewsMessage(long id) throws DataModelException;
	
	/**
	 * Adding news author.
	 * 
	 * @param author
	 * @return
	 * @throws DataModelException
	 */
	Author addNewsAuthor(long newsId, Author author) throws DataModelException;
	
	/**
	 * Search all news for the one author.
	 * 
	 * @param author search criteria.
	 * @return all news for the one author
	 * @throws DataModelException
	 */
	Collection<News> searchByAuthor(Author author) throws DataModelException;
	
	/**
	 * Adds tag to news whith id newId.
	 * 
	 * @param newsId id of news.
	 * @param tag tag/tags for add that to news.
	 * @throws DataModelException
	 */
	Tag addTag(long newsId, Tag tag) throws DataModelException;
	
	/**
	 * Adds tags to news whith id newId.
	 * 
	 * @param newsId id of news.
	 * @param tag tag/tags for add that to news.
	 * @throws DataModelException
	 */
	Collection<Tag> addTag(long newsId, Collection<Tag> tags) throws DataModelException;
	
	/**
	 * Searches news by tag.
	 * 
	 * @param tag tag for search in db.
	 * @throws DataModelException
	 */
	List<News> newsByTag(Tag tag) throws DataModelException;
	
	/**
	 * Adds comment/comments to news with id newId.
	 * 
	 * @param newsId id of news.
	 * @param comment comment/comments for add that to news.
	 * @throws DataModelException
	 */
	Comments addComment(long newsId, Comments comment) throws DataModelException;
	
	/**
	 * Adds comment/comments to news with id newId.
	 * 
	 * @param newsId id of news.
	 * @param comment comment/comments for add that to news.
	 * @throws DataModelException
	 */
	Collection<Comments> addComment(long newsId, Collection<Comments> comments) throws DataModelException;
	
	/**
	 * Deletes comment from news.
	 * 
	 * @param newsId id of news.
	 * @param commentId comment id for delete.
	 * @throws DataModelException
	 */
	void deleteComment(long commentId) throws DataModelException;
	
	/**
	 * Deletes comments from news.
	 * 
	 * @param newsId id of news.
	 * @param commentId comments id for delete.
	 * @throws DataModelException
	 */
	void deleteComment(List<Long> commentIds) throws DataModelException;
	
	/**
	 * Gets list of news sorted by most commented news
	 * 
	 * @return
	 * @throws DataModelException
	 */
	List<News> listNewsSortedByMostCommented() throws DataModelException;
	
	/**
	 * Saves news with author and its tags.
	 * 
	 * @return
	 * @throws DataModelException
	 */
	News saveNews(News news, Author author, List<Tag> tags) throws DataModelException;
}
