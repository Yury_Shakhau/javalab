package com.epam.newsmanagement.model.data;

/**
 * Exception of data model layer.
 * 
 * @author Yury_Shakhau
 *
 */
public class DataModelException extends Exception {
	/**
	 * Identification number for serialization and deserialization.
	 */
	private static final long serialVersionUID = 465334668435921L;

	public DataModelException() {
		super();
	}
	
	public DataModelException(String message) {
		super(message);
	}
	
	public DataModelException(Throwable t) {
		super(t);
	}
	
	public DataModelException(String message, Throwable t) {
		super(message, t);
	}
}
