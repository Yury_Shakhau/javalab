package com.epam.newsmanagement.model.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoFactory implements IDaoFactory {

	@Autowired
	private static IDaoFactory instance;
	
	@Autowired
	private IAuthorDao authorDao;
	
	@Autowired
	private ICommentsDao commentDao;
	
	@Autowired
	private INewsAuthorDao newsAuthorDao;
	
	@Autowired
	private INewsDao newsDao;
	
	@Autowired
	private INewsTagDao newsTagDao;
	
	@Autowired
	private ITagDao tagDao;
	
	@Override
	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	@Override
	public ICommentsDao getCommentDao() {
		return commentDao;
	}

	@Override
	public INewsAuthorDao getNewsAuthorDao() {
		return newsAuthorDao;
	}

	@Override
	public INewsDao getNewsDao() {
		return newsDao;
	}

	@Override
	public INewsTagDao getNewsTagDao() {
		return newsTagDao;
	}

	@Override
	public ITagDao getTagDao() {
		return tagDao;
	}

	public static IDaoFactory getInstance() {
		return instance;
	}
}
