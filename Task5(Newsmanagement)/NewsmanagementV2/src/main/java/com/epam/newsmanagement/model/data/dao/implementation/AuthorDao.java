package com.epam.newsmanagement.model.data.dao.implementation;

import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.DaoFactory;
import com.epam.newsmanagement.model.data.dao.IAuthorDao;
import com.epam.newsmanagement.model.data.dao.INewsAuthorDao;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.entity.Author;
import com.epam.newsmanagement.model.data.entity.NewsAuthor;

/**
 * Extends for this dao.
 * Implements incident dao methods.
 * 
 * @author Yury_Shakhau
 *
 * @param <Author> entity type.
 */
@Repository
public class AuthorDao extends Dao<Author> implements IAuthorDao {

	public AuthorDao() {
		super(Author.class);
	}

	/**
	 * Adds author for news.
	 */
	@Override
	public Author addAuthor(long newsId, Author author) throws DaoException {
		INewsDao newsDao = DaoFactory.getInstance().getNewsDao();
		INewsAuthorDao newsAuthorDao = DaoFactory.getInstance().getNewsAuthorDao();
		if (newsDao.find(newsId) != null) {
			author = add(author);
			if (author.getId() > 0) {
				NewsAuthor newsAuthor = new NewsAuthor();
				newsAuthor.setNewsId(newsId);
				newsAuthor.setAuthorId(author.getId());
				newsAuthorDao.add(newsAuthor);
			} else {
				throw new DaoException("Author did not added.");
			}
		} else {
			throw new DaoException("News with id " + newsId +" does not exists.");
		}
		return author;
	}

}
