package com.epam.newsmanagement.model.data.entity;

/**
 * Entity constants.
 * 
 * @author Yury_Shakhau
 *
 */
public final class Entity {

	private Entity() {}
	
	/**
	 * Foreign key name for each entity.
	 */
	public static final String ID = "id";
}
