package com.epam.newsmanagement.model.data.dao;

import java.util.Collection;

import com.epam.newsmanagement.model.data.entity.Tag;

/**
 * Tag dao extension with specific methods of working whit that entity.
 * 
 * @author Yury_Shakhau
 *
 */
public interface ITagDao extends IDao<Tag> {

	/**
	 * Searches tag by its name.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return this entity with set id.
	 * @throws DaoException
	 */
	Tag findByName(String name) throws DaoException;
	
	/**
	 * Adds tag for news.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return this entity with set id.
	 * @throws DaoException
	 */
	Tag addTag(long newsId, Tag tag) throws DaoException;
	
	/**
	 * Adds tags for news.
	 * 
	 * @param newsId news primary key.
	 * @param tag tag to add.
	 * @return this entity with set id.
	 * @throws DaoException
	 */
	Collection<Tag> addTag(long newsId, Collection<Tag> tags) throws DaoException;
}
