package com.epam.newsmanagement.model.data.dao.implementation;

import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.model.data.dao.INewsTagDao;
import com.epam.newsmanagement.model.data.entity.NewsTag;

/**
 * Extends for for this dao.
 * Implements incident dao mathods.
 * 
 * @author Yury_Shakhau
 *
 * @param <NewsTag> entity type.
 */
@Repository
public class NewsTagDao extends Dao<NewsTag> implements INewsTagDao {

	public NewsTagDao() {
		super(NewsTag.class);
	}
}
