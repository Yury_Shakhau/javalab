package com.epam.newsmanagement.model.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.model.data.DataModelException;
import com.epam.newsmanagement.model.data.dao.DaoException;
import com.epam.newsmanagement.model.data.dao.IAuthorDao;
import com.epam.newsmanagement.model.data.dao.ICommentsDao;
import com.epam.newsmanagement.model.data.dao.IDaoFactory;
import com.epam.newsmanagement.model.data.dao.INewsDao;
import com.epam.newsmanagement.model.data.dao.ITagDao;
import com.epam.newsmanagement.model.data.entity.Author;
import com.epam.newsmanagement.model.data.entity.Comments;
import com.epam.newsmanagement.model.data.entity.News;
import com.epam.newsmanagement.model.data.entity.Tag;

/**
 * Implements required methods of business logic of this application.
 * 
 * @author Yury_Shkahau
 */
@Service
public class NewsmanagementService implements INewsmanagementService {

	@Autowired
	private IDaoFactory daoFactory;
	
	/**
	 * Adds news to db.
	 * 
	 * @param news
	 * @throws DaoException 
	 * @throws DataModelException
	 */
	@Override
	public News addNews(News news) throws DataModelException {
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			news = newsDao.add(news);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return news;
	}

	/**
	 * Edits news from db by its id inside.
	 */
	@Override
	public void editNews(News news) throws DataModelException {
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsDao.save(news);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
	}

	/**
	 * Delete news from db by its input id.
	 */
	@Override
	public void deleteNews(long id) throws DataModelException {
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsDao.deleteNewsFull(id);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
	}

	/**
	 * Delete news from db by its input id.
	 */
	@Override
	public void deleteNews(News news) throws DataModelException {
		deleteNews(news.getId());
	}
	
	/**
	 * Searches all news.
	 * 
	 * @return all news sorted by its comment count.
	 * @throws DataModelException
	 */
	@Override
	public List<News> listOfNews() throws DataModelException {
		List<News> newsList = null;
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsList = newsDao.findAll();
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return newsList;
	}

	/**
	 * Searches new by its id.
	 * 
	 * @param id news id.
	 * @return news founded.
	 * @throws DataModelException
	 */
	@Override
	public News singleNewsMessage(long id) throws DataModelException {
		News news = null;
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			news = newsDao.find(id);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return news;
	}

	/**
	 * Adding news author.
	 * 
	 * @param author
	 * @return
	 * @throws DaoException 
	 * @throws DataModelException
	 */
	@Override
	public Author addNewsAuthor(long newsId, Author author) throws DataModelException {
		IAuthorDao authorDao = daoFactory.getAuthorDao();
		try {
			author = authorDao.addAuthor(newsId, author);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return author;
	}

	/**
	 * Search all news for the one author.
	 * 
	 * @param author search criteria.
	 * @return all news for the one author
	 * @throws DataModelException
	 */
	@Override
	public Collection<News> searchByAuthor(Author author) throws DataModelException {
		List<News> newsList = null;
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsList = newsDao.findNewsByAuthorId(author.getId());
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return newsList;
	}

	/**
	 * Adds tag to news with id newId.
	 * 
	 * @param newsId id of news.
	 * @param tag tag/tags for add that to news.
	 * @throws DataModelException
	 */
	public Tag addTag(long newsId, Tag tag) throws DataModelException {
		ITagDao tagDao = daoFactory.getTagDao();
		try {
			tag = tagDao.addTag(newsId, tag);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return tag;
	}
	
	/**
	 * Adds tags to news whith id newId.
	 * 
	 * @param newsId id of news.
	 * @param tag tag/tags for add that to news.
	 * @throws DataModelException
	 */
	@Override
	public Collection<Tag> addTag(long newsId, Collection<Tag> tags) 
			throws DataModelException {
		ITagDao tagDao = daoFactory.getTagDao();
		try {
			tags = tagDao.addTag(newsId, tags);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return tags;
	}
	
	/**
	 * Searches news by tag.
	 * 
	 * @param tag tag
	 * @return news founded.
	 * @throws DataModelException
	 */
	@Override
	public List<News> newsByTag(Tag tag) throws DataModelException {
		List<News> newsList = null;
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsList = newsDao.findNewsByTagName(tag);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return newsList;
	}
	
	/**
	 * Adds comment/comments to news whith id newId.
	 * 
	 * @param newsId id of news.
	 * @param comment comment/comments for add that to news.
	 * @throws DataModelException
	 */
	@Override
	public Comments addComment(long newsId, Comments comment) 
			throws DataModelException {
		ICommentsDao commentsDao = daoFactory.getCommentDao();
		try {
			comment.setNewsId(newsId);
			comment = commentsDao.add(comment);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return comment;
	}

	/**
	 * Adds comment/comments to news whith id newId.
	 * 
	 * @param newsId id of news.
	 * @param comment comment/comments for add that to news.
	 * @throws DataModelException
	 */
	@Override
	public Collection<Comments> addComment(long newsId, Collection<Comments> comments)
			throws DataModelException {
		for (Comments c : comments) {
			c = addComment(newsId, c);
		}
		return comments;
	}
	
	/**
	 * Deletes comment from news.
	 * 
	 * @param newsId id of news.
	 * @param commentId comment id for delete.
	 * @throws DataModelException
	 */
	@Override
	public void deleteComment(long commentId) 
			throws DataModelException {
		ICommentsDao commentsDao = daoFactory.getCommentDao();
		try {
			commentsDao.delete(commentId);
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
	}

	/**
	 * Deletes comments from news.
	 * 
	 * @param newsId id of news.
	 * @param commentId comments id for delete.
	 * @throws DataModelException
	 */
	@Override
	public void deleteComment(List<Long> commentIds) 
			throws DataModelException {
		ICommentsDao commentsDao = daoFactory.getCommentDao();
		try {
			commentsDao.deleteMany((Long[]) commentIds.toArray());
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
	}
	
	/**
	 * Gets list of news sorted by most commented news
	 * 
	 * @return
	 * @throws DataModelException
	 */
	@Override
	public List<News> listNewsSortedByMostCommented() throws DataModelException {
		List<News> newsList = null;
		INewsDao newsDao = daoFactory.getNewsDao();
		try {
			newsDao.findAll();
		} catch (DaoException e) {
			throw new DataModelException(e);
		}
		return newsList;
	}

	@Override
	public News saveNews(News news, Author author, List<Tag> tags)
			throws DataModelException {
		news = addNews(news);
		if (news.getId() > 0) {
			author = addNewsAuthor(news.getId(), author);
			if (news.getId() > 0) {
				addTag(news.getId(), tags);
			} else {
				throw new DataModelException("Author " + author.getName() + " did not added.");
			}
		} else {
			throw new DataModelException("News " + news.getTitle() + " did not added.");
		}
		return null;
	}
}
