package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

/**
 * Comment for news.
 * 
 * @author Yury_Shakhau
 *
 */
public class Comments implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -27289984413802173L;
	
	public static final String TABLE_NAME = News.class.getSimpleName();
	
	/**
	 * Primary key.
	 */
	public static final String ID = TABLE_NAME + ".id";
	public static final String COMMENT_TEXT = TABLE_NAME + ".commentText";
	public static final String CREATION_DATE = TABLE_NAME +  ".creationDate";
	public static final String NEWS_ID = TABLE_NAME + ".newsId";
	
	/**
	 * All field names of this entity.
	 */
	public static final List<String> FIELDS = Arrays.asList(
			ID,
			COMMENT_TEXT,
			CREATION_DATE,
			NEWS_ID);
	
	/**
	 * Primary key.
	 */
	private long id;
	
	/**
	 * Comment content for news.
	 */
	private String commentText;
	
	/**
	 * Creation date of this comment.
	 */
	private Date creationDate;
	
	/**
	 * Foreign key to news.
	 */
	private Long newsId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
}
