package com.epam.newsmanagement.model.data.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

/**
 * News entity.
 * 
 * @author Yury_Shakhau
 *
 */
public class News implements Serializable, IEntity {

	/**
	 * For deserialization with no exception after modification.
	 */
	private static final long serialVersionUID = -2759289984483L;
	
	public static final String TABLE_NAME = News.class.getSimpleName();
	
	public static final String ID = TABLE_NAME + ".id";
	public static final String SHORT_TEXT = TABLE_NAME + ".shortText";
	public static final String FULL_TEXT = TABLE_NAME +  ".fullText";
	public static final String TITLE = TABLE_NAME + ".title";
	public static final String CREEATION_DATE = TABLE_NAME + ".creationDate";
	public static final String MODIFICATION_DATE = TABLE_NAME + ".modificationDate";
	
	public static final List<String> FIELDS = Arrays.asList(ID,
															SHORT_TEXT,
															FULL_TEXT,
															TITLE,
															CREEATION_DATE,
															MODIFICATION_DATE);
	
	/**
	 * Primary key.
	 */
	private long id;
	
	/**
	 * Short content of news.
	 */
	private String shortText;
	
	/**
	 * Full content of news.
	 */
	private String fullText;
	
	/**
	 * Head title of news.
	 */
	private String title;
	
	/**
	 * Date creation of this.
	 */
	private Date creationDate;
	
	/**
	 * Date modification of this.
	 */
	private Date modificationDate;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
}
