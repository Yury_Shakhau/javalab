package util.model.sql.query;

/**
 * Question symbol for jdbc query.
 * 
 * @author Yury_Shakhau
 *
 */
public class Unknown {
	@Override
	public String toString() {
		return Query.UNKNOWN;
	}
}
