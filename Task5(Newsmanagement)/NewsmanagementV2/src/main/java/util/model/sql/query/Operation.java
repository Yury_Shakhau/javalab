package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public abstract class Operation extends Builder {
	
	private final StringBuilder query;
	
	public Operation(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	private static void appendVar(StringBuilder query, Object opearation, Object var) {
		query.append(opearation);
		if (var.equals(Query.UNKNOWN)) {
			query.append(var);
		} else {
			query.append('\'')
				 .append(var)
				 .append('\'');
		}
	}

	public OperationNextLevel equal(Object var) {
		appendVar(query, Query.EQUALS, var);
		return (OperationNextLevel) this;
	}
	
	public OperationNextLevel larger(Object var) {
		appendVar(query, Query.LARGER, var);
		return (OperationNextLevel) this;
	}
	
	public OperationNextLevel less(Object var) {
		appendVar(query, Query.LESS, var);
		return (OperationNextLevel) this;
	}
	
	public OperationNextLevel largerOrEqual(Object var) {
		appendVar(query, Query.LARGER_OR_EQUALS, var);
		return (OperationNextLevel) this;
	}
	
	public OperationNextLevel lessOrEqual(Object var) {
		appendVar(query, Query.LESS_OR_EQUALS, var);
		return (OperationNextLevel) this;
	}

	public OperationNextLevel orderBy(Object varName) {
		String sql = query.toString();
		if (!sql.contains(" " + Query.ORDER_BY + " ")) {
			query.append(' ')
				 .append(Query.ORDER_BY)
				 .append(' ');
		} else {
			query.append(',');
		}
		query.append(varName);
		return (OperationNextLevel) this;
	}
	
	public OperationNextLevel in(Object...var) {
		query.append(' ').append(Query.IN).append(' ');
		int lastIndex = var.length - 1;
		if (lastIndex > -1) {
			query.append('(');
			for (int i = 0; i < lastIndex; i++) {
				query.append(var[i]).append(',');
			}
			query.append(var[lastIndex]).append(')');
		}
		return (OperationNextLevel) this;
	}
}
