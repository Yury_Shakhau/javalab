package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public class Builder {
	
	private final StringBuilder query;
	
	public Builder(StringBuilder query) {
		this.query = query;
	}
	
	public StringBuilder getQuery() {
		return query;
	}

	public String build() {
		return query.toString();
	}
}
