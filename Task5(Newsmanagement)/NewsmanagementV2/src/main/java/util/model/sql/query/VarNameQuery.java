package util.model.sql.query;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.model.data.dao.implementation.Dao;

/**
 * Contains method for SET data in sql after UPDATE word.
 * 
 * @author Yury_Shakhau
 *
 */
public class VarNameQuery extends Condition {

	private static final Logger LOGGER = Logger.getLogger(Dao.class);
	
	private final StringBuilder query;
	
	public VarNameQuery(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	/**
	 * Subquery for modify data in db.
	 * 
	 * @param varName column name.
	 * @param value new column value in db.
	 * @return next operator for query building.
	 */
	public VarNameQuery set(String varName, Object value) {
		String sql = query.toString();
		if (!sql.contains(" " + Query.SET + " ")) {
			query.append(' ')
				 .append(Query.SET)
				 .append(' ');
		} else {
			query.append(',');
		}
		query.append(varName)
		 	 .append(Query.EQUALS)
		 	 .append('\'')
		 	 .append(value)
		 	 .append('\'');
		return this;
	}
	
	/**
	 * Subquery for modify data in db.
	 * 
	 * @param varName column names.
	 * @param value new column values in db.
	 * @return next operator for query building.
	 */
	public VarNameQuery set(String[] varNames, Object[] values) throws SQLException {
		if (varNames.length != values.length) {
			throw new SQLException("varNames.length=" + varNames.length + 
								   " and values.length must be equals=" + values.length);
		}
		int length = varNames.length;
		if (length > 0) {
			query.append(' ')
				 .append(Query.SET)
				 .append(' ')
				 .append(varNames[0])
				 .append(Query.EQUALS)
		 		 .append('\'')
				 .append(values[0])
				 .append("\'");
			for (int i = 1; i < length; i++) {
				query.append(',')
					 .append(varNames[i])
					 .append(Query.EQUALS)
					 .append('\'')
					 .append(values[i])
					 .append('\'');
			}
		}
		return this;
	}
	
	/**
	 * Subquery for modify data in db.
	 * It works as upper method but not throws exception.
	 * It log it in log file.
	 * 
	 * @param varName column name.
	 * @param value new column value in db.
	 * @return next operator for query building.
	 */
	public VarNameQuery setArray(String[] varNames, Object[] values) {
		VarNameQuery query = null;
		try {
			query = set(varNames, values);
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		return query;
	}
	
	/**
	 * Subquery for modify data in db.
	 * It works as upper method but not throws exception.
	 * It log it in log file.
	 * 
	 * @param varName column name.
	 * @param value new column value in db.
	 * @return next operator for query building.
	 */
	public VarNameQuery setArrayPrpared(String[] varNames, Object[] values) {
		try {
			if (varNames.length != values.length) {
				throw new SQLException("varNames.length=" + varNames.length + 
									   " and values.length must be equals=" + values.length);
			}
			int length = varNames.length;
			if (length > 0) {
				query.append(' ')
					 .append(Query.SET)
					 .append(' ')
					 .append(varNames[0])
					 .append(Query.EQUALS)
					 .append(values[0]);
				for (int i = 1; i < length; i++) {
					query.append(',')
						 .append(varNames[i])
						 .append(Query.EQUALS)
						 .append(values[i]);
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		return this;
	}
}
