package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public class FromOperation extends Builder {

	private StringBuilder query;
	
	public FromOperation(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	public ConditionExtendsQuery from(String tableName) {
		query.append(' ')
			 .append(Query.FROM)
			 .append(' ')
			 .append(tableName);
		return new ConditionExtendsQuery(query);
	}
	
	public ConditionExtendsQuery from(Class<?> entityClass) {
		return from(entityClass.getSimpleName());
	}
}
