package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public class ConditionExtendsQuery extends Condition {

	private StringBuilder query;
	
	public ConditionExtendsQuery(StringBuilder query) {
		super(query);
		this.query = query;
	}

	public ConditionExtendsQuery as(String alias) {
		query.append(' ')
			 .append(alias);
		return this;
	}
	
	public ConditionExtendsQuery join(String tableName) {
		query.append(' ')
			 .append(Query.JOIN)
			 .append(' ')
			 .append(tableName);
		return this;
	}
	
	public ConditionExtendsQuery join(Class<?> entityClass) {
		return join(entityClass.getSimpleName());
	}
	
	public ConditionExtendsQuery join(Object entity) {
		return join(entity.getClass().getSimpleName());
	}
	
	public ConditionExtendsQuery groupBy(Object...column) {
		int length = column.length;
		if (length > 0) {
			query.append(' ')
				 .append(Query.GROU_BY)
				 .append(' ')
				 .append(column[0]);
			for (int i = 1; i < length; i++) {
				query.append(',').append(column[i]);
			}
		}
		return this;
	}
	
	public ConditionExtendsQuery orderBy(Object...column) {
		int length = column.length;
		if (length > 0) {
			query.append(' ')
				 .append(Query.ORDER_BY)
				 .append(' ')
				 .append(column[0]);
			for (int i = 1; i < length; i++) {
				query.append(',').append(column[i]);
			}
		}
		return this;
	}
	
	public ConditionExtendsQuery asc() {
		query.append(' ')
			 .append(Query.DESC);
		return this;
	}
	
	public ConditionExtendsQuery desc() {
		query.append(' ')
			 .append(Query.DESC);
		return this;
	}
}
