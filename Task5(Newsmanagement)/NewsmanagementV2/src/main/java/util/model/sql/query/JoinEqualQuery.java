package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public class JoinEqualQuery {

	private final StringBuilder query;
	
	public JoinEqualQuery(StringBuilder query) {
		this.query = query;
	}
	
	public ConditionExtendsQuery equal(Object var) {
		query.append(Query.EQUALS)
			 .append(var);
		return new ConditionExtendsQuery(query);
	}
}
