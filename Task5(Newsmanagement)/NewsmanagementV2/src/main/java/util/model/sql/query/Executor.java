package util.model.sql.query;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public final class Executor {

	private Executor() {}
	
	private static final SqlTypeConverter CONVERTER = new SqlTypeConverter();

	/**
	 * Converter java types to sql types.
	 * 
	 * @author Yury_Shakhau
	 *
	 */
	private static class SqlTypeConverter {
		
		private static Map<Class<?>, Integer> typeContainer = new HashMap<>();

		static {
			typeContainer.put(Integer.class, Types.INTEGER);
			typeContainer.put(String.class, Types.VARCHAR);
			typeContainer.put(Long.class, Types.BIGINT);
			typeContainer.put(Character.class, Types.CHAR);
			typeContainer.put(Boolean.class, Types.BOOLEAN);
			typeContainer.put(Double.class, Types.DOUBLE);
			typeContainer.put(Float.class, Types.FLOAT);
			typeContainer.put(Byte.class, Types.BIT);
		}
		
		public int convertToSqlType(Class<?> typeClass) {
			return typeContainer.get(typeClass);
		}
	}
	
	/**
	 * Executes statement with adding id.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet executeQuery(PreparedStatement statement, long id) 
			throws SQLException {
		statement.setLong(1, id);
		return statement.executeQuery();
	}
	
	/**
	 * Executes statement with object equals obj.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet executeQuery(PreparedStatement statement, String fieldValue) 
			throws SQLException {
		statement.setObject(1, fieldValue);
		return statement.executeQuery();
	}
	
	/**
	 * Executes statement with value.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet executeQuery(PreparedStatement statement, Object value) 
			throws SQLException {
		statement.setObject(1, value);
		return statement.executeQuery();
	}
	
	/**
	 * Executes statement with adding id.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final int executeUpdate(Statement statement, String sql) 
			throws SQLException {
		return statement.executeUpdate(sql);
	}
	
	/**
	 * Simple execute statement.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final int executeUpdate(PreparedStatement statement) 
			throws SQLException {
		return statement.executeUpdate();
	}
	
	/**
	 * Simple execute statement.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet execute(PreparedStatement statement, long id) 
			throws SQLException {
		statement.setLong(1, id);
		return statement.executeQuery();
	}
	
	/**
	 * Simple execute statement.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet execute(PreparedStatement statement, String value) 
			throws SQLException {
		statement.setString(1, value);
		return statement.executeQuery();
	}

	/**
	 * Simple execute statement.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final ResultSet execute(PreparedStatement statement, Object value) 
			throws SQLException {
		statement.setObject(1, value);
		return statement.executeQuery();
	}
	
	/**
	 * Executes statement with adding many values in that.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final int executeUpdate(PreparedStatement statement, Field[] fields, Object[] values) 
			throws SQLException {
		int j = 1;
		for (int i = 0; i < values.length; i++) {
			Object val = values[i];
			if (val != null) {
				statement.setObject(j, val);
			} else {
				statement.setNull(i, CONVERTER.convertToSqlType(fields[i].getType()));
			}
			j++;
		}
		return statement.executeUpdate();
	}
	
	/**
	 * Executes statement with adding object.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final void addBatch(PreparedStatement statement, Object object) 
			throws SQLException {
		statement.setObject(1, object);
		statement.addBatch();
	}
	
	/**
	 * Adds batch statement with adding many values in that.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final void addBatch(PreparedStatement statement, Field[] fields, Object[] values) 
			throws SQLException {
		for (int i = 0; i < values.length; i++) {
			Object val = values[i];
			if (val != null) {
				statement.setObject(i, val);
			} else {
				statement.setNull(i, CONVERTER.convertToSqlType(fields[i].getType()));
			}
		}
		statement.addBatch();
	}
	
	/**
	 * Simple add batch.
	 * 
	 * @param statement
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static final int[] executeBatch(PreparedStatement statement) 
			throws SQLException {
		return statement.executeBatch();
	}
}
