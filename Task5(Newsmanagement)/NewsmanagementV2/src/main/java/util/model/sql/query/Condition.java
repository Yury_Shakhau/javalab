package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public abstract class Condition extends Builder {

	private StringBuilder query;
	
	public Condition(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	public Operation where(String varName) {
		query.append(' ')
			 .append(Query.WHERE)
			 .append(' ')
			 .append(varName);
		return new OperationNextLevel(query);
	}
	
	public JoinEqualQuery on(Object varName) {
		query.append(' ')
		 	 .append(Query.ON)
		 	 .append(' ')
		 	 .append(varName);
		return new JoinEqualQuery(query);
	}
}
