package util.model.sql.query;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.model.data.entity.Entity;

/**
 * Query builder methods for creating sql dynamically.
 * 
 * @author Yury_Shkahau
 *
 */
public class AfterInsertQuery extends Builder {

	private final StringBuilder query;
	
	public AfterInsertQuery(StringBuilder query) {
		super(query);
		this.query = query;
	}
	
	public AfterInsertQueryExtends variebleNames(String...varNames) {
		query.append(" (");
		int lastIndex = varNames.length - 1;
		if (lastIndex > -1) {
			for (int i = 0; i < lastIndex; i++) {
				query.append(varNames[i]).append(',');
			}
			query.append(varNames[lastIndex]);
		}
		query.append(')');
		return new AfterInsertQueryExtends(query);
	}
	
	/**
	 * And returns entity fields without id field.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Field[] fieldsWithoutId(Field[] fieldNames) {
		List<Field> buff = new ArrayList<>(fieldNames.length);
		for (Field fn : fieldNames) {
			if (!fn.getName().equals(Entity.ID)) {
				buff.add(fn);
			}
		}
		Field[] fieldsRet = new Field[buff.size()];
		for (int i = 0; i < buff.size(); i++) {
			fieldsRet[i] = buff.get(i);
		}
		return fieldsRet;
	}
	
	/**
	 * Makes entity fields accessible from reflection.
	 * And returns them.
	 * 
	 * @param entityClass
	 * @return
	 */
	public static Field[] makeFieldsAccessable(Class<?> entityClass) {
		Field[] fields = entityClass.getDeclaredFields();
		List<Field> fList = new ArrayList<>();
		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			if (!Modifier.isStatic(f.getModifiers())) {
				f.setAccessible(true);
				fList.add(f);
			}
		}
		Field[] retArray = new Field[fList.size()];
		int i = 0;
		for (Field f : fList) {
			retArray[i] = f;
			i++;
		}
		return retArray;
	}
	
	public Builder values(Object entity) 
			throws IllegalArgumentException, IllegalAccessException {
		Class<?> entClass = entity.getClass();
		Field[] fields = fieldsWithoutId(makeFieldsAccessable(entClass));
		query.append(" (");
		int fLastIndex = fields.length - 1;
		for (int i = 0; i < fLastIndex; i++) {
			Field field = fields[i];
			if (!Modifier.isStatic(field.getModifiers())) {
				field.setAccessible(true);
				query.append(field.getName()).append(',');
			}
		}
		query.append(fields[fLastIndex].getName()).append(") ")
		.append(Query.VALUES);
		query.append(" (");
		for (int j = 0; j < fLastIndex; j++) {
			Field field = fields[j];
			Object value = field.get(entity);
			query.append('\'').append(value).append('\'').append(',');
		}
		query.append('\'').append(fields[fLastIndex].get(entity)).append('\'').append(')');
		return this;
	}
	
	public Builder values(Object[] entities) 
			throws IllegalArgumentException, IllegalAccessException {
		int lastIndex = entities.length - 1;
		if (lastIndex > -1) {
			Object entity = entities[0];
			Class<?> entClass = entity.getClass();
			Field[] fields = fieldsWithoutId(makeFieldsAccessable(entClass));
			query.append(" (");
			int fLastIndex = fields.length - 1;
			for (int i = 0; i < fLastIndex; i++) {
				Field field = fields[i];
				if (!Modifier.isStatic(field.getModifiers())) {
					field.setAccessible(true);
					query.append(field.getName()).append(',');
				}
			}
			query.append(fields[fLastIndex].getName()).append(") ")
			.append(Query.VALUES);
			query.append(" (");
			for (int j = 0; j < fLastIndex; j++) {
				Field field = fields[j];
				Object value = field.get(entity);
				query.append(value).append(',');
			}
			query.append(fields[fLastIndex].get(entity)).append(')');
			if (lastIndex > 0) {
				for (int i = 1; i <= lastIndex; i++) {
					query.append(",");
					query.append('(');
					entity = entities[i];
					for (int j = 0; j < fLastIndex; j++) {
						Field field = fields[j];
						Object value = field.get(entity);
						query.append(value).append(',');
					}
					query.append(fields[fLastIndex].get(entity)).append(')');
				}
			}
		}
		return this;
	}
	
	public Builder values(List<Object> entities) 
			throws IllegalArgumentException, IllegalAccessException {
		return values(entities.toArray());
	}
}
