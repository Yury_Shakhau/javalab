package util.model.sql.query;

/**
 * 
 * 
 * @author Yury_Shakhau
 *
 */
public class OperationNextLevel extends Operation {

	private final StringBuilder query;
	
	public OperationNextLevel(StringBuilder query) {
		super(query);
		this.query = query;
	}

	public Operation and(Object varName) {
		query.append(' ')
		 	 .append(Query.AND)
		 	 .append(' ')
		 	 .append(varName);
		return this;
	}
	
	public Operation or(Object varName) {
		query.append(' ')
		 	 .append(Query.OR)
		 	 .append(' ')
		 	 .append(varName);
		return this;
	}
}
